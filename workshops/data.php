    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>
    <?php

    if(isset($_POST['reg'])){

     if(isset($_SESSION['p_id']))
     {
        $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
          $amt=$_POST['amount'];
          $amt= $amt+($amt*0.03);
          $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
          $posted['txnid']=$txnid;
          $posted['amount']=$amt;
          $posted['firstname']=$fname;
          $posted['email']=$email;
          $posted['phone']=$mbl;
          $posted['productinfo']=$_POST['wname'];;
          $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
              empty($posted['key'])
              || empty($posted['txnid'])
              || empty($posted['amount'])
              || empty($posted['firstname'])
              || empty($posted['email'])
              || empty($posted['phone'])
              || empty($posted['productinfo'])

              ) {
              $formError = 1;
          } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
              $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
              $hash_string .= '|';
            }

            $hash_string .= $SALT;



            $hash = strtolower(hash('sha512', $hash_string));
            $action = $PAYU_BASE_URL . '/_payment';
          }
        } elseif(!empty($posted['hash'])) {
          $hash = $posted['hash'];
          $action = $PAYU_BASE_URL . '/_payment';
        }

      }
      else
      {
        echo '<script>alert("Please login First");</script>';
      }
      $con->close();
    }

    ?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>DATA ANALYTICS| Workshop| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>

       html, body{
        height: 100%;
      }
      body {
        background-image: url(img/workshops.jpg) ;
        background-position: center center;
        background-repeat:  no-repeat;
        background-attachment: fixed;
        background-size:  cover;
        background-color: #999;

      }

      h1{
        font-size: 5.5em;
        font-family: Iceland;

      }
      @media screen and (max-width: 480px) {
        h1{
          transform: scale(0.7);

        }
      }
      h3{
        font-family: Play;
        font-size: 1.5em;
        color:yellow;
      }
      p {
        line-height: 1.8;
        margin: 0 0 2rem;
        color: white;
        font-family: 'Quicksand', sans-serif;
        font-size: 1.2em;
      }

    </style>
<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
  <script>
    jQuery(document).ready(function ($) {
      $(window).load(function () {
          submitPayuForm();
      });
    });
  </script>
    <script type="text/javascript">
      NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br>
      <h1 class="heading w3-text-pink w3-center" style="" >DATA ANALYTICS</h1><br>
      <div class="container">
       <div class="row">
        <div class="col-md-10 col-md-offset-1">

          <div class="left-menu">
            <div class="accordion">
              <div class="section">
                <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
                <label for="section-1"><span><h3>ABOUT</h3></span></label>
                <div class="content">
                  <p>
                   • Data analytics is a workshop on data collection, analyzing
                   and communication effectively.<br>
                   • We get to identify importance of data wrangling.<br>
                   • It teaches about collection of large data sets that can not
                   be processed using traditional computing techniques.
                   <br><br> <h3 class="w3-center w3-text-white">Registration Fee : 1290/-</h3>
                 </p>
               </div>
             </div>
             <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>Certification</h3></span></label>
              <div class="content">
                <p>
                 • Certification will be provided from RoboKart.com.<br>
                 • E-certificate will be available to download at ther websites.<br>
               </p>
             </div>
           </div>

           <div class="section">
            <input type="radio" name="accordion-1" id="section-5" value="toggle"/>
            <label for="section-5"> <span><h3>HIGHLIGHTS</h3></span></label>
            <div class="content">
              <p>
                Get to know about data analysis.<br>
                • How this is useful in business gains.<br>
                • Data analytics in product increase, both quantitatively
                and qualitatively.<br>
                • Helps in making more concrete decision-making for
                business.<br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
            <label for="section-6"> <span><h3>Course Content</h3></span></label>
            <div class="content">
              <p>
                <b>Session 1:</b><br>
                <br>• Introduction to data analytics.
                <br>• Importance of it in modern day society.
                <br>• Introduction to learn about data:
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Collection
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Analyzation
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Communication

                <br><b>Session 2:</b><br>
                <br>• Introduction to big data and analytics.
                <br>•  Process of examining data sets in order to draw
                conclusions.
                <br>• Usage of data analytics and its applications.

                <br><b>Session 3:</b><br>
                <br>• How data analytics is useful in business.
                <br>• Importance of data analytics in industries and organisations to make better decisions.


                <br><b>Session 4:</b><br>
                <BR>• Big data analytics helps in:
                  <BR>• Data mining
                    <BR>• Predictive analytics
                      <BR>• Machine learning tools
                        <BR>• Discussion on decision-making results in:
                          <BR>• Reducing risks in business
                            <BR>• Cost reductions
                              <BR>• Opertional efficiencies

                              </p>
                            </div>
                          </div>

                        </div>
                      </div>
                      <h3 class="w3-center w3-text-white">Registration Fee : 1290/-</h3><br>
                      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                       <input type="hidden" name="amount" value="1290"/>
                       <input type="hidden" name="wname" value="Data Analytics"/>
                       <?php if(!$hash) { ?>
                         <?php
                                                       include("../db_config.php");
                                                       $result= $con->query("SELECT p_id from workshops WHERE p_id='$_SESSION[p_id]' and event_name='Data Analytics' and status='success'");
                                                        $count=$result->num_rows;
                                                        if($count==0)
                                                        {
                                                          echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                                        }
                                                        else
                                                        {
                                                            echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                                        }
                                                       $con->close();
                                                    ?>
                       <?php } ?>

                     </form>
                   </div>
                 </div>
               </div>
               <br><br><br><br><br>

               <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray text-center">
                <div class="row" style="font-family: 'Comfortaa', cursive;">
                  <div class="col-md-12">
                    <div class="center">
                      <br>
                      <h2><strong>CONTACT</strong></h2>
                      <p style="color:black">In case of any queries or clarifications, please feel
                        free to contact us  at workshops@mohanamantra.com
                      </p>
                    </div>
                  </div>
                </div>
                <div class="row" style="font-family: 'Comfortaa', cursive;">
                  <div class="col-md-4 col-md-offset-2">
                    <div class="center">
                      <h5>LC Gowtham</h5>
                      <h5>+91 80740 73425</h5>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="center">
                      <h5>Keerthana Reddy</h5>
                      <h5>+91 94924 95525</h5>
                    </div>
                  </div>
                </div><br><br><br>
              </div>
              <form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
                <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

                <input type="hidden" name="surl" value="http://mohanamantra.com/workshops/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
                <input type="hidden" name="furl" value="http://mohanamantra.com/workshops/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
                <input type="hidden" name="curl" value="http://mohanamantra.com/workshops/response.php" />

                <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
                <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
                <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
                <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
                <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

              </tr>
            </table>
          </form>
        </body>

        </html>
