<?php
include("../db_config.php");
    include("../functions.php");
     session_start();

  ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Workshops | Mohana Mantra 2K17</title>
    <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
    <meta name="author" content="Avinash Valluru"/>
    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/nprogress.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/w3.css">
     <link rel="stylesheet" href="../css/tabs.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland" rel="stylesheet">

     <script src="../js/jquery.min.js"></script>
     <script type="text/javascript" src="../js/notify.js"></script>
     <script src="../js/nprogress.js"></script>
    <style>
        body {
            background: #212121 url("img/workshops.jpg") no-repeat top center fixed;
            background-size: cover;
            margin: 0;
            padding: 0;
           height: 100%;
            width: 100%;
        }

        figure{
            transform: scale(0.8);
        }
        h1{
                          font-size: 5.5em;
                            font-family: Iceland;

                    }
             @media screen and (max-width: 480px) {
                h1{
                    transform: scale(0.8);
                }
      }
    </style>

</head>
<body>
<script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<?php include("nav.php");?>
             <h1 class="heading w3-text-cyan w3-center" style="padding-top: 48px;" >Workshops</h1>
<div class="container-fluid" >
         <MARQUEE DIRECTION=LEFT class="w3-text-red"><h4 style="font-family: 'Comfortaa', cursive;">Online Registrations are closed Please register on spot..!!!</h4></MARQUEE>
        <figure class="snip1091 red"><img src="img/da.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-orange">Data Analytics</span></h2>
          </figcaption><a href="data"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/iot.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">Internet Of Things (IOT)</span></h2>
          </figcaption><a href="iot"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/apk.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-orange">Android App Design</span></h2>
          </figcaption><a href="android"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/cloud.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">Cloud Computing</span></h2>
          </figcaption><a href="cloud"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/python.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-orange">Python</span></h2>
          </figcaption><a href="python"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/gb.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">Green Buildings</span></h2>
          </figcaption><a href="greenbuilding"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/stad.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-orange">TALL STRUCTURES USING STAAD-PRO</span></h2>
          </figcaption><a href="stadpro"></a>
        </figure>
         <figure class="snip1091 navy"><img src="img/3d.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">3D Printing</span></h2>
          </figcaption><a href="3dprinting"></a>
        </figure>
         <figure class="snip1091 navy"><img src="img/qd.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">quad copter</span></h2>
          </figcaption><a href="quad"></a>
        </figure>
         <figure class="snip1091 navy"><img src="img/solar.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">Solar Automation</span></h2>
          </figcaption><a href="solar"></a>
        </figure>
         <figure class="snip1091 navy"><img src="img/bwc.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-orange">Brain Control ROBOT</span></h2>
          </figcaption><a href="bcr"></a>
        </figure>

          <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p>In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="center">
                                <h5>LC Gowtham</h5>
                                <h5>+91 80740 73425</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <h5>Keerthana Reddy</h5>
                                <h5>+91 94924 95525</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>
</div>

<script>

  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );
</script>
</body>

</html>
