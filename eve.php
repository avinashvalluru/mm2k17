<?php
include("db_config.php");
    include("functions.php");
     session_start();

  ?>   
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Workshops | Mohana Mantra 2K17</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/nprogress.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland" rel="stylesheet">
    
     <script src="js/jquery.min.js"></script>
     <script type="text/javascript" src="js/notify.js"></script>
     <script src="js/nprogress.js"></script>
    <style>
        body {
            background: #212121 url("images/workshops/workshops.jpg") no-repeat top center fixed;
            background-size: cover;
            margin: 0;
            padding: 0;
           
           height: 100%;
            width: 100%; 
        }
         /*neon*/

            .neon p {
                text-shadow: #000 3px 1px 5px;
                text-transform: uppercase;
                font-weight: 300;
                text-align: center;
                font-size: 2.8em;
                margin: 20px 0 20px 0;
            }

            .neon p a {
                text-decoration: none;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                transition: all 0.5s;
            }

            .ered p a {
                color: #FF1177;
            }

            .ered p a:hover {
                -webkit-animation: neon1 1.5s ease-in-out infinite alternate;
                -moz-animation: neon1 1.5s ease-in-out infinite alternate;
                animation: neon1 1.5s ease-in-out infinite alternate;
            }

            .eblue p a {
                color: #228DFF;
            }

            .eblue p a:hover {
                -webkit-animation: neon2 1.5s ease-in-out infinite alternate;
                -moz-animation: neon2 1.5s ease-in-out infinite alternate;
                animation: neon2 1.5s ease-in-out infinite alternate;
            }

            .eyellow p a {
                color: #FFDD1B;
            }

            .eyellow p a:hover {
                -webkit-animation: neon3 1.5s ease-in-out infinite alternate;
                -moz-animation: neon3 1.5s ease-in-out infinite alternate;
                animation: neon3 1.5s ease-in-out infinite alternate;
            }

            .elblue p a {
                color: #44BDF0;
            }

            .elblue p a:hover {
                -webkit-animation: neon4 1.5s ease-in-out infinite alternate;
                -moz-animation: neon4 1.5s ease-in-out infinite alternate;
                animation: neon4 1.5s ease-in-out infinite alternate;
            }

            p a:hover {
                color: #ffffff !important;
            }

            /*glow for webkit*/

            @-webkit-keyframes neon1 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FF1177, 0 0 70px #FF1177, 0 0 80px #FF1177, 0 0 100px #FF1177, 0 0 150px #FF1177;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FF1177, 0 0 35px #FF1177, 0 0 40px #FF1177, 0 0 50px #FF1177, 0 0 75px #FF1177;
                }
            }

            @-webkit-keyframes neon2 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #228DFF, 0 0 70px #228DFF, 0 0 80px #228DFF, 0 0 100px #228DFF, 0 0 150px #228DFF;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #228DFF, 0 0 35px #228DFF, 0 0 40px #228DFF, 0 0 50px #228DFF, 0 0 75px #228DFF;
                }
            }

            @-webkit-keyframes neon3 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FFDD1B, 0 0 70px #FFDD1B, 0 0 80px #FFDD1B, 0 0 100px #FFDD1B, 0 0 150px #FFDD1B;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FFDD1B, 0 0 35px #FFDD1B, 0 0 40px #FFDD1B, 0 0 50px #FFDD1B, 0 0 75px #FFDD1B;
                }
            }

            @-webkit-keyframes neon4 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #44BDF0, 0 0 70px #44BDF0, 0 0 80px #44BDF0, 0 0 100px #44BDF0, 0 0 150px #44BDF0;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #44BDF0, 0 0 35px #44BDF0, 0 0 40px #44BDF0, 0 0 50px #44BDF0, 0 0 75px #44BDF0;
                }
            }

            /*glow for mozilla*/

            @-moz-keyframes neon1 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FF1177, 0 0 70px #FF1177, 0 0 80px #FF1177, 0 0 100px #FF1177, 0 0 150px #FF1177;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FF1177, 0 0 35px #FF1177, 0 0 40px #FF1177, 0 0 50px #FF1177, 0 0 75px #FF1177;
                }
            }

            @-moz-keyframes neon2 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #228DFF, 0 0 70px #228DFF, 0 0 80px #228DFF, 0 0 100px #228DFF, 0 0 150px #228DFF;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #228DFF, 0 0 35px #228DFF, 0 0 40px #228DFF, 0 0 50px #228DFF, 0 0 75px #228DFF;
                }
            }

            @-moz-keyframes neon3 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FFDD1B, 0 0 70px #FFDD1B, 0 0 80px #FFDD1B, 0 0 100px #FFDD1B, 0 0 150px #FFDD1B;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FFDD1B, 0 0 35px #FFDD1B, 0 0 40px #FFDD1B, 0 0 50px #FFDD1B, 0 0 75px #FFDD1B;
                }
            }

            @-moz-keyframes neon4 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #44BDF0, 0 0 70px #44BDF0, 0 0 80px #44BDF0, 0 0 100px #44BDF0, 0 0 150px #44BDF0;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #44BDF0, 0 0 35px #44BDF0, 0 0 40px #44BDF0, 0 0 50px #44BDF0, 0 0 75px #44BDF0;
                }
            }

            /*glow*/

            @keyframes neon1 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FF1177, 0 0 70px #FF1177, 0 0 80px #FF1177, 0 0 100px #FF1177, 0 0 150px #FF1177;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FF1177, 0 0 35px #FF1177, 0 0 40px #FF1177, 0 0 50px #FF1177, 0 0 75px #FF1177;
                }
            }

            @keyframes neon2 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #228DFF, 0 0 70px #228DFF, 0 0 80px #228DFF, 0 0 100px #228DFF, 0 0 150px #228DFF;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #228DFF, 0 0 35px #228DFF, 0 0 40px #228DFF, 0 0 50px #228DFF, 0 0 75px #228DFF;
                }
            }

            @keyframes neon3 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FFDD1B, 0 0 70px #FFDD1B, 0 0 80px #FFDD1B, 0 0 100px #FFDD1B, 0 0 150px #FFDD1B;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FFDD1B, 0 0 35px #FFDD1B, 0 0 40px #FFDD1B, 0 0 50px #FFDD1B, 0 0 75px #FFDD1B;
                }
            }

            @keyframes neon4 {
                from {
                    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #44BDF0, 0 0 70px #44BDF0, 0 0 80px #44BDF0, 0 0 100px #44BDF0, 0 0 150px #44BDF0;
                }
                to {
                    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #44BDF0, 0 0 35px #44BDF0, 0 0 40px #44BDF0, 0 0 50px #44BDF0, 0 0 75px #44BDF0;
                }
            }

           

                    .neon p{
                        font-size: 1.9em !important;
                    }

                    .everaise{
                        margin: 32px 0;
                    }


                    a:hover {
                        color: #ffffff;
                        text-decoration: none;
                    }

                    .everaise {
                        -webkit-transition: all 0.5s ease;
                        -moz-transition: all 0.5s ease;
                        -ms-transition: all 0.5s ease;
                        -o-transition: all 0.5s ease;
                        transition: all 0.5s ease;
                    }

                    .everaise:hover {
                        -webkit-transform: scale(1.1);
                        -moz-transform: scale(1.1);
                        -ms-transform: scale(1.1);
                        -o-transform: scale(1.1);
                        transform: scale(1.1);
                    }
                    h1{
                          font-size: 5.5em;
                            font-family: Iceland;
                    }   
    </style>
    
</head>
<body>
<script type="text/javascript">
  
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<?php include("nav.php");?> 


                <h1 class="heading w3-text-cyan w3-center" style="margin-top:125px;">Workshops</h1>

    <div class="container-fluid neon" style="margin-top:75px;font-family: 'Comfortaa', cursive;">
        
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4"><div class="elblue everaise"><p><a href="computer_science">Computer Science</a></p></div></div>
                <div class="col-md-2"><div class="ered everaise"><p><a href="eee">EEE</a></p></div></div>
                <div class="col-md-2"><div class="eyellow everaise"><p><a href="ece">ECE</a></p></div></div>
                <div class="col-md-4"><div class="elblue everaise"><p><a href="robotics">robotics</a></p></div></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4"><div class="eblue everaise"><p><a href="civil">civil</a></p></div></div>
                <div class="col-md-4"><div class="eyellow everaise"><p><a href="mechanical">mechanical</a></p></div></div>
                <div class="col-md-4"><div class="ered everaise"><p><a href="photography">Photography</a></p></div></div>
            </div>
        </div>
    </div>
<br><br>

</body>

</html>