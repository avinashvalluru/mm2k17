    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Master's Presentation| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body {
              background-image: url(img/code.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;

        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {
          h1{
            transform: scale(0.7);

          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br>
       <h1 class="heading w3-text-pink w3-center" style="" >Master's Presentation</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                “Paper Presentation” is your lucky chance to do so. This is your platform to express yourself, make your views known, explore your ideas and make the best paper presentation ever. Compete with the sharpest minds in the society, lock horns with the best of all while standing your ground!<br>
                      Register right away  .<br><br>
                    <b>Registration : </b>Rs 100/-  <br>
                    <b>Dates : </b>September 25<sup> th</sup> & 26<sup> th</sup><br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>

              ● The abstract MUST contain the Full name of the participants and  along with their Contact Numbers, Course Name (group), College Name and City and topic details.<br>
              ● The SUBJECT of the mail sent should be in the following format: &lt;name of the participant> &lt;course> &lt;Registration id>, &lt;Contact details>,&lt;College Name>,&lt;city>the mails which aren’t in this format will be discarded. <br>
              ● The shortlisted candidates would be informed personally by email.<br>

              ● The shortlisted teams will have to register for the event participation by paying an amount of Rs.100 through Online Banking, the soft copy of the paid slip must be sent to the mppt.mm17@gmail.com<br>

              <b>NOTE :</b> *participants are requested to take care while paying for the event so as not to pay multiple times<br>
              *If payment is done without receiving the confirmation mail that your abstract is selected,in such cases money will not be refunded if abstract is not shortlisted*

             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>CONDITIONS</h3></span></label>
          <div class="content">
            <p>
              ● All participants must bring their college ID cards and ” mohana mantra id” at the time of the event.<br>
              ● ABSTRACT should be submitted in MS word/PDF format.<br>
              ● Upon the selection of abstract, the participant will be intimated through email which will also contain the date and time regarding when a particular team must present their paper.<br>
              ● Participants must send their full paper to the mentioned email address upon selection of abstract <br>
              ● Participants must bring hard copy of their main paper during day of event and also carry their soft copy . Carrying a backup of your soft copy is advised.<br>
              ● Certificate will be issued to participants who have registered to the event and presented the paper.<br>
              ● Decision of the judges is final.<br>
              <b>NOTE :</b> *Criteria for selection of winners will be informed at the time of event
              Mail ids to which abstracts are to be sent are : mppt.mm17@gmail.com

            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>TOPICS</h3></span></label>
          <div class="content">
            <p>
              • Bitcoin<br>
            • Neural Networks<br>
            • Google Tango<br>
            • Web Operating System<br>
            • Data Science<br>
            • Cloud Storage<br>
            • Quantum Computing<br>
            • Business Intelligence and Analyst<br>
            • Cloud Computing<br>
            • Python<br>
            • Green Computing<br>
           And Any IT related topics.


            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mppt.mm17@gmail.com
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>K ANAND</h5>
        <h5>+91 90322 71864</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>V UDAY SAI</h5>
        <h5>+91 89196 35272 </h5>
      </div>
    </div>
  </div><br><br><br>
</div>
</body>
</html>
