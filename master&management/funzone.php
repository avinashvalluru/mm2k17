    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Master's Funzone| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body {
              background-image: url(img/code.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;

        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {
          h1{
            transform: scale(0.7);

          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br>
       <h1 class="heading w3-text-pink w3-center" style="" >Master's Funzone</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>Pot Jumbola</h3></span></label>
            <div class="content">
              <p>
                <b>About:</b><br>
                Pot Jumbola , it is a game like hitting the pot. It resembles the great festival of lord Krishna‘s Krishnastami. Every one feels fun and enjoy this game.<br>
                <b>Game Details:</b><br>
                Boys have 5 chances to hit the pot without blind fold.<br>
                Girls have single chance with blind fold with flexible height.<br><br>
                <b>Registration fee :</b> Rs.50

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>Comment Guru</h3></span></label>
            <div class="content">
              <p>
                <b>About:</b><br>
                Comment guru, is like commenting on any given picture.Picture is displayed on the screen and it may be any picture like Funny.<br>
                articipants should write their comments on the given paper and the best comment is awarded.

                 <br><br>
                <b>Registration fee :</b> Rs.50
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>Slow Cycling</h3></span></label>
          <div class="content">
            <p>
              <b>About:</b><br>
                Participants should ride the cycle slowly with in the given distance in specified time. If the candidate fall down then they will be eliminated

                 <br><br>
                <b>Registration fee :</b> Rs.50
            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>Group Events</h3></span></label>
          <div class="content">
            <p>
              Group events is a combo of multiple games like
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Archery
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Acting
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Bricks lift
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Balloon Rotate
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Balloon Blow
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Knee Ball

            </p>
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mppt.mm17@gmail.com
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>N CHAITANYA</h5>
        <h5>+91 99669 90343</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>K Muragesh </h5>
        <h5>+91 86889 90566 </h5>
      </div>
    </div>
  </div><br><br><br>
</div>
</body>
</html>
