    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>SarvaSreshta| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
              background-image: url(img/management.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;
          
        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
       <h1 class="heading w3-text-pink w3-center" style="" >SarvaSreshta - Young manager</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                Today,  young professionals are carving out prominent places for themselves. While it’s true that opportunities are expanding for today’s young professionals, the  budding manages must possess some peculiar skills like decision making, analytical and managerial skills. our event “young manager” helps you to improve all those skills that are required for your success.<br>
                The “young manager “ event consists of 5 rounds. i.e., Written Test, Group Discussion, Dark Room, Meet the press, Stress Interview
                <br><br>
                    <b>Registration : </b>Rs 200/-  <br>
                    <b>Dates : </b>September 25<sup> th</sup> & 26<sup> th</sup><br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>

              • Event of Individual participation<br>
              • students from different institutions are allowed.<br>
              • Registration fee for each individual is Rs.200.<br>
              • Interested candidates can register for the event by paying the above mentioned amount on the spot.
              • Details of participants like full name, register id, contact number, course group,  college name, city need to be registered.<br>
              • On the spot registration is also available.<br>
              • Participants should bring a hard copy of their updated resume<br>


             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>CONDITIONS</h3></span></label>
          <div class="content">
            <p>
              • All the participants must report to the registration desk by 10 am on September 25th.<br>
              • Participants must not bring cellphones and laptops to the event.<br>
              • The limit of each round in the event may be one hour (expected).<br>
              • Any participant found in indulging in any form of mispractice will be immediately disqualified.<br>
              • The organization committee reserves all the rights to change any of the above rules as they deem fit.<br>
              • Any further modifications in the guidelines will be communicated accordingly and highlighted in the website.<br>
              • All participants must bring MM id, college id on the day of the event.<br>


            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>Judgement & Prizes</h3></span></label>
          <div class="content">
            <p>
                       The participant who excels in all the rounds will be given the title “THE YOUNG MANAGER” along  with the cash prize.<br>
                       certificates for all the participants and the winner. Prizes will be awarded on the second day i.e., 26th September, 2017.

            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mbamohanmantra@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>N.L.N. Siva </h5>
        <h5>+91 91821 08277</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>K. VigneshRaja  </h5>
        <h5>+91 97003 70666 </h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>