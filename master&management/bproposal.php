    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Bussiness Proposal| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
              background-image: url(img/management.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;
          
        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
       <h1 class="heading w3-text-pink w3-center" style="" >Bussiness Proposal</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                To bring out the new enterprising business and social ideas from the participants having innovative thoughts which are technically feasible, commercially viable and socially relevant.The event helps in nurturing and guiding the participant to shape their business startups.<br>
                This event is a golden oppurtunity for the participants to bring out their entrepreneurial talents. 
                <br><br>
                    <b>Registration : </b>Rs 200/-  <br>
                    <b>Dates : </b>September 25<sup> th</sup> & 26<sup> th</sup><br>
                    <b>No of participants </b>: 2 Per team<br>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>RULES</h3></span></label>
            <div class="content">
              <p>

                <br> The participant must attend the event in a “Formal dress code”
                <br> No of participants : 2 Per team
                <br> The team has to submit a hardcopy of the proposal.
                <br>  The team has to prepare  the business proposal in the ppt mode
                <br> The number of slides should be 10-15
                <br> The time for each presentation is 10 minutes for each team 



             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>TOPICS</h3></span></label>
          <div class="content">
            <p>
              1.  Name of the Company<br>
              2.  Vision and Mission<br>
              3.  Type of Business Enterprise<br>
              4.  Location<br> 
              5.  Capital Structure<br>
              6.  Build a business outline highlighting innovative product or service benefits<br>
              7.  Competitors and Target users<br>
              8.  Development Programs of Government<br>
              9.  Financial Requirements<br>
              10.Promotional strategies<br>
              11.Financial Performance (Break even point,5years trend projection)<br>
              12.Management Tactics.<br>
            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>No of Rounds</h3></span></label>
          <div class="content">
            <p>
                       This event consists of 4 rounds<br>
                        Round 1: Submission of Business proposal<br>
                        Round 2: Proposal Presentation<br>
                        Round 3: Project Simulation(Role play/Promotional Activity)<br>
                        Round 4: Feed Back<br>


            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mbamohanmantra@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>J.Dimple Monika  </h5>
        <h5>+91 91773 59778</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>H.Harisree  </h5>
        <h5>+91 94914 52736 </h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>