    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Master's Mind| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
              background-image: url(img/code.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;
          
        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
       <h1 class="heading w3-text-pink w3-center" style="" >Master's Mind</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
               Exuberating, motivating, enlightening and engaging would describe the quiz competition in our Mohan mantra. It's a pleasure for brain and a food for thought. It can accomplish anything from being ones past time to bigger things like spreading awareness.<br>
                Let us test your knowledge with quiz event here at MM2k17.<br><br>

                • MASTER’S MIND consist of IT related and programming language questions.<br>
                • The time limit for this event is 60 minutes.(event continues throughout the day).<br>
                •  There will be at most 12 teams will be allowed to participate at a time. <br>
                •  The questions will be projected on the screen and choose correct option from it.<br>
                •  It consist of 3 rounds.<br>
                &nbsp;&nbsp;&nbsp;Round 1 : each question consist of 3 points and time limit will be 10sec<br>
                &nbsp;&nbsp;&nbsp;Round 2 : each question consist of 5 points and time limit will be 15sec<br>
                &nbsp;&nbsp;&nbsp;Round 3 : each question consist of 10 points and time limit will be 25sec<br>

                    <b>Registration : </b>Rs 100 per team <br>
                    <b>Dates : </b>September 25<sup> th</sup> & 26<sup> th</sup><br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                • Each team can have at most 2 members.<br>
                • Any no. of teams can come from a college.<br>
                • Spot registrations are available.<br>
                • Interested candidates can also register for event by paying the above mentioned amount through online banking.<br>
                • The soft copy of the receipt should be mailed to mca.quizmm17@gmail.com<br>
                • Details of all team members like full names, Registration id's, contact numbers, course name(group), college name, and city should be sent along with the payment receipt.<br>


             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>CONDITIONS</h3></span></label>
          <div class="content">
            <p>
              • All participants must bring their MM id cards on the day of event.<br>
              • Any kind of misbehaviour/malpractice by the participants will directly lead to disqualification of the team.<br>
              • Arguments at any cost are not accepted decision of the quiz team is final.<br>
              • The organizing committee had the rights to change the above rules as they deem fit. Change in rules if any will be highlighted on the website.<br>


            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>PRIZES</h3></span></label>
          <div class="content">
            <p>
              • The prize money differs based on the winning criteria<br>
              • Amazing 3 prizes will be given<br>



            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mca.quizmm17@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>A GANESH</h5>
        <h5>+91 83094 95513</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>K Muragesh </h5>
        <h5>+91 86889 90566 </h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>