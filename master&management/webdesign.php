    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>WEB - Designing | Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
          background-image: url(img/code.jpg) ;
          background-position: center center;
          background-repeat:  no-repeat;
          background-attachment: fixed;
          background-size:  cover;
          background-color: #999;
          
        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
      <h1 class="heading w3-text-pink w3-center" style="" >WEB - Design </h1><br>
      <div class="container">
       <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="left-menu">
            <div class="accordion">
              <div class="section">
                <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
                <label for="section-1"><span><h3>ABOUT</h3></span></label>
                <div class="content">
                  <p>
                   To expose your creativity and innovative skills on web designing for this we are conducting a event “WEB DESIGNING”. This is a great opportunity to utilize and show your ability on web design skills. So gear up to do some designing!!!  <br>Registrations are now open.<br>
                   <b>Registration : </b>Rs.100/- per team. <br>
                   <b>Dates : </b>September 25<sup> th</sup> & 26<sup> th</sup><br>
                   <b>Timings : </b>11:30AM - 1:00PM <br>

                 </p>
               </div>
             </div>
             <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
              <div class="content">
                <p>
                  ● Team of maximum 2 are allowed.<br>
                  ● Team formed by students of different institutions are also allowed.<br>
                  ● Registration fee for each team is 100/-<br> 
                  ● Interested candidates can register for the event by paying above mentioned amount through online banking.<br>
                  ● The soft copy of the payment receipt is to be mailed to webdesignmm2k17@gmail.com<br>
                  ● Details of all the team members like full names and registration id's along with their contact numbers,course name(group),college name and city need to be sent along with payment receipt.<br>
                  ● On the spot registrations are available.<br>

                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
              <label for="section-3"> <span><h3>CONDITIONS</h3></span></label>
              <div class="content">
                <p>
                  ● All the participants must report to the respective labs by 10:30AM.<br>
                  ● Participants should use the programming language HTML & CSS Only.<br>
                  ● Time limit of the event is 90 minutes.<br>
                  ● Output must be in the format specified. And should satisfy all the test cases.<br>
                  ● Any participant found to be indulging in any form of malpractice will be immediately disqualified.<br>
                  ● The organizing committee reserves all the rights to change any of the above rules as they deem fit. Any further modification in the guidelines will be conveyed accordingly and highlighted on the website.

                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
              <label for="section-4"> <span><h3>JUDGMENT CRITERIA & PRIZES</h3></span></label>
              <div class="content">
                <p>
                 <b>JUDGMENT CRITERIA</b><br>
                 ● The team which executes maximum marks & attractive design within the given time wins the event.<br>
                 ● Thecandidate which completes the task efficiently in minimum possible time will be declared as a winner
                 <br><br>
                 <b>PRIZES</b>
                 <br>● Certificates for all the participants and winners.
                 <br>● Prizes will be awarded on second day i.e.sept 26th.
                 <br>● Amazing  2  prizes will be given



               </p>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>     
 </div>
 <br><br><br><br><br>
 <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;webdesignmm2k17@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>C A HAREESH</h5>
        <h5>+91 97006 02488</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>K MURAGESH </h5>
        <h5>+91 86889 90566 </h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>