    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Bussiness Quiz| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
              background-image: url(img/management.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;
          
        }

        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
       <h1 class="heading w3-text-pink w3-center" style="" >Bussiness Quiz</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                Motivating, Enlightment  and engaging would describe the quiz competition in our Mohan mantra. It can accomplish anything from being ones past time to bigger things like spreading awareness.<br>
                Let us test your knowledge with quiz event here at MM2K17.<br>
                • Business quiz is a form of game which related to business field in which team attempt to answer questions correctly<br>
                • Business quiz consists of 4 rounds and the time limit for each round is minimum 30 minutes (event continuous for 2 days)<br>
               <br><br>
                    
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
          • Each team consists of only two members<br>
          • No limit for teams from each college<br>
          • Spot registrations are available<br>
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>CONDITIONS</h3></span></label>
          <div class="content">
            <p>
              • Without Mohan mantra ID cards the participants are not allowed to this event.<br>
              • Any kind of misbehaviour/malpractice by the participants will directly lead to dis-qualification of the team.<br>
              • Arguments at any cost are not accepted, decision of the quiz team is final.<br>
              • The organizing committee has the rights to change the above rules as they deem fit. Change in rules if any will be highlighted on the website.<br>
              • Usage of mobiles for participants in this event is strictly prohibited.<br>

            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>No of Rounds</h3></span></label>
          <div class="content">
            <p>
                       •  The rounds are:<br>
                Round 1 (written test): It consist of brand names, abbreviations, headquarters, current affairs and cross search puzzle.<br>
                Round 2 (Identification round): It related to identifying of app logos, parental companies, CEOs, Books of CEO and founders of companies.<br>
                Round 3(Visual round): It related to Mascots, Brand ambassadors, company logos, connection round, Ad identification.<br>
                Round 4 (Rapid Fire): It consist of 10 questions per team and time limit is one minute.



            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;mbamohanmantra@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>R. Dheeraj   </h5>
        <h5>+91 89781 42314</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>D. Ramesh   </h5>
        <h5>+91 85005 95213 </h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>