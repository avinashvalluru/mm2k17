var currPage=0;
var isAnimating=false;
var musicOn=true;
window.onload = function(){
	
	
	var bLazy = new Blazy();
	isAnimating=true;
	var headline = document.querySelector('#text0');
			segmenter = new Segmenter(document.querySelector('#segment_0'), {
					onReady: function() {
						segmenter.animate();
						headline.classList.remove('trigger-headline--hidden');
						isAnimating=false;
					}
				});
	$("#date_0").css({opacity:"1"});
	$("#img_1").css({opacity:"0"});
	$("#ps_0").removeClass( "w3-hide" )
	$(document).keydown(function(e) {
		if(!isAnimating){
			isAnimating=true;
			var nextPage;
			switch(e.which) {
			    case 39:
			    	nextPage=(currPage+1)%2;
			    break;

			    case 37:
			   		nextPage=(currPage-1)%2;
			   		if(nextPage<0){
			   			nextPage=1;
			   		}
			    break;

			    default:
			    	isAnimating=false;
			     return;
			}
			$("#text"+currPage).addClass('trigger-headline--hidden');
			$("#date_"+currPage).css({opacity:"0"});
			$("#img_"+currPage).css({opacity:"0"});
			$("#ps_"+currPage).addClass( "w3-hide" )
  			$("#song_"+currPage).get(0).pause();
			var curr=currPage;
			setTimeout(function(){
				moveSlide(curr,nextPage);
			},1000);
			currPage = nextPage;
		}
	});
	
	$(".prev").click(function(){
		if(!isAnimating){
			isAnimating=true;
			var nextPage=(currPage-1)%2;
			if(nextPage<0){
				nextPage=1;
			}
			$("#text"+currPage).addClass('trigger-headline--hidden');
			$("#date_"+currPage).css({opacity:"0"});
			$("#img_"+currPage).css({opacity:"0"});
			$("#ps_"+currPage).addClass( "w3-hide" )
	  		$("#song_"+currPage).get(0).pause();
			var curr=currPage;
			setTimeout(function(){
				moveSlide(curr,nextPage);
			},1000);
			currPage = nextPage;
		}
	});
	$(".next").click(function(){
		if(!isAnimating){
			isAnimating=true;
			var nextPage=(currPage+1)%2;
			$("#text"+currPage).addClass('trigger-headline--hidden');
			$("#date_"+currPage).css({opacity:"0"});
			$("#img_"+currPage).css({opacity:"0"});
			$("#ps_"+currPage).addClass( "w3-hide" )
	  		$("#song_"+currPage).get(0).pause();
			var curr=currPage;
			setTimeout(function(){
				moveSlide(curr,nextPage);
			},1000);
			currPage = nextPage;
		}
	});
}

$(document).ready(function(){
	
  $("#bars").click(function(){
  	if(musicOn){
  		$(".bar").removeClass('music');
  		musicOn=false;
  		$("#song_"+currPage).get(0).pause();
  	}else{
  		$(".bar").addClass('music');
  		musicOn=true;
  		$("#song_"+currPage).get(0).play();
  	}
  })

})
function moveSlide(currPage,nextPage){
	$("#page"+currPage).css({width:0});
		$("#segment_"+currPage).css({width:0});
		$("#page"+nextPage).css({width:'100vw'});
		$("#segment_"+nextPage).css({width:'100vw'});
		$("#date_"+nextPage).css({opacity:"1"});
		$("#img_"+nextPage).css({opacity:"1"});
		$("#ps_"+nextPage).removeClass( "w3-hide" )
		if(nextPage==0){
			$(".bar").css({"background-color":"#fff"});
			$("nav span").addClass('white');
			$("nav span").removeClass('yellow');
			$("nav span").removeClass('black');
			$("nav i").addClass('whiter');
			$("nav i").removeClass('yellower');
			$("nav i").removeClass('blacker');
			var headline = document.querySelector('#text0');
			segmenter = new Segmenter(document.querySelector('#segment_0'), {
					onReady: function() {
						segmenter.animate();
						headline.classList.remove('trigger-headline--hidden');
					}
				});
		}else{
			$(".bar").css({"background-color":"#fbe094"});
			$("nav span").removeClass('white');
			$("nav span").removeClass('black');
			$("nav span").addClass('yellow');
			$("nav i").removeClass('whiter');
			$("nav i").removeClass('blacker');
			$("nav i").addClass('yellower');
			var headline = document.querySelector('#text1')
			segmenter = new Segmenter(document.querySelector('#segment_1'), {
					pieces: 4,
					animation: {
						duration: 1500,
						easing: 'easeInOutExpo',
						delay: 100,
						translateZ: 100
					},
					parallax: true,
					positions: [
						{top: 0, left: 0, width: 45, height: 45},
						{top: 55, left: 0, width: 45, height: 45},
						{top: 0, left: 55, width: 45, height: 45},
						{top: 55, left: 55, width: 45, height: 45}
					],
					onReady: function() {
						segmenter.animate();
						headline.classList.remove('trigger-headline--hidden');
					}
				});
	}
	if(musicOn){
		$("#song_"+nextPage).get(0).play();
	}
	isAnimating=false;
}
