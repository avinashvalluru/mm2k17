<?php
 if(!isset($_POST["status"]))
 {
  header("location:index");
 }
  $status=$_POST["status"];
  $firstname=$_POST["firstname"];
  $amount=$_POST["amount"]; //Please use the amount value from database
  $txnid=$_POST["txnid"];
  $posted_hash=$_POST["hash"];
  $key=$_POST["key"];
  $productinfo=$_POST["productinfo"];
  $email=$_POST["email"];
  $salt="H5xwGsQ6"; //Please change the value with the live salt for production environment
  $error=$_POST["error"];;


//Validating the reverse hash
If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

                  }
  else {

        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
     $hash = hash("sha512", $retHashSeq);
 ?>
<!DOCTYPE html>
<html>
<head>
<title>Mohana Mantra'17</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/w3.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<style type="text/css">
  @media print {
  #home,#link {
    display: none;
  }
}
</style>
</head>
<body>
<?php
 if ($hash != $posted_hash) {
         echo ' <div class="alert alert-danger text-center">
          <strong>Error occured : Transaction failed</strong></a>.
        </div>';
       }
  else
  {



     echo '
     <div class="row">
     <center><img class="w3-image" width="200" height="200" id="logo" src="../images/logomm.png" ><span class="w3-hide-large"></center>
<div class="col-md-8 col-md-offset-2" style="margin-top:35px;">

  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title text-center">Payment Slip For Proshows</h3>
    </div>
    <div class="panel-body text-center">
      <div class="panel panel-primary">
        <table class="table table-bordered table-striped table-hover">
          <tbody>
            <tr>
              <td>Registration_ID</td>
              <td>'. $firstname .'</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>'.$email.'</td>
            </tr>
            <tr>
              <td>Event</td>
              <td>'.$productinfo.'</td>
            </tr>
            <tr>
              <td>Payment Status</td>
              <td>'.$status.'</td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>'.$amount.'(Incl. Tax)</td>
            </tr>
            <tr>
              <td>Txn Id</td>
              <td>'.$txnid.'</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</div>
  </div>
</div>';
echo '<div class="row">';
    include("../db_config.php");
      $stmt= $con->prepare("INSERT INTO proshows (transaction_id,p_id,email,event_name,amount,status) values(?,?,?,?,?,?)");
        $stmt->bind_param('ssssss',$txnid, $firstname,$email,$productinfo,$amount,$status);
        if($stmt->execute())
        {
          //echo "$amount";
           if($status=="success"){
               echo ' <div col-md-8 col-md-offset-2> <div class="alert alert-success text-center">
          <strong>Booking Successfully Done</strong></div>.
        </div>';
         }
          else
          {
                 echo '<div col-md-8 col-md-offset-2> <div class="alert alert-warning text-center">
                  <strong>Booking failed </strong>.<br> <strong>ErrorNo: '.$error.'</strong></div>
                </div>';
          }


        }
        else
        {
             echo '<div col-md-8 col-md-offset-2> <div class="alert alert-warning text-center">
          <strong>Booking Failed</strong>.</div>
        </div>';

        }
         echo '<center><a id="link" href="index"> <button type="button" id="home" class="btn btn-primary">close</button></a>
                </center>';
  }

 $con->close();

?>

</div>
</body>
</html>
