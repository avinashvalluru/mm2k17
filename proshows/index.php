    <?php
    session_start();
    include("../db_config.php");
    include("../functions.php");


?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            //$amt=300;
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$_POST['lcost'];
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }


        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();


}

?>

 <!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

	    <title>Proshows| Mohana Mantra'17</title>
	    <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
		<meta name="theme-color" content="#000000">
	    <meta name="author" content="Avinash Valluru"/>
	    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
	    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
		<link rel="icon" type="image/png" href="../images/mm.png" sizes="16x16"/>
		<link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/check.css" />
		<link rel="stylesheet" type="text/css" href="css/arrows.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<link rel="stylesheet" type="text/css" href="../css/w3.css" />
		<link rel="stylesheet" type="text/css" href="../css/icons.min.css" />


	     <script type="text/javascript" src="../js/notify.js"></script>


		<script src="js/jquery.js"></script>
		<script src="js/blazy.min.js"></script>
		<script src="js/demo.js"></script>
		<script>
		document.documentElement.className = 'js no-touch csstransitions';
		</script>
		<style type="text/css">
				      @media screen and (max-width: 480px) {
					                #img_0{
						position:absolute;
					    width:592px; /*image width */
					    height:512px; /*image height */
					    left:50%;
					    top:50%;
					    margin-left:-296px; /*image width/2 */
					    margin-top:-356px; /*image height/2 */
					    transform: scale(0.7);

					}
						                #img_1{
						position:absolute;
					    width:592px; /*image width */
					    height:512px; /*image height */
					    left:50%;
					    top:50%;
					    margin-left:-296px; /*image width/2 */
					    margin-top:-316px; /*image height/2 */
					    transform: scale(0.3);

					}
				}
					*,
					*:before,
					*:after {
					  -webkit-box-sizing: border-box;
					  -moz-box-sizing: border-box;
					  box-sizing: border-box;
					}
					.button {
					  display: inline-block;
					  position: relative;
					  margin: 1em;
					  padding: 0.67em;
					  border: 2px solid #FFF;
					  overflow: hidden;
					  text-decoration: none;
					  font-size: 2em;
					  outline: none;
					  color: #FFF;
					  background: rgba(0,0,0,0.7);
					  font-family: 'raleway', sans-serif;
					}

					.button span {
					  -webkit-transition: 0.6s;
					  -moz-transition: 0.6s;
					  -o-transition: 0.6s;
					  transition: 0.6s;
					  -webkit-transition-delay: 0.2s;
					  -moz-transition-delay: 0.2s;
					  -o-transition-delay: 0.2s;
					  transition-delay: 0.2s;
					}

					.button:before,
					.button:after {
					  content: '';
					  position: absolute;
					  top: 0.67em;
					  left: 0;
					  width: 100%;
					  text-align: center;
					  opacity: 0;
					  -webkit-transition: .4s,opacity .6s;
					  -moz-transition: .4s,opacity .6s;
					  -o-transition: .4s,opacity .6s;
					  transition: .4s,opacity .6s;
					}

					/* :before */
					.button:before {
					  content: attr(data-hover);
					  -webkit-transform: translate(-150%, 0);
					  -moz-transform: translate(-150%, 0);
					  -ms-transform: translate(-150%, 0);
					  -o-transform: translate(-150%, 0);
					  transform: translate(-150%, 0);
					}

					/* :after */
					.button:after {
					  content: attr(data-active);
					  -webkit-transform: translate(150%, 0);
					  -moz-transform: translate(150%, 0);
					  -ms-transform: translate(150%, 0);
					  -o-transform: translate(150%, 0);
					  transform: translate(150%, 0);
					}

					/* Span on :hover and :active */
					.button:hover span,
					.button:active span {
					  opacity: 0;
					  -webkit-transform: scale(0.3);
					  -moz-transform: scale(0.3);
					  -ms-transform: scale(0.3);
					  -o-transform: scale(0.3);
					  transform: scale(0.3);
					}

					/*
					    We show :before pseudo-element on :hover
					    and :after pseudo-element on :active
					*/
					.button:hover:before,
					.button:active:after {
					  opacity: 1;
					  -webkit-transform: translate(0, 0);
					  -moz-transform: translate(0, 0);
					  -ms-transform: translate(0, 0);
					  -o-transform: translate(0, 0);
					  transform: translate(0, 0);
					  -webkit-transition-delay: .4s;
					  -moz-transition-delay: .4s;
					  -o-transition-delay: .4s;
					  transition-delay: .4s;
					}

					/*
					  We hide :before pseudo-element on :active
					*/
					.button:active:before {
					  -webkit-transform: translate(-150%, 0);
					  -moz-transform: translate(-150%, 0);
					  -ms-transform: translate(-150%, 0);
					  -o-transform: translate(-150%, 0);
					  transform: translate(-150%, 0);
					  -webkit-transition-delay: 0s;
					  -moz-transition-delay: 0s;
					  -o-transition-delay: 0s;
					  transition-delay: 0s;
					}

		</style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body class="demo-2">
 <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
<?php include("nav.php");?>

	<div class="container" id="page0" >
		<div class="segmenter" id="segment_0" style="background-image: url(img/9.png);"></div>
			<h2 class="trigger-headline trigger-headline--hidden" id="text0" style="font-size: 5vw;padding: 10px;"><!--<span style="padding: 0 1vw;">T</span><span style="padding: 0 1vw;">H</span><span style="padding: 0 1vw;">E</span><span style="padding: 0 1vw;"> &nbsp;&nbsp;&nbsp;</span><span style="padding: 0 1.75vw 0 1vw;">L</span><span style="padding: 0 1vw 0 1.75vw;">O</span><span style="padding: 0 1vw;">C</span><span style="padding: 0 1vw;">A</span><span style="padding: 0 1.75vw 0 1vw;">L</span><span style="padding: 0 1vw 0 1.75vw;">T</span><span style="padding: 0 1vw;">R</span><span style="padding: 0 1.75vw 0 1vw;">A</span><span style="padding: 0 1vw 0 1.75vw;">I</span><span style="padding: 0 1vw;">N</span><span style="padding: 0 1vw;"></span><span style="padding: 0 1vw;"></span>--></h2>
			<h5 class="trigger-headline trigger-headline--hidden w3-text-white" style="font-family: 'Comfortaa', cursive;"  id="date_0"></h5>

			<img src="img/land.png" id="img_0" class="trigger-headline  " >
			<audio class="b-lazy" id='song_0' style="position: absolute;top: 0;left: 0;z-index: 200" autoplay loop>
			  <source src="songs/local.mp3" type="audio/mpeg">
			</audio>
			<form id="form_0" class="w3-display-bottommiddle" action="index.php" method="POST">

				<input type="hidden" name="ename" value="Local Train">
				<input type="hidden" name="lcost" value="300">
				<!--<button type="submit" name="submit" class=" w3-button w3-black w3-text-white w3-border w3-border-pink w3-round-large w3-margin">Book Tickets Now!<br>Rs 300/-</button><br><br><br>-->
				<button class="button w3-hide" id="ps_0" type="submit" name="reg" data-hover="₹ 300/-" data-active="I'M ACTIVE"><span>Book Tickets Now</span></button><br>
			</form>

		</div>
    
	<div class="container" id="page1" >
			<div class="segmenter" id="segment_1" style="background-image: url(img/13.png)"></div>
			<h2 class="trigger-headline trigger-headline--hidden" id="text1" style="color:#202425;font-size: 5vw"><!--<span style="padding: 0 1vw;">S</span><span style="padding: 0 1vw;">I</span><span style="padding: 0 1vw;">A</span><span style="padding: 0 1vw;">N</span><span style="padding: 0 1.75vw 0 1vw;">A</span><span style="padding: 0 1vw 0 1.75vw;">C</span><span style="padding: 0 1vw;">A</span><span style="padding: 0 1vw;">T</span><span style="padding: 0 1vw;">H</span><span style="padding: 0 1vw;">E</span><span style="padding: 0 1vw;">R</span><span style="padding: 0 1vw;">I</span><span style="padding: 0 1vw;">N</span><span style="padding: 0 1vw;">E</span>--></h2>

			<h5 class="trigger-headline " style="color:black; margin-top:-35px; font-weight: bolder;" id="date_1">26<sup>th</sup> september</h5>
			<img src="img/ps.png" id="img_1" class="trigger-headline hidden">
			<audio class="b-lazy" id='song_1' style="position: absolute;top: 0;left: 0;z-index: 200" loop>
			  <source src="songs/ps.mp3" type="audio/mpeg">
			</audio>
			<form id="form_1" class="w3-display-bottommiddle" action="index.php" method="POST">

				<input type="hidden" name="ename" value="Paris and Simo">
				<input type="hidden" name="cost" value="300">
				<!--<button type="submit" name="submit" class=" w3-button w3-black w3-text-white w3-border w3-border-pink w3-round-large w3-margin">Book Tickets Now!<br>Rs 300/-</button><br><br><br>-->
				<button class="button w3-hide" id="ps_1" type="submit" name="reg" data-hover="₹ 300/-" data-active="I'M ACTIVE"><span>Book Tickets Now</span></button><br>
			</form>
	</div>
	<nav>
		<span class="prev white"><i class="whiter"></i></span>
		<span class="next white"><i class="whiter"></i></span>
	</nav>

	    <div id="bars" style="position: fixed;bottom: 5vh;left: 10vw;">
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
			<div class="music bar"></div>
		</div>
		<script src="js/anime.min.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/main.js"></script>
     <form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/proshows/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/proshows/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/proshows/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" />

    </form>
	</body>

</html>
