<?php

   include('acc_session_check.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accommodation | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
  .band
  {
    background-color:rgba(150,0,200,0.7);
  }
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo  $_SESSION['login_mem_id'] ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                           <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="">
                        <a href="online.php"><i class="fa fa-fw fa-dashboard"></i> Online Registration</a>
                    </li>
                    <li class="">
                        <a href="spot.php"><i class="fa fa-fw fa-dashboard"></i> Spot Registration</a>
                    </li>
                    <li class="active">
                        <a href="bview.php"><i class="fa fa-fw fa-dashboard"></i> View Boys Accommodation</a>
                    </li>
                    <li class="">
                        <a href="gview.php"><i class="fa fa-fw fa-dashboard"></i> View Girls Accommodation</a>
                    </li>
                    <li class="">
                        <a href="oboys.php"><i class="fa fa-fw fa-dashboard"></i> Online Boys Registration</a>
                    </li>
                    <li class="">
                        <a href="ogirls.php"><i class="fa fa-fw fa-dashboard"></i>Online Girls Registration</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

           <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                       <h1 class="page-header">
                           View Boys Accommodation<small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i>MMID Search
                            </li>
                        </ol>
                    </div>

                <div class="col-md-12  panel panel-default breadcrumb">

                   <?php
                          $result = $con->query("SELECT * FROM acc_issue WHERE  gender='male'");
                                $count=$result->num_rows;
                                //  echo "$count";
                        if($count>=1)
                        {
                   ?>
                             <div class="row well" >
                                   <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Registration ID</th>
                                                    <th>MM_ID</th>
                                                    <th>Full Name</th>
                                                    <th>Col Name</th>
                                                    <th>Mobile</th>
                                                    <th>From Date</th>
                                                    <th>To Date</th>
                                                    <th>amount</th>
                                                    <th>Reg_Mode</th>
                                                    <th>Txn_id</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php
                                    while($row=$result->fetch_assoc())
                                    {
                                        $id=$row['p_id'];
                                        $query = $con->query("SELECT * FROM participants  WHERE p_id='$id'");
                                        $res=$query->fetch_assoc();
                                        $query1 = $con->query("SELECT * FROM mmid_issue  WHERE reg_id='$id'");

                                        $co=$query1->num_rows;
                                        $mmid='';         
                                       if($co==1)
                                       {
                                        $res1=$query1->fetch_assoc();
                                        $mmid=$res1['mm_id'];
                                       }
                                           echo '
                                                  <tr>
                                                    <td>'.$row['p_id'].'</td>
                                                    <td>'.$mmid.'</td>
                                                    <td>'.$res['full_name'].'</td>
                                                    <td>'.$res['col_name'].'</td>
                                                    <td>'.$res['mobile'].'</td>
                                                    <td>'.$row['adate'].'</td>
                                                    <td>'.$row['vdate'].'</td>
                                                    <td>'.$row['amount'].'</td>
                                                    <td>'.$row['mode'].'</td>
                                                    <td>'.$row['txn_id'].'</td>
                                                </tr>';
                                        }

                               }
                               else
                                {
                             echo '<div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-info-circle"></i>  <strong>No Results Found
                                        </div>
                                    </div>';
                                }



                    ?>
                    </tbody>
                   </table>
             </div>
             </div>

                </div>
           </div>




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
