<?php
 function validateFormData( $formData )
    {

        $formData = trim( stripslashes( htmlspecialchars( strip_tags( str_replace( array( '(', ')' ), '', $formData ) ), ENT_QUOTES ) ) );



        return $formData;

    }
    function tempId() {
    $result = '';
    $length=7;
    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return 'TM'.$result;
  }
  function getRandomString($length = 8) {
    $characters = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

?>