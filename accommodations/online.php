<?php

   include('acc_session_check.php');
   if(isset($_POST['submit']))
    {
          //echo '<script>alert("hii")</script>';

          $pid=$_POST['p_id'];
          $gender=$_POST['gender'];
          $adate=$_POST['adate'];
          $vdate=$_POST['vdate'];
          $txn=$_POST['txn_no'];
          $amount=$_POST['amount'];
          $mode=$_POST['reg_mode'];
          $issue_by=$_SESSION['login_id'];
          $stmt= $con->prepare("INSERT INTO acc_issue (p_id,gender,adate,vdate,txn_id,amount,mode,issue_by) values(?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssss',$pid,$gender,$adate,$vdate,$txn,$amount,$mode,$issue_by);
        if($stmt->execute())
        {
             $msg = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Issued Succesfully</strong>
                        </div>';

       }
       elseif (mysqli_errno($con)==1062) {
         # code...
        $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Already Registered with that transaction Number</strong>
                        </div>';
       }
       else
       {
            $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Issue Failed Tryagain</strong>
                        </div>';
       }
   }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accommodation | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
  .band
  {
    background-color:rgba(150,0,200,0.7);
  }
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo  $_SESSION['login_mem_id'] ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                           <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="active">
                        <a href="online.php"><i class="fa fa-fw fa-dashboard"></i> Online Registration</a>
                    </li>
                    <li class="">
                        <a href="spot.php"><i class="fa fa-fw fa-dashboard"></i> Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="bview.php"><i class="fa fa-fw fa-dashboard"></i> View Boys Accommodation</a>
                    </li>
                    <li class="">
                        <a href="gview.php"><i class="fa fa-fw fa-dashboard"></i> View Girls Accommodation</a>
                    </li>
                    <li class="">
                        <a href="oboys.php"><i class="fa fa-fw fa-dashboard"></i> Online Boys Registration</a>
                    </li>
                    <li class="">
                        <a href="ogirls.php"><i class="fa fa-fw fa-dashboard"></i>Online Girls Registration</a>
                    </li>

                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

           <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       <?php
                           if($msg){?>
                                <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <?php echo $msg ?>

                                        </div>
                                    </div>
                       <?php $msg=""; } ?>
                       <h3 class="page-header">
                           Online  Accommodation<small></small>
                        </h3>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i>Online Registrations
                            </li>
                        </ol>
                    </div>

                <div class="col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                  <h3 style="color:green" class="text-center">Enter Registration Number</h3>
                <form class="form-horizontal panel-body" action="online.php" method="GET">
                    <div class="form-group input-group">
                        <input type="text" name="reg_id" placeholder="Enter Registration Id" class="form-control">
                        <span class="input-group-btn"><button class="btn btn-default" type="submit" name="search"><i class="fa fa-search"></i></button></span>
                    </div>
                </form>
               <?php  if(isset($_GET['search']))
                           {

                               $id = $_GET['reg_id'];

                               $sql = "SELECT * FROM participants p, acc a WHERE p.p_id=a.p_id and a.status='success' and p.p_id='$id'";
                               $result = $con->query($sql);
                               $count=$result->num_rows;

                               if($count==1)
                               {
                                    $row=$result->fetch_assoc();
                                    $input=$row['p_id'];

                                    $result1=$con->query("SELECT * FROM mmid_issue where reg_id='$input'");
                                    $count1=$result1->num_rows;
                                    //echo "$count1";
                                    $mmid='';
                                   if($count1==1)
                                   {
                                     $res1=$result1->fetch_assoc();
                                    $mmid=$res1['mm_id'];
                                   }

                                   echo '<form class="form-horizontal panel-body" method="POST" action="online.php">

                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Reg_ID</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  minlength="6" maxlength="50" required="required" class="form-control" name="p_id"  value="'.$row['p_id'].'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">MM ID</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  minlength="6" maxlength="50" required="required" class="form-control" name="mm_id"  value="'.$mmid.'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Gender</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  minlength="6" maxlength="50" required="required" class="form-control" name="gender"  value="'.$row['gender'].'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="fullname" class="col-sm-2 control-label">FullName</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="4" maxlength="50" required="required" class="form-control" id="fullname" name="fullname" placeholder="FullName" value="'.$row['full_name'].'" readonly>
                                                </div>
                                                </div>

                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                  <input type="email"  minlength="6" maxlength="50" required="required" class="form-control" name="email" placeholder="Email" value="'.$row['email'].'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">College </label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="2" maxlength="70" required="required" class="form-control" name="clg" placeholder="College Name" value="'.$row['col_name'].'" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="10" maxlength="10" class="form-control" name="mobile" required placeholder="Enter 10 digit mobile num" value="'.$row['mobile'].'" onkeypress="return isNumber(event)" readonly>
                                                </div>
                                              </div>


                                               <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Fromdate</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="adate" placeholder="From Date" value="'.$row['adate'].'/9/2017" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                                 </div>
                                                 <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Todate</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="vdate" placeholder="To date" value="'.$row['vdate'].'/9/2017" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                                 </div>
                                                 <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Txn_no</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number" value="'.$row['transaction_id'].'" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                                 </div>
                                                 <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Amount</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="amount" placeholder="Amount" value="'.$row['amount'].'" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                                 </div>
                                                 <input type="hidden" name="reg_mode" value="Online">
                                                 <div class="form-group">
                                                  <div class="col-sm-offset-2 col-sm-10">
                                                    <input class="btn btn-primary" type="submit" name="submit" value="Register">
                                                  </div>
                                                </div>
                                        </form>';
                               }
                            else
                            {
                              echo '<div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-info-circle"></i>  <strong>No Results Found
                                        </div>
                                    </div>';
                              }
                    }
                   ?>
            </div>
           </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
