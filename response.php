<?php
session_start();
include("db_config.php");
 if(!isset($_POST["status"]))
 {
  header("location:accommodation");
 }
  $status=$_POST["status"];
  $firstname=$_POST["firstname"];
  $amount=$_POST["amount"]; //Please use the amount value from database
  $txnid=$_POST["txnid"];
  $phone=$_POST["phone"];
  $posted_hash=$_POST["hash"];
  $key=$_POST["key"];
  $productinfo=$_POST["productinfo"];
  $gender=$_POST["udf1"];
  $adate=$_POST["udf2"];
  $atime=$_POST["udf3"];
  $vdate=$_POST["udf4"];
  $vtime=$_POST["udf5"];
  $email=$_POST["email"];
  $salt="H5xwGsQ6"; //Please change the value with the live salt for production environment
  $error=$_POST["error"];;


//Validating the reverse hash
If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'||||||'.$vtime.'|'.$vdate.'|'.$atime.'|'.$adate.'|'.$gender.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

                  }
  else {

        $retHashSeq = $salt.'|'.$status.'||||||'.$vtime.'|'.$vdate.'|'.$atime.'|'.$adate.'|'.$gender.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
     $hash = hash("sha512", $retHashSeq);
 ?>
<!DOCTYPE html>
<html>
<title>Mohana Mantra'17</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<body>
<?php
 if ($hash != $posted_hash) {
         echo ' <div class="alert alert-danger text-center">
          <strong>Error occured : Transaction failed'.$error.'</strong></a>.
        </div>';
       }
  else
  {



     echo '
     <div class="row">
     <center><img class="w3-image" width="200" height="200" id="logo" src="images/logomm.png" ><span class="w3-hide-large"></center>
<div class="col-md-8 col-md-offset-2" style="margin-top:35px;">

  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title text-center">Accommodation Payment Slip</h3>
    </div>
    <div class="panel-body text-center">
      <div class="panel panel-primary">
        <table class="table table-bordered table-striped table-hover">
          <tbody>
            <tr>
              <td>Registration_ID</td>
              <td>'. $firstname .'</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>'.$email.'</td>
            </tr>
            <tr>
              <td>Contact No</td>
              <td>'.$phone.'</td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>'.$gender.'</td>
            </tr>
            <tr>
              <td>Event</td>
              <td>'.$productinfo.'</td>
            </tr>
            <tr>
              <td>From</td>
              <td>'.$adate.'/09/17&nbsp;'.$atime.'</td>
            </tr>
            <tr>
              <td>To</td>
              <td>'.$vdate.'/09/17&nbsp;'.$vtime.'</td>
            </tr>
            <tr>
              <td>Payment Status</td>
              <td>'.$status.'</td>
            </tr>
            <tr>
              <td>Amount</td>
              <td>'.$amount.'(Incl. Tax)</td>
            </tr>
            <tr>
              <td>Txn Id</td>
              <td>'.$txnid.'</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</div>
  </div>
</div>';
echo '<div class="row">';

      $stmt= $con->prepare("INSERT INTO acc (transaction_id,p_id,email,mobile,gender,event_name,adate,atime,vdate,vtime,amount,status) values(?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssssssssss',$txnid, $firstname,$email,$phone,$gender,$productinfo,$adate,$atime,$vdate,$vtime,$amount,$status);


        if($stmt->execute())
        {
          //echo "$amount";
           if($status=="success"){
               echo ' <div col-md-8 col-md-offset-2> <div class="alert alert-success text-center">
                    <strong>Registered Successfully</strong></div>.
                  </div>';
           }
          else
          {
                 echo '<div col-md-8 col-md-offset-2> <div class="alert alert-warning text-center">
                  <strong>Registration failed due to unsuccesssfull Payment </strong>.</div>
                </div>';
          }


        }
        else
        {
             echo '<div col-md-8 col-md-offset-2> <div class="alert alert-warning text-center">
          <strong>Registration Failed Due to internal Error Contact : +91 7799773346</strong>.</div>
        </div>';

        }
         echo '<center><a href="accommodation"> <button type="button" class="btn btn-primary">close</button></a>
                </center>';

  }

 $con->close();

?>
<br><br><br><br><br>
</div>
</body>
</html>
