<?php
   
   include('org_session_check.php');
   if(isset($_POST['submit']))
    {
       include("db_config.php");
        $ppwd = $_POST['ppwd'];
        $npwd = $_POST['npwd'];
        $ppwd=md5($ppwd);
        $npwd=md5($npwd);
        $result=$con->query("SELECT * from mm_team where  team_mem_id='$_SESSION[login_id]'"); 
        $count=$result->num_rows;
        if($count == 1) {
             $pid=$_SESSION['login_id'];
              $row=$result->fetch_assoc();
              if($ppwd===$row['team_mem_password']){
  
                   $con->query("UPDATE mm_team SET  team_mem_password='$npwd' WHERE team_mem_id='$pid'");
                 
                    $_SESSION['error']="Successfully Changed";
                  
              }
              else
              {
                $_SESSION['error']="Password Change Failed";
              }
        }
        else
        {
         $_SESSION['error']="Error";
        }    

    }


   
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospitality | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">        
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo  $_SESSION['login_mem_id'] ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                           <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="">
                        <a href="pub_reg.php"><i class="fa fa-fw fa-dashboard"></i>Onsite Registration</a>
                    </li>
                    <li class="">
                        <a href="search.php"><i class="fa fa-fw fa-dashboard"></i>Search participants</a>
                    </li>
                     <li class="">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>
                </ul>
                 
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Profile <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Change Password
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <?php
       if($_SESSION['error']){?> 
            <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong><?php echo $_SESSION['error'] ?></strong> 
                        </div>
                    </div>
                </div>    
   <?php $_SESSION['error']=""; } ?>
                    </div>
                    
                </div>
                
                    <div class="row">
    
     <div class="col-md-8 col-md-offset-2" >

              <div class="panel panel-primary">
                <div class="panel-heading"> 
                  <h3 class="panel-title text-center">Change Password</h3>
              </div>
              <div class="panel-body text-center">
                  <form class="form-horizontal" action="profile" method="POST">
                    <div class="form-group">
                      <label for="ppw" class="col-lg-2 control-label">Current Password</label>
                      <div class="col-lg-10">
                        <input type="password" class="form-control" name="ppwd"  placeholder="Current Password">
                      </div>
                    </div><br><br>
                    <div class="form-group">
                      <label for="inputEmail" class="col-lg-2 control-label">New Password</label>
                      <div class="col-lg-10">
                        <input type="password" class="form-control" name="npwd" id="npwd" placeholder="New Password">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail" class="col-lg-2 control-label">Current Password</label>
                      <div class="col-lg-10">
                        <input type="password" class="form-control" name="cpwd" id="cpwd" placeholder="Current Password">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2 text-center">
                        <button type="submit" name="submit" class="btn btn-primary">Change Password</button>
                      </div>
                    </div>

                    </form>
              </div>
          </div>

        </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
        var password = document.getElementById("npwd")
      , confirm_password = document.getElementById("cpwd");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
  </script> 
</body>

</html>
