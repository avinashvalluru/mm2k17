<?php
   include('org_session_check.php');

    if(isset($_POST['submit']))
    {
        $name = validateFormData( $_POST['fullname'] );
        $email = validateFormData( $_POST['email'] );
        $mobile = validateFormData( $_POST['mobile'] );
        $clg = validateFormData( $_POST['clg'] );
        $txn_no= validateFormData( $_POST['txn_no'] );
        $mmid= validateFormData( $_POST['mmid'] );
        $name=strtoupper($name);
        $mmid=strtoupper($mmid);
        $email=strtolower($email);
        $clg=strtoupper($clg);
        $txn_no=strtoupper($txn_no);
        $pid=tempId();
        $password="demo123";
        $encriptpass=md5($password);
        $reg_mode="Spot";
        $email=$email."@".$pid;
        $reg_by=$_SESSION['login_id'];
        $stmt= $con->prepare("INSERT INTO participants (p_id,full_name,col_name,email,password,mobile,reg_mode,reg_by,txn_no) values(?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('sssssssss',$pid,$name,$clg,$email,$encriptpass,$mobile,$reg_mode,$reg_by,$txn_no);

        if($stmt->execute())
        {

            $stmt1=$con->prepare("INSERT INTO mmid_issue(reg_id,mm_id,issue_by) values(?,?,?)");
            $stmt1->bind_param("sss",$pid,$mmid,$reg_by);
            if ($stmt1->execute()) {
                # code...

                $_SESSION['error'] = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Registration successfull</strong>
                        </div>';

            }
            else
             {
                 $_SESSION['error'] = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Registration successfull MMID link failure Check again'.mysqli_error($con).'</strong>
                        </div>';

             }

        }
        else{
            $_SESSION['error'] = "<div class='alert alert-danger alert-dismissable'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <i class='fa fa-info-circle'></i>  <strong>mysqli_error($con)</strong>
                        </div>";
        }
        $stmt->close();
        $stmt1->close();
        $con->close();

    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospitality | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
           <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo  $_SESSION['login_mem_id']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="">
                        <a href="pub_reg.php"><i class="fa fa-fw fa-book"></i>Onsite Registration</a>
                    </li>

                     <li class="">
                        <a href="search.php"><i class="fa fa-fw fa-search"></i>Search participants</a>
                    </li>
                    <li class="active">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>
                    <li class="">
                        <a href="searchmmid.php"><i class="fa fa-fw fa-search"></i>Search MMID</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       <h1 class="page-header">
                           Spot Registration<small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i>Spot Registration
                            </li>
                        </ol>
                    </div>
                </div>
                  <?php
       if($_SESSION['error']){?>
            <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <?php echo $_SESSION['error'] ?>

                    </div>
                </div>
   <?php $_SESSION['error']=""; } ?>
                <div class="jumbotron col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                <form class="form-horizontal panel-body" action="spot_reg" method="POST">
                      <div class="form-group">
                        <label for="fullname" class="col-sm-2 control-label">FullName</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="4" maxlength="20" required="required" class="form-control" name="fullname" id="fullname" placeholder="FullName" >
                        </div>
                        </div>
                       <!-- <div class="form-group">
                                <label class="col-sm-2 control-label">Gender</label>
                                <label class="radio-inline">
                                  <div class="col-sm-10">
                                    <input type="radio" name="gender" id="gender" value="mALE" checked>Male
                                  </div>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" id="gender" value="option2">Female
                                </label>

                        </div>-->

                      <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">ID_Card</label>
                        <div class="col-sm-10">
                          <input type="text"  minlength="4" maxlength="20" required="required" class="form-control" name="email" id="email" placeholder="Enter collegeId or proofId">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">College </label>
                        <div class="col-sm-10">
                          <input type="text" minlength="4" maxlength="30" required="required" class="form-control" name="clg" id="clg" placeholder="College Name" onkeypress="return blockSpecialChar(event);">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Mobile</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="10" maxlength="10" class="form-control" id="inputPassword3" name="mobile" required="" placeholder="Enter 10 digit mobile num"onkeypress="return isNumber(event)">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Txn_NO</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="2" maxlength="7" class="form-control" id="inputPassword3" name="txn_no" required="" placeholder="Enter Receipt num">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">MMID</label>
                        <div class="col-sm-10">
                          <input type="text" name="mmid" minlength="8" maxlength="8" class="form-control" id="inputPassword3" style="text-transform: uppercase;" required placeholder="Scan/Enter Mmid"onkeypress="return blockSpecialChar(event);">
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" name="submit" class="btn btn-default">Register</button>
                        </div>
                      </div>
                </form>

             </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
 <script src="js/script.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
