<?php
   
   include('org_session_check.php');  
   if(isset($_POST['resend'])){
       $pid=$_POST['pid'];
       $email=$_POST['email'];
       $name=$_POST['fname'];
       $password=getRandomString();
       $encriptpass=md5($password);
       $stmt= $con->prepare("UPDATE participants SET password=? where p_id=?");
       $stmt->bind_param('ss',$encriptpass,$pid);
       if($stmt->execute())
       {

        $msg = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Email Sent Successfully</strong> 
                        </div>';
            include 'smtp/Send_Mail.php';
             require_once( 'smtp/class.smtp.php' );
             $to = $email;
              $subject = "Email Confirmation - Mohana Mantra 2K17"; 
               $body = '<!DOCTYPE html>

                    <html lang="en">

                    <head>
                    </head>
                    <body>

                    <div style="margin: 0; padding: 0; class="reg">

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="border-bottom: 1px solid #f7f7f7; padding: 12px 0;" align="center"><a href="http://mohanamantra.com" target="_blank"> <img src="http://mohanamantra.com/images/mm.png" alt="Mohana Mantra" width="190" border="0" /> </a></td>

                    </tr>

                    </tbody>

                    </table>

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="padding: 40px 20px 48px 20px;" align="center">

                    <table style="max-width: 450px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

                    <tbody>

                    <tr>

                    <td style="color: #4d4f50; font-size: 28px; font-weight: bold; line-height: 42px;" align="left">Hey ' . $name . ',</td>

                    </tr>

                    <tr>

                    <td style="color: #585a5b; font-size: 16px; line-height: 24px; padding-top: 8px;" align="left">Thank you for Registering!</td>

                    </tr>

                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Registration Id :' . $pid . '<br /> Password : ' . $password . ' <br><br><br>
                      <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' . $pid . '">

                    </td>
                      <tr><h3 style="color:red">We suggest you to please change your password</h3>
                    </tr>
                    <tr>
                                    <td class="">
                                        <h4 class="center">Instructions to attend the fest</h4>
                                        <p> The following should be presented during the time of registration, failing which you will not be allowed into the fest </p>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>Your College ID card</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>2 printed copies of This  confirmation Mail</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   
                                                    <td>
                                                    <span>2 Photo Copies</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Gear up to visit our land of Joy and Learning!<br>Looking forward to your presence,<br />Team Mohana Mantra.<br> <br><h3> Note: Any technical issues contact <br>Avinash +91 7799 7733 46</td>

                    </tr>

                    </tbody>

                    </table>

                    </td>

                    </tr>

                    </tbody>

                    </table>

                    </div>

                    </body>

                    </html>';
                       Send_Mail( $to, $subject, $body );  

        }
        else{
            $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Email Failed</strong> 
                        </div>';
        }
        $stmt->close();
       

   }
    if (isset($_POST['update'])) {
        $name = validateFormData( $_POST['fullname'] );
        $email = validateFormData( $_POST['email'] );
        $mobile = validateFormData( $_POST['mobile'] );
        $clg = validateFormData( $_POST['clg'] );
        $txn_no= validateFormData( $_POST['txn_no'] );
        $name=strtoupper($name);
        $email=strtolower($email);
        $clg=strtoupper($clg);
        $txn_no=strtoupper($txn_no);
        $reg_by =$_SESSION['login_id'];
        $pid=$_POST['pid'];
         $stmt= $con->prepare("UPDATE participants SET full_name =?,col_name=?,email=?,mobile=?,reg_by=?,txn_no=? where p_id=?");
         $stmt->bind_param('sssssss',$name,$clg,$email,$mobile,$reg_by,$txn_no,$pid);
         if($stmt->execute())
         {
             $msg = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Updated Successfully</strong> 
                        </div>';

         }
         else
         {
             if(mysqli_errno($con)==1062)
             {
                 $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Updataion Failed Email already exist</strong> 
                        </div>';
             }
             else
             {
                 $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Updataion Failed</strong> 
                        </div>';


             }
              
         }

       }  
        if (isset($_POST['update_email'])) {
            
        $name = validateFormData( $_POST['fullname'] );
        $email = validateFormData( $_POST['email'] );
        $mobile = validateFormData( $_POST['mobile'] );
        $clg = validateFormData( $_POST['clg'] );
        $txn_no= validateFormData( $_POST['txn_no'] );
        $name=strtoupper($name);
        $email=strtolower($email);
        $clg=strtoupper($clg);
        $txn_no=strtoupper($txn_no);
        $reg_by =$_SESSION['login_id'];
        $pid=$_POST['pid'];
        $password=getRandomString();
        $encriptpass=md5($password);
         $stmt= $con->prepare("UPDATE participants SET full_name =?,password=?,col_name=?,email=?,mobile=?,reg_by=?,txn_no=? where p_id=?");
         $stmt->bind_param('ssssssss',$name,$encriptpass,$clg,$email,$mobile,$reg_by,$txn_no,$pid);
                 if($stmt->execute())
                 {
                     $msg = '<div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-info-circle"></i>  <strong>Updated Successfully</strong> 
                                </div>';
                                include 'smtp/Send_Mail.php';
             require_once( 'smtp/class.smtp.php' );
             $to = $email;
              $subject = "Email Confirmation - Mohana Mantra 2K17"; 
               $body = '<!DOCTYPE html>

                    <html lang="en">

                    <head>
                    </head>
                    <body>

                    <div style="margin: 0; padding: 0; class="reg">

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="border-bottom: 1px solid #f7f7f7; padding: 12px 0;" align="center"><a href="http://mohanamantra.com" target="_blank"> <img src="http://mohanamantra.com/images/mm.png" alt="Mohana Mantra" width="190" border="0" /> </a></td>

                    </tr>

                    </tbody>

                    </table>

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="padding: 40px 20px 48px 20px;" align="center">

                    <table style="max-width: 450px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

                    <tbody>

                    <tr>

                    <td style="color: #4d4f50; font-size: 28px; font-weight: bold; line-height: 42px;" align="left">Hey ' . $name . ',</td>

                    </tr>

                    <tr>

                    <td style="color: #585a5b; font-size: 16px; line-height: 24px; padding-top: 8px;" align="left">Thank you for Registering!</td>

                    </tr>

                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Registration Id :' . $pid . '<br /> Password : ' . $password . ' <br><br><br>
                      <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' . $pid . '">

                    </td>
                      <tr><h3 style="color:red">We suggest you to please change your password</h3>
                    </tr>
                    <tr>
                                    <td class="">
                                        <h4 class="center">Instructions to attend the fest</h4>
                                        <p> The following should be presented during the time of registration, failing which you will not be allowed into the fest </p>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>Your College ID card</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>2 printed copies of This  confirmation Mail</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   
                                                    <td>
                                                    <span>2 Photo Copies</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Gear up to visit our land of Joy and Learning!<br>Looking forward to your presence,<br />Team Mohana Mantra.<br> <br><h3> Note: Any technical issues contact <br>Avinash +91 7799 7733 46</td>

                    </tr>

                    </tbody>

                    </table>

                    </td>

                    </tr>

                    </tbody>

                    </table>

                    </div>

                    </body>

                    </html>';
                       Send_Mail( $to, $subject, $body );  
                 }
                 else
                 {
                    if(mysqli_errno($con)==1062)
                         {
                             $msg = '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <i class="fa fa-info-circle"></i>  <strong>Updataion Failed Email already exist</strong> 
                                    </div>';
                         }
                         else
                         {
                             $msg = '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <i class="fa fa-info-circle"></i>  <strong>Updataion Failed</strong> 
                                    </div>';


                         }

                 }

       }   
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospitality | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">        
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo  $_SESSION['login_mem_id']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="">
                        <a href="pub_reg.php"><i class="fa fa-fw fa-book"></i>Onsite Registration</a>
                    </li>
                     
                     <li class="active">
                        <a href="search.php"><i class="fa fa-fw fa-search"></i>Search participants</a>
                    </li>
                     <li class="">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>
                    <li class="">
                        <a href="searchmmid.php"><i class="fa fa-fw fa-search"></i>Search MMID</a>
                    </li>
                </ul>
                
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       <?php
                           if($msg){?> 
                                <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <?php echo $msg ?>
                                            
                                        </div>
                                    </div>    
                       <?php $msg=""; } ?>
                       <h1 class="page-header">
                           Search<small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i>Search participants
                            </li>
                        </ol>
                    </div>
                
                <div class="col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                <form class="form-horizontal panel-body" action="search.php" method="GET">
                    <div class="form-group input-group">
                        <input type="text" name="search_data" class="form-control">
                        <span class="input-group-btn"><button class="btn btn-default" type="submit" name="search"><i class="fa fa-search"></i></button></span>
                    </div>
                    <div class="form-group">
                        <label class="radio-inline">Search By</label>
                            <label class="radio-inline">
                            <input type="radio" name="opt" id="name" value="full_name" checked>Name
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="opt" id="email" value="email" >Email
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="opt" id="tempid" value="p_id">Registration Id
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="opt" id="txnno" value="txn_no">Transaction No
                            </label>

                    </div>       
                </form>
            </div>
           </div> 
                <?php
                
                    if(isset($_POST['edit'])){
               
                  echo '<div class="row well">';
                        $pid=$_POST['pid'];

                        $result= $con->query("SELECT * FROM participants WHERE p_id='$pid'");
                        $count=$result->num_rows;
                        if($count==1)
                        {
                             $row=$result->fetch_assoc();
                                     echo '<div class="jumbotron col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                                        <form class="form-horizontal panel-body" method="POST" action="search.php">
                                        <input type="hidden" name="pid" value="'.$row['p_id'].'">
                                              <div class="form-group">
                                                <label for="fullname" class="col-sm-2 control-label">FullName</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="4" maxlength="50" required="required" class="form-control" id="fullname" name="fullname" placeholder="FullName" value="'.$row['full_name'].'">
                                                </div>
                                                </div> 
                                            
                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                  <input type="email"  minlength="6" maxlength="50" required="required" class="form-control" name="email" placeholder="Email" value="'.$row['email'].'">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">College </label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="2" maxlength="70" required="required" class="form-control" name="clg" placeholder="College Name" value="'.$row['col_name'].'" onkeypress="return blockSpecialChar(event);">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="10" maxlength="10" class="form-control" name="mobile" required placeholder="Enter 10 digit mobile num" value="'.$row['mobile'].'" onkeypress="return isNumber(event)">
                                                </div>
                                              </div>
                                               <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Txn_no</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number" value="'.$row['txn_no'].'" onkeypress="return blockSpecialChar(event);">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                  <input class="btn btn-primary" type="submit" name="update" value="Update">&nbsp;&nbsp;&nbsp;
                                                  <input class="btn btn-primary" type="submit" name="update_email" value="Update and Email">
                                                </div>
                                              </div>
                                              
                                        </form>

                                     </div>
                                     </div>'; 
                        }
                       
                  
                 }
                  
                        else if(isset($_GET['search']))
                           {
                               $data = validateFormData( $_GET['search_data'] );
                                $opt = validateFormData( $_GET['opt'] );
                                $result = $con->query("SELECT * FROM participants WHERE $opt LIKE '%$data%' ");                              
                                $count=$result->num_rows;
                                //  echo "$count";
                               if($count>=1)
                               {
                                   ?>
                                    <div class="row well" >
                                   <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Registration ID</th>
                                                    <th>Full Name</th>
                                                    <th>Col Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile</th>
                                                    <th>Registration Mode</th>
                                                    <th>Txn_no</th>
                                                    <th>Remail</th>
                                                    <th>Update</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php        
                                    while($row=$result->fetch_assoc())
                                    {
                                    
                                           echo '
                                                  <tr>
                                                    <td>'.$row['p_id'].'</td>
                                                    <td>'.$row['full_name'].'</td>
                                                    <td>'.$row['col_name'].'</td>
                                                    <td>'.$row['email'].'</td>
                                                    <td>'.$row['mobile'].'</td>
                                                     <td>'.$row['reg_mode'].'</td>
                                                    <td>'.$row['txn_no'].'</td>
                                                    
                                                    <td><form action="search" method="POST">
                                                    <input type="hidden" name="pid" value="'.$row['p_id'].'">
                                                    <input type="hidden" name="email" value="'.$row['email'].'">
                                                    <input type="hidden" name="fname" value="'.$row['full_name'].'">
                                                    <button type="submit" name="resend" class="btn btn-primary">Resend</button></td>
                                                    <td><button type="submit" name="edit" class="btn btn-primary">Update</button></td>
                                                    </form>
                                                </tr>';
                                    }
                               }
                               else
                                {
                             echo '<div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-info-circle"></i>  <strong>No Results Found
                                        </div>
                                    </div>';
                                }

                           }
                           
                    ?>
                    </tbody>
                   </table>
             </div>
             </div> 
             </div> 
                            
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
<?php
 $con->close();
?>
</html>
