<?php 
   include('org_session_check.php');

    if(isset($_POST['submit']))
    {
        $name = validateFormData( $_POST['fullname'] );
        $email = validateFormData( $_POST['email'] );
        $mobile = validateFormData( $_POST['mobile'] );
        $clg = validateFormData( $_POST['clg'] );
        $txn_no= validateFormData( $_POST['txn_no'] );
        $name=strtoupper($name);
        $email=strtolower($email);
        $clg=strtoupper($clg);
        $txn_no=strtoupper($txn_no);
        $pid=tempId();
        $password=getRandomString();
        $encriptpass=md5($password);
        $reg_mode="Onsite";
        $reg_by=$_SESSION['login_id'];
        $stmt= $con->prepare("INSERT INTO participants (p_id,full_name,col_name,email,password,mobile,reg_mode,reg_by,txn_no) values(?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('sssssisis',$pid,$name,$clg,$email,$encriptpass,$mobile,$reg_mode,$reg_by,$txn_no);

        if($stmt->execute())
        {
             $_SESSION['error'] = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Registration successfull</strong> 
                        </div>';
            include 'smtp/Send_Mail.php';
             require_once( 'smtp/class.smtp.php' );
             $to = $email;
              $subject = "Email Confirmation - Mohana Mantra 2K17"; 
               $body = '<!DOCTYPE html>

                    <html lang="en">

                    <head>
                    </head>
                    <body>

                    <div style="margin: 0; padding: 0; class="reg">

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="border-bottom: 1px solid #f7f7f7; padding: 12px 0;" align="center"><a href="http://mohanamantra.com" target="_blank"> <img src="http://mohanamantra.com/images/mm.png" alt="Mohana Mantra" width="190" border="0" /> </a></td>

                    </tr>

                    </tbody>

                    </table>

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">

                    <tbody>

                    <tr>

                    <td style="padding: 40px 20px 48px 20px;" align="center">

                    <table style="max-width: 450px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

                    <tbody>

                    <tr>

                    <td style="color: #4d4f50; font-size: 28px; font-weight: bold; line-height: 42px;" align="left">Hey ' . $name . ',</td>

                    </tr>

                    <tr>

                    <td style="color: #585a5b; font-size: 16px; line-height: 24px; padding-top: 8px;" align="left">Thank you for Registering!</td>

                    </tr>

                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Registration Id :' . $pid . '<br /> Password : ' . $password . ' <br><br><br>
                      <img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' . $pid . '">

                    </td>
                      <tr><h3 style="color:red">We suggest you to please change your password</h3>
                    </tr>
                    <tr>
                                    <td class="">
                                        <h4 class="center">Instructions to attend the fest</h4>
                                        <p> The following should be presented during the time of registration, failing which you will not be allowed into the fest </p>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>Your College ID card</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                    <span>2 printed copies of This  confirmation Mail</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   
                                                    <td>
                                                    <span>2 Photo Copies</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                    <tr>

                    <td style="padding-top: 8px; color: #585a5b; font-size: 16px; line-height: 24px;" align="center"><br />Gear up to visit our land of Joy and Learning!<br>Looking forward to your presence,<br />Team Mohana Mantra.<br> <br><h3> Note: Any technical issues contact <br>Avinash +91 7799 7733 46</td>

                    </tr>

                    </tbody>

                    </table>

                    </td>

                    </tr>

                    </tbody>

                    </table>

                    </div>

                    </body>

                    </html>';
                       Send_Mail( $to, $subject, $body );  

        }
        else{
            $_SESSION['error'] = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Registration Failed email already exist</strong> 
                        </div>';;
        }
        $stmt->close();
        $con->close();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospitality | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.html">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">        
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo  $_SESSION['login_mem_id']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                     <li class="active">
                        <a href="pub_reg.php"><i class="fa fa-fw fa-dashboard"></i>Onsite Registration</a>
                    </li>
                    <li class="">
                        <a href="search.php"><i class="fa fa-fw fa-dashboard"></i>Search participants</a>
                    </li>
                     <li class="">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>
                </ul>
                
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       <h1 class="page-header">
                           Onsite Registartion<small>  new Registration</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i>Onsite Registrations
                            </li>

                        </ol>
                    </div>
                </div>
                <?php
       if($_SESSION['error']){?> 
            <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <?php echo $_SESSION['error'] ?>
                        
                    </div>
                </div>    
   <?php $_SESSION['error']=""; } ?>
                <div class="jumbotron col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                <form class="form-horizontal panel-body" method="POST" action="pub_reg.php">
                      <div class="form-group">
                        <label for="fullname" class="col-sm-2 control-label">FullName</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="5" maxlength="50" required="required" class="form-control" id="fullname" name="fullname" placeholder="FullName" ">
                        </div>
                        </div> 
                    
                      <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email"  minlength="6" maxlength="50" required="required" class="form-control" name="email" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="college" class="col-sm-2 control-label">College </label>
                        <div class="col-sm-10">
                          <input type="text" minlength="2" maxlength="70" required="required" class="form-control" name="clg" placeholder="College Name" onkeypress="return blockSpecialChar(event);">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="10" maxlength="10" class="form-control" name="mobile" required="" placeholder="Enter 10 digit mobile num" onkeypress="return isNumber(event)">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="college" class="col-sm-2 control-label">Txn_no</label>
                        <div class="col-sm-10">
                          <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number" onkeypress="return blockSpecialChar(event);">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input class="btn btn-default" type="submit" name="submit" value="Register">
                        </div>
                      </div>
                </form>

             </div>   
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>
    <script src="js/notify.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
     $(document).ready(function(){
    $("#fullname").keypress(function(event){
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
    });
});
</script>
</body>

</html>
