<?php

   include('org_session_check.php');
   if (isset($_POST['issue'])) {
      # code...
      $reg_id=$_POST['reg_id'];
      $txn_no=$_POST['txn_no'];
      $mm_id=$_POST['mm_id'];
      $mm_id= strtoupper($mm_id);
      //echo "$reg_id,$txn_no,$mm_id";

      $stmt= $con->prepare("UPDATE participants SET txn_no=? WHERE p_id=?");
      $stmt->bind_param('ss',$txn_no,$reg_id);
      if($stmt->execute())
      {
          $msg="success";
           $issueby= $_SESSION['login_id'];
          $stmt1= $con->prepare("INSERT INTO mmid_issue (reg_id,mm_id,issue_by) values(?,?,?)");
          $stmt1->bind_param('sss',$reg_id,$mm_id,$issueby);
          if($stmt1->execute())
          {
                $msg = '<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>MMID ISSUED Successfully</strong>
                        </div>';
          }
          else
          {
               $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong> MMID linking failed</strong>
                        </div>';
          }

      }
      else
      {
          $msg = '<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Both transaction and mmid linking failed</strong>
                        </div>';

     }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hospitality | Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo  $_SESSION['login_mem_id']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"> <i class="fa fa-fw fa-user"></i><?php echo  $_SESSION['login_mem_name']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                    <li class="">
                        <a href="pub_reg.php"><i class="fa fa-fw fa-book"></i>Onsite Registration</a>
                    </li>

                     <li class="">
                        <a href="search.php"><i class="fa fa-fw fa-search"></i>Search participants</a>
                    </li>
                     <li class="">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="active">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>
                    <li class="">
                        <a href="searchmmid.php"><i class="fa fa-fw fa-search"></i>Search MMID</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       <?php
                           if($msg){?>
                                <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <?php echo $msg ?>

                                        </div>
                                    </div>
                       <?php $msg=""; } ?>
                       <h1 class="page-header">
                           MMID issue<small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i>MMID ISSUE
                            </li>
                        </ol>
                    </div>

                <div class="col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                  <h3 style="color:green" class="text-center">MMID ISSUE</h3>
                <form class="form-horizontal panel-body" action="mmid.php" method="GET">
                    <div class="form-group input-group">
                        <input type="text" name="reg_id" placeholder="Enter Registration Id" class="form-control">
                        <span class="input-group-btn"><button class="btn btn-default" type="submit" name="search"><i class="fa fa-search"></i></button></span>
                    </div>
                </form>
               <?php  if(isset($_GET['search']))
                           {
                               $reg_id = $_GET['reg_id'];

                               $result = $con->query("SELECT * FROM participants WHERE p_id='$reg_id' ");
                                $count=$result->num_rows;

                                //  echo "$count";
                               if($count==1)
                               {
                                    $row=$result->fetch_assoc();
                                   echo '<form class="form-horizontal panel-body" method="POST" action="mmid.php">

                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Registration ID</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  minlength="6" maxlength="50" required="required" class="form-control" name="reg_id"  value="'.$row['p_id'].'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="fullname" class="col-sm-2 control-label">FullName</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="4" maxlength="50" required="required" class="form-control" id="fullname" name="fullname" placeholder="FullName" value="'.$row['full_name'].'" readonly>
                                                </div>
                                                </div>

                                              <div class="form-group">
                                                <label for="email" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                  <input type="email"  minlength="6" maxlength="50" required="required" class="form-control" name="email" placeholder="Email" value="'.$row['email'].'" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">College </label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="2" maxlength="70" required="required" class="form-control" name="clg" placeholder="College Name" value="'.$row['col_name'].'" onkeypress="return blockSpecialChar(event);" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="10" maxlength="10" class="form-control" name="mobile" required placeholder="Enter 10 digit mobile num" value="'.$row['mobile'].'" onkeypress="return isNumber(event)" readonly>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label for="mobile" class="col-sm-2 control-label">Registration Mode</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="10" maxlength="10" class="form-control" name="reg_mode" required  value="'.$row['reg_mode'].'"  readonly>
                                                </div>
                                              </div>';
                                              if($row['reg_mode']=='Intra')
                                              {
                                                echo  '<div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Txn_no</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number" value="SVEC"  readonly>
                                                </div>
                                                 </div>';
                                              }
                                              else if($row['txn_no']!='UNPAID')
                                              {
                                                  echo  '<div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Txn_no</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number" value="'.$row['txn_no'].'"  readonly>
                                                </div>
                                                 </div>';
                                              }
                                              else
                                              {
                                                        echo  '<div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">Txn_no</label>
                                                <div class="col-sm-10">
                                                  <input type="text"  maxlength="20" required="required" class="form-control" name="txn_no" placeholder="receipt number">
                                                </div>
                                                 </div>';
                                              }
                                              echo '<div class="form-group">
                                                <label for="college" class="col-sm-2 control-label">MM ID</label>
                                                <div class="col-sm-10">
                                                  <input type="text" minlength="8" maxlength="8" required="required" class="form-control"  name="mm_id" placeholder="MMID" style="text-transform: uppercase;">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <div class="col-sm-offset-4 col-sm-10">
                                                  <input class="btn btn-primary text-center" type="submit" name="issue" value="ISSUE ID">

                                                </div>
                                              </div>

                                        </form>';
                               }
                                 else  {
                             echo '<div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-info-circle"></i>  <strong>No Results Found
                                        </div>
                                    </div>';
                                }
                    }
                   ?>
            </div>
           </div>




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
<?php
 $con->close();
?>
</html>
