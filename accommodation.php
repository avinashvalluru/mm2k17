 <?php
    include("db_config.php");
    include("functions.php");
    session_start();



    if(isset($_POST['allotme'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';

            $posted['udf1']=$_POST['gender'];
            $posted['udf2']=$_POST['fromdate'];
            $posted['udf3']=$_POST['fromtime'];
            $posted['udf4']=$_POST['todate'];
            $posted['udf5']=$_POST['totime'];
            $days=$_POST['todate']-$_POST['fromdate'];
            if($days==1)
            {
                $amt=400;
            }
            else if($days==2)
            {
                $amt=700;
            }
            else if($days==3)
            {
                $amt=900;
            }
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']="accommodation";
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Accommodation | Mohana Mantra</title>
    <meta name="theme-color" content="#000000">
    <meta name="author" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-material-design.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/normalize.css" rel="stylesheet">
    <link href="css/w3.css" rel="stylesheet">
    <link rel="stylesheet" href="css/nprogress.css">
    <script src="js/nprogress.js"></script>
    <script src="../use.fontawesome.com/a26c95d609.js"></script>
    <script src="js/jquery.min.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-81476335-1', 'auto');
        ga('send', 'pageview');

    </script>

<style>
     html, body{
            height: 100%;
          }
          body {
          background: #000000 url("images/bg.jpg") no-repeat top center fixed;
          background-size: cover;
          margin: 0;
          padding: 0;
          height: 100%;
          width: 100%;

          }
         #trans a{
            color:yellow;
          }
    h2 {
        font-weight: 700;
    }

    #accform {
        background-color: rgba(133, 40, 245, 0.3);
    }

    #contact {
        background-color: rgba(199, 0, 57, 0.9);
    }

    #trans {
        background-color: rgba(88, 24, 69, 0.3);
        color: #000;
    }

    .form-control {
        color: #BDBDBD;
    }

    .form-group label.control-label {
        font-size: 18px;
    }

    .form-group.is-focused .radio label:hover {
        color: white;
    }

    p {
        font-size: 20px;
        margin-bottom: 0;
    }

    .fa {
        font-size: 60%;
    }

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg td {
        padding: 10px 20px;
        border-style: solid;
        border-width: 0px;
        overflow: hidden;
        word-break: normal;
    }

    .tg th {
        font-weight: normal;
        padding: 10px 20px;
        border-style: solid;
        border-width: 0px;
        overflow: hidden;
        word-break: normal;
    }

    .tg .tg-baqh {
        text-align: center;
        vertical-align: top
    }

    .tg .tg-yw4l {
        vertical-align: top
    }
    #error
    {
        color: red;
    }
</style>
<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
  <script>
    jQuery(document).ready(function ($) {
      $(window).load(function () {
          submitPayuForm();
      });
    });
  </script>

<script>
    $(document).ready(function () {

        $.material.init();
    });
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function () {
        NProgress.inc();
    }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<div style="overflow-x:hidden;overflow-y: hidden;" class="text-center">
   <?php include("nav.php");?>


    <div class="container-fluid" id="accform">

        <div class="row w3-text-white"  style="margin-top: 80px; font-family: 'Quicksand', sans-serif;">
            <div class="col-md-6" >
                <div class="col-md-10 col-md-offset-1">
                    <br>
                    <h2 class="w3-text-pink" Style="font-family: 'Comfortaa', cursive;">Info</h2>
                    <div>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;Accommodation will be provided
                            from 5 PM on September 24th 2017 to 05 PM on September 27th 2017. </p>
                        <br>
                        <p class="text-justify"><i  style="margin: auto;" class="fa fa-circle-thin" aria-hidden="true"></i>&nbsp;&nbsp;
                            <strong>Accommodation Fee </strong> (Excl Tax) :</p>
                        <div class="center w3-text-white">
                            <table class="tg" style="margin-left: 35px">
                                <tr>
                                    <th class="tg-yw4l">No. of Days</th>
                                    <th class="tg-yw4l">Fee</th>
                                </tr>
                                <tr>
                                    <td class="tg-baqh">01</td>
                                    <td class="tg-yw4l">&#8377;400</td>
                                </tr>
                                <tr>
                                    <td class="tg-baqh">02</td>
                                    <td class="tg-yw4l">&#8377;700</td>
                                </tr>
                                <tr>
                                    <td class="tg-baqh">03</td>
                                    <td class="tg-yw4l">&#8377;900</td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;Participants of Mohana Mantra will
                            be accommodated in Vidyanikethan Hostels or in Private facilities in city. </p>
                        <br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;
                            Students housed in Tirupati will be provided with transport facility to and from the
                            College.</p><br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;
                            Students can get help with accommodation at the Hospitality desk on campus or prior to fest
                            by contacting us.</p><br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;
                            You are of at most importance to us. We hope to make your stay here a good experience.</p>
                        <br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;
                            Accommodations Will be charged on 24hrs basis.</p>
                        <br>
                        <p class="text-justify"><i style="margin: auto;" class="fa fa-circle-thin"
                                                   aria-hidden="true"></i>&nbsp;&nbsp;
                            Photo of the Participant and College Id card or any valid proof are Mandatory .</p><br><br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-10 col-md-offset-1">
                    <br>
                    <h2 class="w3-text-pink" Style="font-family: 'Comfortaa', cursive;">Accommodation</h2>
                    <!--<p class="center red"><strong>*BOOKINGS CLOSED</strong></p>-->
                    <br>
                    <div class="tabbg w3-card-4" style="padding-bottom: 75px;" >
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in" id="about1">
                                <form action="accommodation" class="form-horizontal" role="form" method="POST">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <div class="col-md-10">

                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" name="gender" value="male" checked="checked">Male</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <div class="col-md-10">
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" name="gender" value="female">Female</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="border-top: 1px solid #000000;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">From</label>
                                                    <div class="col-md-10">
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" class="date" name="fromdate" value="24"
                                                                          checked="checked">24/09/2017</label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" class="date" name="fromdate" value="25">25/09/2017</label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" class="date" name="fromdate" value="26">26/09/2017</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">To</label>
                                                    <div class="col-md-10">
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" class="date" name="todate" value="25">25/09/2017</label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label><input type="radio" class="date" name="todate" value="26" checked="checked">26/09/2017</label>
                                                        </div>
                                                        <div class="radio radio-primary">
                                                            <label><input  type="radio" class="date" name="todate" value="27">27/09/2017</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="border-top: 1px solid #000000;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><abbr title="Starts from 05:00 PM 24th SEPT 2016">Arrival
                                                            Time</abbr></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-md-offset-2">
                                                <div class="form-group">
                                                    <label><abbr title="Vacating ends at 5:00 PM 27 SEPT 2017">Vacating
                                                            Time</abbr></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="fromtime" class="col-md-2 control-label">From</label>
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <select id="fromtime" name="fromtime" class="form-control" required>
                                                            <option class="atime" disabled value="12AM">12:00 AM</option>
                                                            <option class="atime" disabled value="01AM">01:00 AM</option>
                                                            <option class="atime" disabled value="02AM">02:00 AM</option>
                                                            <option class="atime" disabled value="03AM">03:00 AM</option>
                                                            <option class="atime" disabled value="04AM">04:00 AM</option>
                                                            <option class="atime" disabled value="05AM">05:00 AM</option>
                                                            <option class="atime" disabled value="06AM">06:00 AM</option>
                                                            <option class="atime" disabled value="07AM">07:00 AM</option>
                                                            <option class="atime" disabled value="08AM">08:00 AM</option>
                                                            <option class="atime" disabled value="09AM">09:00 AM</option>
                                                            <option class="atime" disabled value="10AM">10:00 AM</option>
                                                            <option class="atime" disabled value="11AM">11:00 AM</option>
                                                            <option class="atime" disabled value="12PM">12:00 PM</option>
                                                            <option class="atime" disabled value="01PM">01:00 PM</option>
                                                            <option class="atime" disabled value="02PM">02:00 PM</option>
                                                            <option class="atime" disabled value="03PM">03:00 PM</option>
                                                            <option class="atime" disabled value="04PM">04:00 PM</option>
                                                            <option value="05PM" id="asel" selected>05:00 PM</option>
                                                            <option value="06PM">06:00 PM</option>
                                                            <option value="07PM">07:00 PM</option>
                                                            <option value="08PM">08:00 PM</option>
                                                            <option value="09PM">09:00 PM</option>
                                                            <option value="10PM">10:00 PM</option>
                                                            <option value="11PM">11:00 PM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="totime" class="col-md-2 control-label">To</label>
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <select id="totime" name="totime" class="form-control" required>
                                                            <option  value="12AM">12:00 AM</option>
                                                            <option  value="01AM">01:00 AM</option>
                                                            <option  value="02AM">02:00 AM</option>
                                                            <option  value="03AM">03:00 AM</option>
                                                            <option  value="04AM">04:00 AM</option>
                                                            <option  value="05AM" id="vsel" selected>05:00 AM</option>
                                                            <option  value="06AM">06:00 AM</option>
                                                            <option  value="07AM">07:00 AM</option>
                                                            <option  value="08AM">08:00 AM</option>
                                                            <option  value="09AM">09:00 AM</option>
                                                            <option  value="10AM">10:00 AM</option>
                                                            <option  value="11AM">11:00 AM</option>
                                                            <option  value="12PM">12:00 PM</option>
                                                            <option  value="01PM">01:00 PM</option>
                                                            <option  value="02PM">02:00 PM</option>
                                                            <option  value="03PM">03:00 PM</option>
                                                            <option  value="04PM">04:00 PM</option>
                                                            <option value="05PM">05:00 PM</option>
                                                            <option class="vtime" value="06PM">06:00 PM</option>
                                                            <option class="vtime" value="07PM">07:00 PM</option>
                                                            <option class="vtime" value="08PM">08:00 PM</option>
                                                            <option class="vtime" value="09PM">09:00 PM</option>
                                                            <option class="vtime" value="10PM">10:00 PM</option>
                                                            <option class="vtime" value="11PM">11:00 PM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br><br><br><br><br>
                                           <span id="error"></span>
                                           <span id="amount"></span><br>
                                           <?php
                                           include("db_config.php");
                                           $result= $con->query("SELECT p_id from acc WHERE p_id='$_SESSION[p_id]' and status='success'");
                                            $count=$result->num_rows;
                                            if($count==0)
                                            {
                                                echo '<h5 style='color:red'>* Online Closed Spot Registartions Opened!!</h5>';
                                            }
                                            else
                                            {
                                                echo "<h5 style='color:green'>* You have already registered Successfully</h5>";
                                            }
                                           $con->close();
                                        ?>
                                        <br>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="row w3-text-white" id="trans">
            <div class="col-md-8 col-md-offset-2" ><br>
                <h2 class="w3-text-pink" Style="font-family: 'Comfortaa', cursive;">Transportation</h2><br>
                <div class="row" style="font-family: 'Quicksand', sans-serif; font-weight: lighter;">
                    <div class="col-md-6">
                        <div class="center">
                            <p style="text-align: center;">Bus Facility to the Campus will be available
                                from the following stations:</p><br>
                            <ul>
                                <li style="font-size: 20px;">
                                    Bus Station<a href="https://goo.gl/DuW0EN" target="_blank">&nbsp;Route Map</a>
                                </li>
                                <li style="font-size: 20px;">
                                    Railway Station <a href="https://goo.gl/I1WjGC" target="_blank">&nbsp;Route
                                        Map</a>
                                </li>
                                <li style="font-size: 20px;">
                                    Mahati Auditorium<a href="https://goo.gl/Em7AJ0" target="_blank">&nbsp;Route
                                        Map</a>
                                </li>
                                <li style="font-size: 20px;">
                                    Lotus Temple, Iskcon<a href="https://goo.gl/4fW6mw" target="_blank">&nbsp;Route
                                        Map</a>
                                </li>
                                <li style="font-size: 20px;">
                                    Balaji Colony<a href="https://goo.gl/cLiSKV" target="_blank">&nbsp;Route Map</a>
                                </li>
                                <li style="font-size: 20px;">
                                    Annamayya Circle<a href="https://goo.gl/M4Ezn4" target="_blank">&nbsp;Route
                                        Map</a>
                                </li>
                            </ul>
                        </div>
                        <br>
                    </div>
                    <div class="col-md-6">
                        <div class="center">
                            <p style="text-align: center;">Participants enjoying our hospitality will be
                                provided free transport facility</p><br>
                            <p>Pawan Kumar P</p>
                            <p style="margin-bottom: 10px;">+91 9160 284 061</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row w3-text-white" id="contact">
            <div class="col-md-8 col-md-offset-2">
                <div class="row" style="font-family: 'Quicksand', sans-serif; font-weight: lighter;" >
                    <div class="col-md-12">
                        <br>
                        <h2 class="w3-text-cyan" Style="font-family: 'Comfortaa', cursive;">Contact</h2>
                        <p style="text-align: justify;">For Hospitality related queries, Please reach out to us at <a
                                href="mailto:hospitality@mohanamantra.com" style="font-weight: 300;">hospitality@mohanamantra.com</a>
                            or contact : +91 98480 23553(Mouli)
                        </p><br>
                    </div>
                </div>
                <div class="row" style="font-family: 'Quicksand', sans-serif; font-weight: lighter;">
                    <div class="col-md-6">
                        <div class="center">
                            <p>Accommodation - Boys</p>
                            <p>P Nitish</p>
                            <p>+91 76809 91995</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="center">
                            <p>Accommodation - Girls</p>
                            <p>Eesha</p>
                            <p>+91 91001 49073</p>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>

    </div>

</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/modernizr.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
    $(".date").change(function() {
        var fromdate = $("input[name='fromdate']:checked"). val();
        var todate = $("input[name='todate']:checked"). val();
        fromdate = parseInt(fromdate);
        todate =  parseInt(todate);
        var validate=0;
         validate= todate-fromdate;

           if(validate<=0)
           {
            $("#allot").prop('disabled',true);

             $("#error").html("<h5> *Please select Correct dates</h5>");
           }
           else {
            $("#allot").prop('disabled',false);
             $("#error").html("");
           }
           if(fromdate==24)
           {
             $(".atime").attr('disabled','disabled');

             $("#asel").html('<option value="05PM" id="asel" selected>05:00 PM</option>');

           }
           else{
             $(".atime").removeAttr('disabled');
           }
           if(todate==27)
           {
                $(".vtime").attr('disabled','disabled');
                $(".vsel").attr('selected','selected');
           }
           else{
              $(".vtime").removeAttr('disabled');
              $("#vsel").html('<option  value="05AM" id="vsel" selected>05:00 AM</option>');
           }

    });
});
</script>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
         <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" />
         <input type="hidden" name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" />
         <input type="hidden" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" />
         <input type="hidden" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" />
         <input type="hidden" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" />
         <input type="hidden" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" />

      </table>
    </form>

</body>
</html>
