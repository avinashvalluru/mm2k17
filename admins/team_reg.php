<?php

   include('admin_session_check.php');
    if(isset($_POST['submit']))
    {
        $name = validateFormData( $_POST['fullname'] );
        $roll = validateFormData( $_POST['rollno'] );
        $panel = validateFormData( $_POST['panel'] );
        $password = validateFormData( $_POST['password'] );
        $password=md5($password);
        $name=strtoupper($name);
        $roll=strtoupper($roll);
        $stmt= $con->prepare("INSERT INTO mm_team (team_mem_roll_no,team_mem_name,team_mem_panel_name,team_mem_password) values(?,?,?,?)");
        $stmt->bind_param('ssss',$roll,$name,$panel,$password);

        if($stmt->execute())
        {
             $_SESSION['error'] = "Registration Successful";
        }
        else{
            $_SESSION['error'] = "Registration failed Roll no already exist";
        }
        mysqli_close($con);
    }

    if (isset($_POST['deactivate'])) {
        # code...
        $id=$_POST['pid'];
        //$sql = "UPDATE mm_team set status=\'1\' where team_mem_id=100";
        $con->query("UPDATE mm_team set status='1' where team_mem_id=$id");
    }
     if (isset($_POST['activate'])) {
        # code...
        $id=$_POST['pid'];
        //$sql = "UPDATE mm_team set status=\'1\' where team_mem_id=100";
        $con->query("UPDATE mm_team set status='0' where team_mem_id=$id");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin| Mohanamantra</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Admin Panel-Hospitality</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo  $_SESSION['login_id']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li>
                            <a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="">
                        <a href="home.php"><i class="fa fa-fw fa-dashboard"></i>DashBoard</a>
                    </li>
                     <li class="active">
                        <a href="team_reg.php"><i class="fa fa-fw fa-dashboard"></i>Team Registration</a>
                    </li>
                   <li class="">
                        <a href="search.php"><i class="fa fa-fw fa-dashboard"></i>Search Participants</a>
                    </li>
                      <!--<li class="">
                        <a href="spot_reg.php"><i class="fa fa-fw fa-search"></i>Spot Registration</a>
                    </li>
                    <li class="">
                        <a href="mmid.php"><i class="fa fa-fw fa-search"></i>MMID isuue</a>
                    </li>-->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Team Registrations
                            </li>

                        </ol>
                    </div>
                </div>
                 <?php
       if($_SESSION['error']){?>
            <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong><?php echo $_SESSION['error'] ?></strong>
                        </div>
                    </div>
                </div>
              <?php $_SESSION['error']=""; } ?>
              <div class="row">
                <div class="jumbotron col-md-6 col-md-offset-3 panel panel-default breadcrumb">
                <form class="form-horizontal panel-body" method="POST" action="team_reg.php">
                      <div class="form-group">
                        <label for="fullname" class="col-sm-2 control-label">FullName</label>
                        <div class="col-sm-10">
                          <input type="text" minlength="5" maxlength="20" required="required" class="form-control" name="fullname" placeholder="FullName" onkeypress="return onlyAlphabets(event,this);">
                        </div>
                        </div>

                      <div class="form-group">
                        <label for="rollno" class="col-sm-2 control-label">RollNo</label>
                        <div class="col-sm-10">
                          <input type="text"   required="required" class="form-control" name="rollno" placeholder="Roll No">
                        </div>
                      </div>
                      <div class="form-group">
                                <label class="col-sm-2 control-label">Panel</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="panel">
                                    <option value="Hospitality">Hospitality</option>
                                    <option value="Technoholic">Technoholic</option>
                                    <option value="Kalakshetra">Kalakshetra</option>
                                    <option value="Workshops">Workshops</option>
                                    <option value="Finance">Finance</option>
                                </select>
                                </div>
                       </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">password</label>
                        <div class="col-sm-10">
                          <input type="password" minlength="4" maxlength="10" class="form-control" name="password" required="" placeholder="Enter the password">
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input class="btn btn-default" type="submit" name="submit" value="Register">
                        </div>
                      </div>
                </form>

             </div>
             </div>
             <div class="row">
             <?php

                                $result = $con->query("SELECT * FROM mm_team");
                                $count=$result->num_rows;
                                //  echo "$count";
                               if($count>=1)
                               {
                                   ?>
                                    <div class="row well" >
                                   <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>UserName</th>
                                                    <th>Name</th>
                                                    <th>Panel Name</th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php
                                    while($row=$result->fetch_assoc())
                                    {

                                           echo '
                                                  <tr>
                                                    <td>'.$row['team_mem_id'].'</td>
                                                    <td>'.$row['team_mem_roll_no'].'</td>
                                                    <td>'.$row['team_mem_name'].'</td>
                                                    <td>'.$row['team_mem_panel_name'].'</td>
                                                    <td><form action="team_reg" method="POST">
                                                    <input type="hidden" name="pid" value="'.$row['team_mem_id'].'">';
                                                    if($row['status']==0)
                                                    {
                                                        echo "<button type='submit' name='deactivate' class='btn btn-primary' onclick='return confirm(\"Are you Sure to Deactiavte \");'>Deactivate</button></td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<button type='submit' name='activate' class='btn btn-primary' onclick='return confirm(\"Are you Sure to Actiavte \");'>Activate</button></td>";
                                                    }

                                                   echo '</form>
                                                </tr>';
                                    }
                               }
                               else
                                {
                             echo '<div class="row">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <i class="fa fa-info-circle"></i>  <strong>No Results Found
                                        </div>
                                    </div>';
                                }



                    ?>
                    </tbody>
                   </table>
             </div>
             </div>
              </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>
    <script src="js/notify.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
