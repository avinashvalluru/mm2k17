<?php

   include('admin_session_check.php');


?>
<!DOCTYPE html>
<html>
<title>Mohana Mantra'17</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<body>
<?php
$result=$con->query("select * from participants p, mmid_issue m where p.p_id=m.reg_id and p.reg_mode='SELF'");
$rec_no=0;
    $count=$result->num_rows;
    //echo "$count";
  while($row=$result->fetch_assoc())
  {
     $rec_no++;

         echo '
         <div class="row">

    <div class="col-md-8 col-md-offset-2" style="margin-top:25px;">

      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title text-center">Registration Payment Slip</h3>
        </div>
        <div class="panel-body text-center">
          <div class="panel panel-primary">
            <table class="table table-bordered table-striped table-hover">
              <tbody>
                <tr>
                  <td>Receipt No</td>
                  <td>OLMM000'. $rec_no .'</td>
                </tr>
                <tr>
                  <td>MM_ID</td>
                  <td>'.$row['mm_id'].'</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>'.$row['full_name'].'</td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td>'.$row['mobile'].'</td>
                </tr>
                <tr>
                  <td>College</td>
                  <td>'.$row['col_name'].'</td>
                </tr>
                <tr>
                  <td>Amount</td>
                  <td>100 /-</td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
    </div>
      </div>
    </div>';


  }


 $con->close();

?>
<br><br><br><br><br>
</div>
</body>
</html>
