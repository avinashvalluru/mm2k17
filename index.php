<?php
   include("db_config.php");
    include("functions.php");
     session_start();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Mohana Mantra'17 | Sree vidyanikethan</title>
	<meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
	<meta name="theme-color" content="#000000">
    <meta name="author" content="Avinash Valluru"/>
    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
	<link rel="icon" type="image/png" href="images/mm.png" sizes="16x16"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/foundation.min.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/player.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/ihover.css">
	<link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/icons.min.css">
	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->
    <script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/scrolloverflow.js"></script>
    <script type="text/javascript" src="js/jquery.downCount.js"></script>
	<script type="text/javascript" src="js/jquery.fullPage.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script src='js/particles.js'></script>
	<script src="js/modernizr.js"></script>
	<script src="js/notify.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				anchors: ['Home', 'About', 'Events', 'Accommodation','Gallery','Sponsors','Contact'],
				//sectionsColor: ['#4A6FB1', '#939FAA', '#323539'],
				menu:'#qmenu',
				scrollOverflow: true,
				navigation: true,
				navigationPosition: 'right',
				//navigationTooltips: ['First page', 'Second page', 'Third and last page']
			});
		});
	</script>
	<style>

		  body {
    background: #000000 url("images/bg.jpg") no-repeat top center fixed;
    background-size: cover;
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
    overflow:scroll;
   -webkit-overflow-scrolling:touch;
    }
		 }
		/* .bg{
		  position: absolute;
		  top: 0;
		  left:0;
		  height: 100%;
		  width: 100%;
		  z-index: -1;
		  text-align: center;
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		}*/

	    .contactform .title {
	        font-weight: bold;
	    }


	    .contactform .value {
	        margin-left: 19px;
	        margin-bottom: 20px;
	    }



		#particles-js{
		height:100%;
		  width:100%;
		position:absolute;

		}
		@media screen and (max-width: 480px) {

					#contact,#events,#sponsors h1{
						margin-top: 80px;
					}
					#logo{
						transform: scale(1.7);

					}
			}
		a,a:hover{
			text-decoration: none;
			}
			.menu-bar {
				 background: rgb(5, 5, 5);
				/* background-color: rgb(59,208,157);	 */
				float: right;
				position: fixed;
				z-index: 8;
				height: 48px;
				width: 230px;
				top: 24px;
				left: 40px;
				padding-left: 24px;
				border-top-right-radius: 12px;
				border-bottom-right-radius: 12px;
				box-shadow : 0 0 5px 0 rgba(255, 255, 255, 0.7);

			}

			.menu-bar-btn {
				 background-color: rgb(5, 5, 5);
				/* background-color: rgb(59,208,157);	 */
				color: white;
				border: none;
				cursor: pointer;
				display: inline-block;
				height: 48px;
				width: 100px;
				border-top-right-radius: 12px;
				border-bottom-right-radius: 12px;
			}

			.menu-bar-item {
				position: relative;
				display: inline-block;
			}
			.contactform{
				background: rgba(15,25,36,0.3);
				padding: 20px;
			}

			.abtn {
			  display: inline-block;
			  margin: 1em 0;
			  padding: 1em 2em;
			  background: black;
			  border-radius: 3px;
			  font-weight: 400;
			  text-align: center;
			}
			.orange span {
			  background: -webkit-linear-gradient(left, #ffcb52, #ff451f);
			  -webkit-background-clip: text;
			  -webkit-text-fill-color: transparent;
			}
			.buttons {
			  display: -webkit-box;
			  display: -ms-flexbox;
			  display: flex;
			  -webkit-box-flex: 1;
			      -ms-flex: 1 0 1;
			          flex: 1 0 1;
			  -ms-flex-wrap: wrap;
			      flex-wrap: wrap;
			  -webkit-box-orient: vertical;
			  -webkit-box-direction: normal;
			      -ms-flex-direction: column;
			          flex-direction: column;
			  padding: 1em;
			}
			.border .orange {
			  border-left: 2px solid #ffcb52;
			  border-right: 2px solid #ff451f;
			  background-image: -webkit-linear-gradient(left, #ffcb52, #ff451f), -webkit-linear-gradient(left, #ffcb52, #ff451f);
			  background-image: linear-gradient(left, #ffcb52, #ff451f), linear-gradient(left, #ffcb52, #ff451f);
			  background-size: 100% 2px;
			  background-position: 0 100%, 0 0;
			  background-repeat: no-repeat;
			  background-clip: border-box;
			}
			.contact_card{
			  background-color: rgba(3, 12, 71,0.1);
			  position: relative;
			  text-align: center;
			  display: inline-block;
			  padding: 10px;
			  height: 100%;
			  width: 350px;
			  transition: all 1s;
			}
			.contact_card:hover{
			  background-color:rgba(201, 174, 168,0.1);
			}
			.contact_card_post{
			  font-size: 19px;
			  color: #cca400;
			}
			.contact_info{
			  position: relative;
			  display: table;
			  margin-top: 10px;
			}
			.contact_image{
			  display: inline-block;
			  width: 120px;
			  height: 120px;
			  background-position: center;
			  background-size: cover;
			  background-repeat: no-repeat;
			  border-radius: 50%;
			  margin-right: 15px;
			}
			.contact_content{
			  display: table-cell;
			  text-align: left;
			  vertical-align: middle;
			}
			figure.snip0069 {
				font-family: 'Raleway', Arial, sans-serif;
				color: #fff;
				position: relative;
				float: left;
				overflow: hidden;
				margin: 10px 1%;
				min-width: 210px;
				max-width: 310px;
				width: 100%;
				text-align: center;
   			}
			figure.snip0069 * {
			  -webkit-box-sizing: border-box;
			  box-sizing: border-box;
			}
			figure.snip0069 > .image {
			  display: block;
			  position: relative;
			  padding-bottom: 5px;
			}
			figure.snip0069 > .image img {
			  width: 70%;
			  border-radius: 50%;
			  border: 3px solid #e91e63;
			}
			@-webkit-keyframes outerRotate1 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(360deg);
			  }
			}
			@-moz-keyframes outerRotate1 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(360deg);
			  }
			}
			@-o-keyframes outerRotate1 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(360deg);
			  }
			}
			@keyframes outerRotate1 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(360deg);
			  }
			}
			@-webkit-keyframes outerRotate2 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(-360deg);
			  }
			}
			@-moz-keyframes outerRotate2 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(-360deg);
			  }
			}
			@-o-keyframes outerRotate2 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(-360deg);
			  }
			}
			@keyframes outerRotate2 {
			  0% {
			    transform: translate(-50%, -50%) rotate(0);
			  }
			  100% {
			    transform: translate(-50%, -50%) rotate(-360deg);
			  }
			}
			@-webkit-keyframes textColour {
			  0% {
			    color: #fff;
			  }
			  100% {
			    color: #3BB2D0;
			  }
			}
			@-moz-keyframes textColour {
			  0% {
			    color: #fff;
			  }
			  100% {
			    color: #3BB2D0;
			  }
			}
			@-o-keyframes textColour {
			  0% {
			    color: #fff;
			  }
			  100% {
			    color: #3BB2D0;
			  }
			}
			@keyframes textColour {
			  0% {
			    color: #fff;
			  }
			  100% {
			    color: #3BB2D0;
			  }
			}

			.e-loadholder {
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  width: 240px;
			  height: 240px;
			  border: 5px solid #1B5F70;
			  border-radius: 120px;
			  box-sizing: border-box;
			}
			.e-loadholder:after {
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  content: " ";
			  display: block;
			  background: #222;
			  transform-origin: center;
			  z-index: 0;
			}
			.e-loadholder:after {
			  width: 100px;
			  height: 200%;
			  -webkit-animation: outerRotate2 30s infinite linear;
			  -moz-animation: outerRotate2 30s infinite linear;
			  -o-animation: outerRotate2 30s infinite linear;
			  animation: outerRotate2 30s infinite linear;
			}
			.e-loadholder .m-loader {
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  width: 200px;
			  height: 200px;
			  color: #888;
			  text-align: center;
			  border: 5px solid #2a93ae;
			  border-radius: 100px;
			  box-sizing: border-box;
			  z-index: 20;
			  text-transform: uppercase;
			}
			.e-loadholder .m-loader:after {
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  content: " ";
			  display: block;
			  background: #222;
			  transform-origin: center;
			  z-index: -1;
			}
			.e-loadholder .m-loader:after {
			  width: 100px;
			  height: 106%;
			  -webkit-animation: outerRotate1 15s infinite linear;
			  -moz-animation: outerRotate1 15s infinite linear;
			  -o-animation: outerRotate1 15s infinite linear;
			  animation: outerRotate1 15s infinite linear;
			}
			.e-loadholder .m-loader .e-text {
			  font-size: 14px;
			  font-size: 1.4rem;
			  line-height: 130px;
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  -webkit-animation: textColour 1s alternate linear infinite;
			  -moz-animation: textColour 1s alternate linear infinite;
			  -o-animation: textColour 1s alternate linear infinite;
			  animation: textColour 1s alternate linear infinite;
			  display: block;
			  width: 140px;
			  height: 140px;
			  text-align: center;
			  border: 5px solid #3bb2d0;
			  border-radius: 70px;
			  box-sizing: border-box;
			  z-index: 20;
			}
			.e-loadholder .m-loader .e-text:before, .e-loadholder .m-loader .e-text:after {
			  position: absolute;
			  top: 50%;
			  left: 50%;
			  -webkit-transform: translate(-51%, -50%);
			  -moz-transform: translate(-51%, -50%);
			  -ms-transform: translate(-51%, -50%);
			  -o-transform: translate(-51%, -50%);
			  transform: translate(-51%, -50%);
			  content: " ";
			  display: block;
			  background: #222;
			  transform-origin: center;
			  z-index: -1;
			}
			.e-loadholder .m-loader .e-text:before {
			  width: 110%;
			  height: 40px;
			  -webkit-animation: outerRotate2 3.5s infinite linear;
			  -moz-animation: outerRotate2 3.5s infinite linear;
			  -o-animation: outerRotate2 3.5s infinite linear;
			  animation: outerRotate2 3.5s infinite linear;
			}
			.e-loadholder .m-loader .e-text:after {
			  width: 40px;
			  height: 110%;
			  -webkit-animation: outerRotate1 8s infinite linear;
			  -moz-animation: outerRotate1 8s infinite linear;
			  -o-animation: outerRotate1 8s infinite linear;
			  animation: outerRotate1 8s infinite linear;
			}
			.loader{
				margin:0;
				padding:0;
				height: 100%;
				width: 100%;
				background:#222;
				position: fixed;
				top: 0;
				left: 0;
				overflow: hidden;
				z-index:1000;
			}

			    .parent{
			    	transform: scale(0.8);
			    }


			.fb{color:#3b5998;}
			.twitter{color: #00b6f1 ;}
			.google{color: #dc4e41;}
			.insta{color: #3f729b;}
			.youtube{color:#cd201f;}

			#fp-nav.right {
			  right: 0px;
			  margin-right: 20px;
			  position: absolute; }

			#fp-nav ul {
			  margin-top: -22px; }

			#fp-nav ul li, .fp-slidesNav ul li {
			  height: 14px;
			  margin: 4px;
			  margin-right: 0; }

			#fp-nav ul li a.active span,
			#fp-nav ul li a:hover span,
			#fp-nav ul li a.active:hover span,
			#fp-nav ul li a span {
			  background:#e91e63;
			  width: 16px;
			  border-radius: 0;
			  height: 1px;
			  left: auto;
			  right: 0;
			  top: 0;
			  margin-top: 0;
			  -webkit-transition: 0.3s;
			          transition: 0.3s; }
			#fp-nav ul li a.active span,
			#fp-nav ul li a.active:hover span {
			  width: 32px;
			  background: #e5223c; }
            footer {
			  background: #2db34a;
			  bottom: 0;
			  left: 0;
			  padding: 20px 0;
			  position: fixed;
			  right: 0;
			  text-align: center;
			}

			.team-member img {
        height: 180px;
        width: 180px;
    }

</style>
	<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-104456890-1', 'auto');
		  ga('send', 'pageview');
	</script>

</head>
<body>
	<div id="overlay" class="w3-display-container loader" style=" ">

	        <div class="e-loadholder">
				<div class="m-loader">
					<span class="e-text">Loading</span>
				</div>
			</div>
	</div>
	<script>
		jQuery(document).ready(function ($) {
	    $(window).load(function () {
	        setTimeout(function(){
	            $('#overlay').fadeOut('slow', function () {

	            	$.notify("Spot Registrations available for all the events!!!", "info");
	            	$.notify("Amazing offers for Workshop participants Hurryup!!!", "info");
	            });
	        },1000);


	    });
		});
	</script>
<header class="hh-header header-top  w3-padding-64">
  	<div class="">
	   <a class="menu-icon">
			<div class="bars">
				<div class="bar1"></div>
				<div class="bar2"></div>
				<div class="bar3"></div>
			</div>
		</a>
		<!-- End of menu icon -->
	</div>
   <div class="menu-bar">
		<div class="menu-bar-item">
			<a href="proshows"><button class="menu-bar-btn">Proshows</button></a>
				<!-- no menu-bar-dropdown  -->
		</div>

		<div class="menu-bar-item w3-dropdown-hover ">
			<?php
			    if($_SESSION['p_id'])
			    {
                    echo '<button  class="menu-bar-btn">'.$_SESSION['p_id'].'</button>
					<div class="w3-dropdown-content w3-bar-block w3-black ">
			      <a href="profile" class="w3-bar-item ">Profile</a>
			      <a href="logout" class="w3-bar-item ">Logout</a>
			    </div>';
			    }
			    else
			    {
			    	echo '<a href="login"><button  class="menu-bar-btn">Login</button></a>';
			    }


			    ?>

			</div>
	</div>
</header>
	<div id="particles-js"></div>
	<nav class="page-menu">
		<div class="menu-wrapper w3-center">
			<ul id="qmenu" class="menu-list qmenu">
				<li data-menuanchor="Home">
					<a href="#Home">
						<span class="title">Home</span>
						<i class="icon lnr lnr-home"></i>
					</a>
				</li>
				<li data-menuanchor="About">
					<a href="#About">
						<span class="title">About Us</span>
						<i class="icon lnr lnr-flag"></i>
					</a>
				</li>
				<li data-menuanchor="Events">
					<a href="#Events">
						<span class="title">Events</span>
						<i class="icon lnr lnr-bullhorn"></i>
					</a>
				</li>
				<li data-menuanchor="Accommodation">
					<a href="#Accommodation">
						<span class="title">Accommodation</span>
						<i class="icon lnr lnr-apartment"></i>
					</a>
				</li>
				<li data-menuanchor="Gallery">
					<a href="#Gallery">
						<span class="title">Gallery</span>
						<i class="icon lnr lnr-camera"></i>
					</a>
				</li>
				<li data-menuanchor="Sponsors">
					<a href="#Sponsors">
						<span class="title">Sponsors</span>
						<i class="icon lnr lnr-briefcase"></i>
					</a>
				</li>
				<li data-menuanchor="Contact">
					<a href="#Contact">
						<span class="title">Contact</span>
						<i class="icon lnr lnr-phone"></i>
					</a>
				</li>
				<li data-menuanchor="proshows">
					<a href="proshows">
						<span class="title">Proshows</span>
						<i class="icon lnr lnr-music-note"></i>
					</a>
				</li>

			</ul>
		</div>
	</nav>
	<!-- END OF menu button page navigation -->

<div id="fullpage">

	<div class="section" id="home">
		<div class=" w3-display-container">
			  <div class="logo w3-display-middle">
				<span class="w3-hide-large"><br><br><br></span>
				<img class="w3-image" width="500" height="500" id="logo" src="images/logomm.png" ><span class="w3-hide-large"><br><br><br><br></span>
		        <div class="c-clock clock clock-countdown countdown w3-center" style="margin-top: -50px" >
					<div class="clock-wrapper">
						<div class="tile tile-days">
							<span class="days">00</span>
							<span class="txt">days</span>
						</div>
						<div class="tile tile-hours">
							<span class="hours">00</span>
							<span class="txt">hours</span>
						</div>
						<span class="w3-hide-large"><br></span>
						<div class="tile tile-minutes">
							<span class="minutes">00</span>
							<span class="txt">minutes</span>
						</div>
						<span class="w3-hide-large">&nbsp;&nbsp;&nbsp;</span>
						<div class="tile tile-seconds">
							<span class="seconds">00</span>
							<span class="txt">seconds</span>
						</div>
					</div>

				</div><br><br>

			</div>
		</div>

		 	 <div id="project-wrapper" class="w3-display-bottommiddle">
				<div id="project-container">
					<div id="overlay"></div>
					<div id="content">
			      <i class="fa fa-play play-btn fa-2x" aria-hidden="true" style="float: right;margin-right: 5%;"></i>
						<h2 class="w3-text-pink">Vidyanikethan Radio</h2>
						<h4 class="w3-text-cyan">CseArena</h4>
					</div>
				</div>
			</div>
	</div>
	<div class="section" id="about">
	    <section class="content w3-container"><br><br><br>
			<div class="w3-content w3-justify w3-text-white w3-padding-16" id="about">
					<h2 class="heading w3-text-pink" Style="font-family: 'Comfortaa', cursive;">About MohanaMantra</h2>
					<hr style="width:200px" class="w3-opacity">
				 <p style="font-family: 'Quicksand', sans-serif; font-size: 15px; font-weight:300;font-size:20px;"></span>Mohana Mantra is a National level Techno-Cultural fest commenced with an objective of uncovering the impeccable talents of young calibers through multifarious technical events,Cultural extravaganza and many frolic events.Sree Vidyanikethan initiated this nourishing experience from the last four years.Mohana Mantra  makes the best indelible memories and vivacious moments with the perfect blend of Knowledge and Entertainment.
				 </p>
				  <div class=""><center>
					  <img src="images/vidyanikethan.png" height="200" width="200" class="w3-image"alt="svec_logo"></center>
					</div>
				 <h2 class="heading w3-text-pink" style="font-family: 'Comfortaa', cursive;">About <span class="w3-hide-large" ><br></span>Sree Vidyanikethan</h2>
					<hr style="width:200px" class="w3-opacity">
				 <p style="font-family: 'Quicksand', sans-serif; font-size: 15px; font-weight:300;font-size:20px;">Sree Vidyanikethan Educational Trust came into existence with the noble idea of providing the best of education and facilities to every child in the country. Founded and operated under the visionary leadership of Legendary Actor Dr. M. Mohan Babu, the Trust has witnessed numerous achievements in the journey so far. While being an ingenious blend of multi-cultural and plural societies, the Trust is also endowed with cultivated traditions and values, thereby becoming one of the “Most Trusted names in education”. Over the years, the Trust has been growing rapidly, establishing academic institutions at Hyderabad, Tirupati and all over Andhra Pradesh and offering unmatched standards of academics in various realms of education in demand driven domains.
				 </p><br><br><br>

			</div>
	    </section>
	</div>
	<div class="section" id="events"><center>

		<section class="container"><br>
			<h1 class="heading w3-text-pink" style="font-family: 'Comfortaa', cursive;">Events</h1><br>
			<div class="row  ">
				<div class=" col-sm-4 ">
				<!-- colored -->
					<div class="ih-item circle colored effect3 bottom_to_top">
						<a href="technoholik"  >
							<div class="img"><img src="images/panels/events1.jpg" alt="img"></div>
						<div class="info">
						  <h3>TECHNOHOLIK</h3>
						  <b><p style="font-size: 0.8em" class="w3-text-white">Explore.Enchance.Enchant</p></b>
						</div>
						</a>
					</div>
				<!-- end colored -->
			      	<h3 class="heading w3-text-cyan" style="font-family: 'Comfortaa', cursive;">TECHNOHOLIK</h3>
			  	</div>
			  <div class=" col-sm-4 ">
				<div class="ih-item circle colored effect3 bottom_to_top"><a href="kalakshetra">
					<div class="img"><img src="images/panels/kala.png" alt="img"></div>
					<div class="info">
					  <h3>KALAKSHETRA</h3>
					  <b><p style="font-size: 0.9em" class="w3-text-white">Art.Culture.Fun</p></b>
					</div></a></div>
				<!-- end colored -->
				<h3 class="heading w3-text-orange" style="font-family: 'Comfortaa', cursive;">KALAKSHETRA</h3>

			  </div>
			  <div class=" col-sm-4">

				<!-- colored -->
				<div class="ih-item circle colored effect3 bottom_to_top"><a href="workshops">
					<div class="img"><img src="images/panels/workshop.jpg" alt="img"></div>
					<div class="info">
					  <h3>WORKSHOPS</h3>

					  <b><p style="font-size: 0.9em" class="w3-text-white">Seek.Imbibe.Exhibit</p></b>
					</div></a></div>
				<!-- end colored -->
				<h3 class="heading w3-text-cyan" style="font-family: 'Comfortaa', cursive;">WORKSHOPS</h3>

			  </div>

		</div>

		<div class="row  ">
			<div class="col-sm-4 ">

				<!-- colored -->
				<div class="ih-item circle colored effect3 top_to_bottom"><a href="spotevents">
					<div class="img"><img src="images/panels/spot_events.png" alt="img"></div>
					<div class="info">
					  <h3>SPOT EVENTS</h3>
					  <b><p style="font-size: 0.9em" class="w3-text-white">Amuse.Giggle.Glitter</p></b>
					</div></a></div>
				<!-- end colored -->
				<h3 class="heading w3-text-orange" style="font-family: 'Comfortaa', cursive;">SPOT EVENTS</h3>
			  </div>

			  <div class="col-sm-4 ">

				<!-- colored -->
				<div class="ih-item circle colored effect3 top_to_bottom"><a href="pharmacy">
					<div class="img"><img src="images/panels/pharm.jpg" alt="img"></div>
					<div class="info">
					  <h3>PHARMACY</h3>
					  <b><p style="font-size: 0.9em" class="w3-text-white">Crave.Innovate.Formulate</p></b>
					</div></a></div>
				<!-- end colored -->
				<h3 class="heading w3-text-cyan" style="font-family: 'Comfortaa', cursive;">PHARMACY</h3>

			  </div>
			  <div class="col-sm-4 ">


				<div class="ih-item circle colored effect3 top_to_bottom"><a href="master&management">
					<div class="img"><img src="images/panels/manage.jpg" alt="img"></div>
					<div class="info">
					  <h3>MASTER'S&nbsp;&<br>MANAGEMENT</h3><br>
					  <b><p style="font-size: 0.9em" class="w3-text-white">Rise.Manage.Shine</p></b>
					</div></a></div>

				<h3 class="heading w3-text-orange style="font-family: 'Comfortaa', cursive;">MASTER'S & MANAGEMENT</h3>

			  </div><br><br><br>

			</div><br><br><br>
			</center>
		</section>
	</div>
	<div class="section" id="accommodation">
	   <section class="content w3-container"><br><br><br>
			<div class="w3-content w3-justify w3-text-white w3-padding-16" id="about">
					<h2 class="heading w3-text-pink" Style="font-family: 'Comfortaa', cursive;">Accommodations</h2>
					<hr style="width:200px" class="w3-opacity">
				 <p style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300;font-size:20px;"><span class="w3-hide-large" style="font-size:15px;"></span>We know that you have made your way very long to make it to Mohana Mantra and we are here to assure that your stay here is safe and secure.From accommodation to Transportation everything will be prior and accommodated in Vidyanikethan Hostels or in Private facilities in city. We make your stay a good experience .
				 </p>
				 <div class="buttons border col-md-4 col-md-offset-4">
				 <a class="abtn orange" href="accommodation"><span><b>Registration Open<br></b></span></a>
				 </div>
			</div>
	    </section>
	</div>
	<div class="section" id="gallery">
        <h1 class="heading w3-text-pink w3-center" style="font-family: 'Comfortaa', cursive;">Gallery</h1>
		<div class="w3-container ">
			<div class="parent">
    			<div class="rotate">
        		<ul class="rotate-ul">
	            <li data-link="#" class="rotate-ul-li rotate-a"></li>
	            <li data-link="#" class="rotate-ul-li rotate-b"></li>
	            <li data-link="#" class="rotate-ul-li rotate-c"></li>
	            <li data-link="#" class="rotate-ul-li rotate-d"></li>
	            <li data-link="#" class="rotate-ul-li rotate-e"></li>
	            <li data-link="#" class="rotate-ul-li rotate-f"></li>
	            <li data-link="#" class="rotate-ul-li rotate-g"></li>
	            <li data-link="#" class="rotate-ul-li rotate-h"></li>
	            </ul>
               </div>
            </div>
		</div>
	</div>
	<div class="section" id="sponsors">

		<section class="container-fluid">
            <div class="row " >
                <div class="col-md-10 col-md-offset-1 ">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 text-center w3-text-pink">
                            <h1 style="font-family: 'Comfortaa', cursive;">Title Sponsor</h1><br>
                            <img src="images/sponsors/cc.jpg" width="400" class="img-responsive sponsor_img img-rounded">
                        </div>
                    </div>

                    <div class="row w3-text-cyan">
                    	<div class="col-md-6 text-center"><br><br>
                            <div class="col-md-12">
								 <h2 style="font-family: 'Comfortaa', cursive;">Associate Sponsor</h2>
							</div>
	                        <figure>
							  <div class="image"><img src="images/sponsors/zero.jpg" class="w3-image img-rounded" alt="Associate Sponsor"/></div>
							</figure>
                        </div><br><br>
                        <div class="col-md-6 text-center">
                            <div class="col-md-12">
								 <h2 style="font-family: 'Comfortaa', cursive;">Beauty Partner</h2>
							</div>
	                        <figure>
							  <div class="image"><img src="images/sponsors/bts.png" class="w3-image   img-rounded" alt="Beauty Partner"/></div>
							</figure>
                        </div>
                </div><br>
                 <h2 class="w3-text-pink w3-center" style="font-family: 'Comfortaa', cursive; background:rgba(0,0,0,0.7);">Event Sponsors</h2><br>
                  <div class="row w3-text-cyan">
                  	<div class="col-md-4">
                  		<figure>
							  <div class="image">
                  		<img src="images/sponsors/logo2.jpg" class="w3-image  img-rounded" alt="codechef"/></div></figure>
                  	</div>
                  	<div class="col-md-4">
                  		<figure>
							  <div class="image"><img src="images/sponsors/logo3.jpg" class="w3-image   img-rounded" alt="robocart"/></div></figure>
                  	</div>
                  	<div class="col-md-4">
                  		<figure>
							  <div class="image"><img src="images/sponsors/logo4.jpg" class="w3-image   img-rounded" alt="Canter Cadd"/></div></figure>
                  	</div>
                  	<div class="col-md-4 ">
                  		<figure>
							  <div class="image"><img src="images/sponsors/logo5.jpg" class="w3-image   img-rounded" alt="robocart"/></div></figure>
                  	</div>
                  	<div class="col-md-4">
                  		<figure>
							  <div class="image"><img src="images/sponsors/logo6.jpg" class="w3-image   img-rounded" alt="Canter Cadd"/></div></figure>
                  	</div>
                    <div class="col-md-4">
                  		<figure>
							  <div class="image"><img src="images/sponsors/cl.jpg" class="w3-image   img-rounded" alt="Cl"/></div></figure>
                  	</div>
                </div><br><br>
                 <br><br>
            </div>
        </section>
	</div>
  <div class="section" id="contact">
			    <br><br>
			    <h1 class="heading w3-text-pink w3-center" style="font-family: 'Comfortaa', cursive;">Contact Us</h1>
		    <div class="container-fluid"><br>
		        <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">
		        	<div class="col-md-12 col-md-offset-2">
					<div class="col-md-3 col-md-offset-1 text-center" style="margin-left: 20px">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/sc1.jpeg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Vamsi Kvsrk</h3>
							<p class="w3-text-white">Student Coordinator</p>
							<p class="w3-text-white">+91 76809 91993</p>
						</div>
					</div>
					<div class="col-md-3 col-md-offset-1 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/sc2.jpg" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">PG Dinesh</h3>
							<p class="w3-text-white">Student Coordinator</p>
							<p class="w3-text-white">+91 81254 81299</p>
						</div>
					</div>
					<!--<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/wd.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Avinash Valluru</h3>
							<p class="w3-text-white">Web Developer</p>
							<p class="w3-text-white">+91 7799773346</p>
						</div>
					</div>-->
					</div>
				</div>

				   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pr1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Sreekanth</h3>
							<p class="w3-text-white">Public Relations</p>
							<p class="w3-text-white">+91 86888 89375</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pr2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Monika</h3>
							<p class="w3-text-white">Public Relations</p>
							<p class="w3-text-white">+91 91607 51064</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/hospi1.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Gowthashya Mouli</h3>
							<p class="w3-text-white">Hospitality</p>
							<p class="w3-text-white">+91 98480 23553</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/hospi2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Nithish Reddy</h3>
							<p class="w3-text-white">Hospitality</p>
							<p class="w3-text-white">+91 7799773346</p>
						</div>
					</div>

				</div>

				   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/fac1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Varshith</h3>
							<p class="w3-text-white">Facilities</p>
							<p class="w3-text-white">+91 97009 05948</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/fac2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Sarfaraz</h3>
							<p class="w3-text-white">Facilities</p>
							<p class="w3-text-white">+91 96032 13769</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/market1.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Paartheesh</h3>
							<p class="w3-text-white">Marketing</p>
							<p class="w3-text-white">+91 70329 42705</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/market2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Anudeep</h3>
							<p class="w3-text-white">Marketing</p>
							<p class="w3-text-white">+91 82971 61559</p>
						</div>
					</div>

				</div>
					   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/log1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Pawan Kumar</h3>
							<p class="w3-text-white">Logistics</p>
							<p class="w3-text-white">+91 91602 84061</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/log2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Madhavi</h3>
							<p class="w3-text-white">Logistics</p>
							<p class="w3-text-white">+91 97014 82902</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/fin.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Apoorva Reddy</h3>
							<p class="w3-text-white">Finance</p>
							<p class="w3-text-white">+91 94943 20807</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/dec.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Lakshmi Teja</h3>
							<p class="w3-text-white">Decorations</p>
							<p class="w3-text-white">+91 91008 02844</p>
						</div>
					</div>

				</div>
					   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/tech1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Sai Vamsi</h3>
							<p class="w3-text-white">Technoholik</p>
							<p class="w3-text-white">+91 83417 31765</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/tech2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Kunal Singh</h3>
							<p class="w3-text-white">Technoholik</p>
							<p class="w3-text-white">+91 95812 16803</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/kala1.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Siri</h3>
							<p class="w3-text-white">Kalakshetra</p>
							<p class="w3-text-white">+91 97015 43206</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/kala2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Afrooz</h3>
							<p class="w3-text-white">Kalakshetra</p>
							<p class="w3-text-white">+91 90109 86487</p>
						</div>
					</div>

				</div>
					   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/work1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>L.C Gowtham</h3>
							<p class="w3-text-white">Workshops</p>
							<p class="w3-text-white">+91 8074073425</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/work2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Keerthana</h3>
							<p class="w3-text-white">Workshops</p>
							<p class="w3-text-white">+91 94924 95525</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/spot1.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Shashank</h3>
							<p class="w3-text-white">Spot Events</p>
							<p class="w3-text-white">+91 81878 30991</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/spot2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Shiva Prakash</h3>
							<p class="w3-text-white">Spot Events</p>
							<p class="w3-text-white">+91 95812 16803</p>
						</div>
					</div>

				</div>
					   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pub1.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Amarnath</h3>
							<p class="w3-text-white">Publicity</p>
							<p class="w3-text-white">+91 94949 38599</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pub2.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Jessi Praneeth</h3>
							<p class="w3-text-white">Publicity</p>
							<p class="w3-text-white">+91 96039 08204</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pro1.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Chakradhar</h3>
							<p class="w3-text-white">Proshows</p>
							<p class="w3-text-white">+91 95508 93959</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pro2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Akhil Sai</h3>
							<p class="w3-text-white">Proshows</p>
							<p class="w3-text-white">+91 99123 21776</p>
						</div>
					</div>

				</div>
					   <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">

					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/pharm.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Kathyainai</h3>
							<p class="w3-text-white">Pharmacy Representative</p>
							<p class="w3-text-white">+91 82473 96337</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/mca.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Chaitanya</h3>
							<p class="w3-text-white">MCA Representative</p>
							<p class="w3-text-white">+91 99669 90343</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/mba.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Meghanath</h3>
							<p class="w3-text-white">MBA Representative</p>
							<p class="w3-text-white">+91 86889 90566</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/deg.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Amrutha</h3>
							<p class="w3-text-white">Degree Representative</p>
							<p class="w3-text-white">+91 90590 26954</p>
						</div>
					</div>

				</div>
				  <div class="row w3-center " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">
		        	<div class="col-md-12 col-md-offset-1">
					<div class="col-md-3  text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/wd.jpg" alt="sc1" /></div>
						</figure>
						<div class="col-md-12 heading w3-center w3-text-cyan">
							<h3>Avinash</h3>
							<p class="w3-text-white">Web-Designer</p>
							<p class="w3-text-white">+91 77997 73346</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/des1.jpg" height="215" alt="sc2"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Lalith Kumar</h3>
							<p class="w3-text-white">Designer</p>
							<p class="w3-text-white">+91 95534 34031</p>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<figure class="snip0069">
						  <div class="image"><img src="images/team/des2.jpg" alt="wd"/></div>
						</figure>
						<div class="col-md-12">
							<h3 class="heading w3-center w3-text-cyan">Seshasai</h3>
							<p class="w3-text-white">Designer</p>
							<p class="w3-text-white">+91 91775 	73085</p>
						</div>
					</div>
					</div>
				</div> <br><br><br>
    			<div class="row contactinfo col-md-offset-1 " style="font-family: 'Quicksand', sans-serif; font-size: 17px; font-weight:300; color: white">
       			 	<div class="col-sm-4" >
	            		<span class="icon w3-text-pink lnr lnr-map-marker" aria-hidden="true"></span>
				        <span class="title heading w3-text-cyan">Address</span>
				        <div class="value">
				            Sree Vidyanikethan Engineering College (Autonomous)<br>
							Sree Sainath Nagar<br>
							Tirupati<br>
							Andhra Pradesh - 517102.<br>
							<a href="policy" class="w3-text-pink" target="_blank">Payment Policy</a><br>
				        </div>
				    </div>
				    <div class="col-sm-4" >
				        <span class="icon w3-text-pink lnr lnr-phone-handset" aria-hidden="true"></span>
				        <span class="title heading w3-text-cyan">Phone</span>
				        <div class="value">
				            0877 - 2236711, 12, 13, 14<br>
				            0877 - 3066900
	              		</div>
	                </div>
	                <div class="col-sm-4" >
			            <span class="icon w3-text-pink lnr lnr-envelope"" aria-hidden="true"></span>
			            <span class="title heading w3-text-cyan">Email</span>
			            <div class="value">
			                 mm@vidyanikethan.edu<br>
			                 connect@mohanamantra.com
			            </div>
			        </div>
			             <div class="value">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                <a href="https://www.facebook.com/MohanaMantraSVEI" target="_blank"><span class="fb"><i class="fa fa-facebook-square  w3-xxlarge"></i></span></a>
							<a href="https://twitter.com/mohana_mantra?lang=en" target="_blank"><span class="twitter"><i class="fa fa-twitter-square  w3-xxlarge"></i></span></a>
							<a href="https://plus.google.com/+MohanaMantra" target="_blank"><span class="google"><i class="fa fa-google-plus-square w3-xxlarge"></i></span></a>
							<a href="https://www.instagram.com/mohana_mantra/?hl=en"  target="_blank"><span class="insta"><i class="fa fa-instagram   w3-xxlarge"></i></span></a>
							<a href="https://www.youtube.com/channel/UC_PbePIyi447OEdMeoHfWuA" target="_blank"><span class="youtube"><i class="fa fa-youtube-square  w3-xxlarge"></i></span></a><br>
							<!--<h6 class="w3-right w3-margin">Crafted By <a href="https://www.facebook.com/avinash.valluru" target="_blank"><span class="w3-text-pink" style="font-family: 'Comfortaa', cursive;">Avinash Valluru</a><br>+91 7799 7733 46</span></h6>-->

			            </div>
			    </div>
        	</div><br><br><br>
	</div>
</div>

<script>
	// 3. Show/hide menu when icon is clicked
    var menuItems = $('.page-menu');
    var menuIcon = $('.menu-icon,#menu-link');
    // Menu icon clicked
    menuIcon.on('click', function () {
        if (menuIcon.hasClass('menu-visible')) {
            menuIcon.removeClass('menu-visible');
        } else {
            menuIcon.addClass('menu-visible');
        }
        if (menuItems.hasClass('menu-visible')) {
            menuItems.removeClass('menu-visible');
        } else {
            menuItems.addClass('menu-visible');
        }
        return false;
    });
    // Hide menu after a menu item clicked
    $(".page-menu a").on('click',function(){
        if (menuItems.hasClass('menu-visible')) {
            menuIcon.removeClass('menu-visible');
            menuItems.removeClass('menu-visible');
        }
        return true;
   		 });
</script>
<script class="source" type="text/javascript">
        $('.countdown').downCount({
            date: '09/25/2017 14:00:00',
            offset: +10
        }, function () {
            alert('The Wait is Over!');
        });
</script>
<script>
	/* ---- particles.js config ---- */
	particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 350,
      "density": {
        "enable": true,
        "value_area":1000
      }
    },
    "color": {
      "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
    },

    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#fff"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "img/github.svg",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 0.6,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 2,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": false,
      "distance": 190,
      "color": "#ffffff",
      "opacity": 0.4,
      "width": 1
    },
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable":false,
        "mode": "grab"
      },
      "onclick": {
        "enable": false
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 140,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 500,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect":false
   });
</script>
<script>
$('.play-btn').click(function() {
  if ( $(this).hasClass("fa-play") ) {
    $('<audio id="Audio"><source src="http://78.129.224.13/stream.mp3?ipport=78.129.224.13_23545" type="audio/mpeg"></audio>').appendTo('body');
      $('#Audio')[0].play();
    $(this).removeClass("fa-play");
    $(this).addClass("fa-pause");
  } else {
    $("#Audio").remove();
    $(this).removeClass("fa-pause");
    $(this).addClass("fa-play");
  }
});

$('.column').click(function() {
  $(this).toggleClass("active");
})
</script>

</body>
</html>
