<?php
include("../db_config.php");
    include("../functions.php");
     session_start();

  ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Spot Events | Mohana Mantra 2K17</title>
    <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
    <meta name="author" content="Avinash Valluru"/>
    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/nprogress.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/w3.css">
     <link rel="stylesheet" href="../css/tabs.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland" rel="stylesheet">

     <script src="../js/jquery.min.js"></script>
     <script type="text/javascript" src="../js/notify.js"></script>
     <script src="../js/nprogress.js"></script>
    <style>
        html, body{
          height: 100%;
        }
        body {
              background-image: url(img/spot.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;

        }
        figure{
            transform: scale(0.8);
        }
        h1{
                          font-size: 5.5em;
                            font-family: Iceland;

                    }
             @media screen and (max-width: 480px) {
                h1{
                    transform: scale(0.8);
                }
      }
    </style>

</head>
<body>
<script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<?php include("nav.php");?>
             <h1 class="heading w3-text-pink w3-center" style="padding-top: 48px;" >Spot Events</h1>
<div class="container" >

        <figure class="snip1091 red"><img src="img/game.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Lan Gaming</span></h2>
          </figcaption><a href="lan_game"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/bdp.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Best Dp</span></h2>
          </figcaption><a href="bestdp"></a>
        </figure>
       <!-- <figure class="snip1091 red"><img src="img/dpt.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">DP Treasure hunt</span></h2>
          </figcaption><a href="#"></a>
        </figure>-->
        <figure class="snip1091 navy"><img src="img/fz.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Fun zone</span></h2>
          </figcaption><a href="funzone"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/kabadi.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Kabaddi</span></h2>
          </figcaption><a href="kabaddi"></a>
        </figure>
        <!--<figure class="snip1091 navy"><img src="img/hr.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Horror House</span></h2>
          </figcaption><a href="#"></a>
        </figure>-->
        <figure class="snip1091 red"><img src="img/thunt.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Treasure Hunt</span></h2>
          </figcaption><a href="treasure_hunt"></a>
        </figure>
        <!--<figure class="snip1091 red"><img src="img/GC.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Gully Cricket</span></h2>
          </figcaption><a href="gullycricket"></a>
        </figure>-->

        <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p>In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="center">
                                <h5>Shashank</h5>
                                <h5>+91 81879 30991</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <h5>Siva Prakash</h5>
                                <h5>+91 73966 50671</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>

</div>

<script>

  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );
</script>
</body>

</html>
