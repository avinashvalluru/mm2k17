    <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

    ?>   
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>SpotEvents| Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        html, body{
          height: 100%;
        }
        body { 
              background-image: url(img/spot.jpg) ;
              background-position: center center;
              background-repeat:  no-repeat;
              background-attachment: fixed;
              background-size:  cover;
              background-color: #999;
          
        }


        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {   
          h1{
            transform: scale(0.7);
            
          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }   
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

    </head>
    <body>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br> 
      <h1 class="heading w3-text-pink w3-center" style="" >FuN ZoNe</h1><br>
      <div class="container">
       <div class="row">
        <div class="col-md-10 col-md-offset-1">

          <div class="left-menu">
            <div class="accordion">
              <div class="section">
                <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
                <label for="section-1"><span><h3>Spot Lights</h3></span></label>
                <div class="content">
                  <p>
                   Fun one is a zone of exciting, refreshing and extremely funny events. Events in fun zone renders bag full of entertainment and will be as flashy as colors and rainbow.
                  </p>
                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
                <label for="section-2"> <span><h3>Horror House</h3></span></label>
                <div class="content">
                    <p>
                    The Horror house adds more excitement to the fest . Horror house consists of many scary
                    visualizations which would be thrilling and even scary .A walk into Horror house will be
                    never delightfull
                 </p>
                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
                <label for="section-3"> <span><h3>MORE</h3></span></label>
                <div class="content">
                    <p>
                    • Electric Bull ride <br>
                    • Sting the ring<br> 
                    • Food events <br>
                    • Hit the wicket <br>
                    • Arm wrestling <br>
                    • Dart board<br>
                 </p>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div><br><br><br><br><br>
                  <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray text-center">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p class="w3-text-black">In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="center">
                                <h5>Satya</h5>
                                <h5>+91 97047 03913</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <h5>Siva Prakash</h5>
                                <h5>+91 73966 50671</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>
         
  </body>
</html>