<?php

$con->query("CREATE TABLE IF NOT EXISTS `mm_team` (
  `team_mem_id` int(20) NOT NULL AUTO_INCREMENT,
  `team_mem_roll_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `team_mem_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `team_mem_panel_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `team_mem_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`team_mem_id`),
  UNIQUE KEY  (`team_mem_roll_no`)
  )ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=100;");

$con->query("CREATE TABLE IF NOT EXISTS `participants` (
  `p_id` varchar(20) NOT NULL ,
  `full_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `col_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `reg_mode` enum('Spot','Onsite','Self','Intra') DEFAULT NULL,
  `reg_by` varchar(20) DEFAULT NULL,
  `txn_no` varchar(20) NOT NULL,
  PRIMARY KEY (`p_id`),
  UNIQUE KEY  (`email`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1005 ;");

$con->query("CREATE TABLE IF NOT EXISTS `mmid_issue` (
  `reg_id` varchar(20) NOT NULL ,
  `mm_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `issue_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`reg_id`),
  UNIQUE KEY  (`mm_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1005 ;");

$con->query("CREATE TABLE IF NOT EXISTS `workshops` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

$con->query("CREATE TABLE IF NOT EXISTS `spotevents` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

$con->query("CREATE TABLE IF NOT EXISTS `proshows` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

$con->query("CREATE TABLE IF NOT EXISTS `pharmacy` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

$con->query("CREATE TABLE IF NOT EXISTS `tech` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

$con->query("CREATE TABLE IF NOT EXISTS `kala` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");




$con->query("CREATE TABLE IF NOT EXISTS `acc` (
  `transaction_id` varchar(30) NOT NULL ,
  `p_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `atime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `vdate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `vtime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(20)NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");


$con->query("CREATE TABLE IF NOT EXISTS `acc_issue` (
  `p_id` varchar(20) NOT NULL ,
  `gender` varchar(20) NOT NULL ,
  `adate` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `vdate` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mode` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `issue_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`txn_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1005 ;");



/*mysql_query("CREATE TABLE IF NOT EXISTS `subjects` (
  `sub_id` int(20) NOT NULL AUTO_INCREMENT,
  `sub_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sub_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sub_id`),
  UNIQUE KEY  (`sub_code`)
  ) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2000 ;");

mysql_query("CREATE TABLE IF NOT EXISTS `exams` (
  `exam_id` int(20) NOT NULL  AUTO_INCREMENT,
  `exam_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sub_id` int(20) NOT NULL,
  `add_date` date DEFAULT NULL,
  `add_time` time DEFAULT NULL,
  `exam_from` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exam_to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(11) DEFAULT NULL,
  `total_ques` int(11) DEFAULT NULL,
  `attempt_students` bigint(20) DEFAULT NULL,
  `examiner_id` int(20),
  PRIMARY KEY (`exam_id`),
  UNIQUE KEY  (`exam_name`,`sub_id`),
    INDEX `par_ind` (`sub_id`),
    FOREIGN KEY (`sub_id`) REFERENCES `subjects`(`sub_id`) ON DELETE RESTRICT

  ) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5000 ;");

mysql_query("CREATE TABLE `question` (
  `exam_id` int(20) NOT NULL DEFAULT '0',
  `q_id` int(11) NOT NULL DEFAULT '0',
  `q_name` varchar(500) DEFAULT NULL,
  `optiona` varchar(100) DEFAULT NULL,
  `optionb` varchar(100) DEFAULT NULL,
  `optionc` varchar(100) DEFAULT NULL,
  `optiond` varchar(100) DEFAULT NULL,
  `cor_answer` enum('optiona','optionb','optionc','optiond') DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  PRIMARY KEY (`exam_id`,`q_id`),
   INDEX `exa_ind` (`exam_id`),
    FOREIGN KEY (`exam_id`) REFERENCES `exams`(`exam_id`) ON DELETE RESTRICT

) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  ;");
mysql_query("CREATE TABLE `stu_answers` (
  `stu_id` int(20) NOT NULL DEFAULT '0',
  `exam_id` int(20) NOT NULL DEFAULT '0',
  `q_id` int(11) NOT NULL DEFAULT '0',
  `answered` enum('answered','unanswered','review') DEFAULT NULL,
  `stdanswer` enum('optiona','optionb','optionc','optiond') DEFAULT NULL,
  PRIMARY KEY (`stu_id`,`exam_id`,`q_id`),
  KEY `exam_id` (`exam_id`,`q_id`),
  CONSTRAINT `studentanswer_ibfk_1` FOREIGN KEY (`stu_id`) REFERENCES `students` (`stu_id`),
  CONSTRAINT `studentanswer_ibfk_2` FOREIGN KEY (`exam_id`, `q_id`) REFERENCES `question` (`exam_id`, `q_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  ;");

mysql_query("CREATE TABLE `student_test` (

  `stu_id` int(20) NOT NULL DEFAULT '0',
  `exam_id` int(20) NOT NULL DEFAULT '0',
  `starttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `correctlyanswered` int(11) DEFAULT NULL,
  `status` enum('over','inprogress') DEFAULT NULL,
  PRIMARY KEY (`stu_id`,`exam_id`),
  KEY `exam_id` (`exam_id`),
  CONSTRAINT `studenttest_ibfk_1` FOREIGN KEY (`stu_id`) REFERENCES `students` (`stu_id`),
  CONSTRAINT `studenttest_ibfk_2` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`)
  ) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  ;");

  mysql_query("CREATE TABLE `stu_feedback` (

  `sno` int(20) NOT NULL AUTO_INCREMENT,
  `stu_id` int(20) NOT NULL DEFAULT '0',
  `feedback` varchar(550) NOT NULL,
  PRIMARY KEY (`sno`),
  CONSTRAINT `studentfeedback_ibfk_1` FOREIGN KEY (`stu_id`) REFERENCES `students` (`stu_id`)
  ) ENGINE=INNODB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  ;");

  $sql = "ALTER TABLE `stu_feedback` ADD UNIQUE(`stu_id`)";
  mysql_query($sql);*/
  ?>
