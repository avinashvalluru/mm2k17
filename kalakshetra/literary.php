   <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>LITERARY | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>

    html, body{
      height: 100%;
    }
    body {
      background-image: url(img/bg/literary.jpg) ;
      background-position: center center;
      background-repeat:  no-repeat;
      background-attachment: fixed;
      background-size:  cover;
      background-color: #999;

    }



    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-white w3-center" style="" >Youth Parliament (Mock Parliament)</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
              <label for="section-1"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  •Movies, Music and Masthi! Is this all today’s youth is concerned about? Of course no, you might answer “We create awareness about social issues, comment on politicians BUT we do it on Facebook, Twitter and YouTube”.<br>
                  •What would you do if you were a parliamentarian bestowed with the opportunity to Raise Your Voice and speak about country’s issues? How would you highlight the problems of the country and fight for them. Could you be the best parliamentarian? Your words, moves and attitude… everything matters! Gear up youngsters of India, here’s a chance to voice your opinions as a LEADER in The Youth Parliament. <br>
                  •What’s more? You win awesome prizes and the Final Winner will be awarded the title <b>“Best ORATOR OF MOHANA MANTRA -2K17”</b><br>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>HOW IS IT DONE?</h3></span></label>
              <div class="content">
                <p>
                 Round 1: You will have to present a speech of 3 minutes (this session will be like JAM).<br>
                 Round 2:
                 PART 1: The selected participants will be divided into Ruling and Opposing parties on based on lucky dip. Ministries will also be allotted.<br>
                 PART 2: The teams will be given 5-10 minutes to discuss with each other. There after the winning party will be passing a bill. The participants have to randomly speak on the topic spontaneously. (Grab, Speak and Pin your Point)<br>
                 -> Based on their various attributes the judges will decide the winners.<br>
                 NOTE: Be prepared to answer, counter, tackle and LISTEN.
                 <br>


               </p>
             </div>
           </div>
           <div class="section">
            <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
            <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                The registration fee for this event is Rs.50/-

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"> <span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
                Exciting  prizes will be awarded and amazing goodies

              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <h1 class="heading w3-text-white w3-center" style="" >ENGLISH CHAMPION</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-81" checked="checked"/>
              <label for="section-81"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  Savoring the literary beauty in Jane Austen lines?<br>
                  Bewitched by the wizards laden Harry Potter Series?<br>
                  Has an eye to spot grammatical errors?<br>
                  Simply Put, Is English your strength?<br>
                  Is Literature your love?<br>
                  If the answer is yes, it’s time for you to see the limelight… Come and become the English Champion!<br>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-82" value="toggle"/>
              <label for="section-82"> <span><h3>INSTRUCTIONS</h3></span></label>
              <div class="content">
                <p>
                 •  Prelims (a written test) will be held at first.<br>
                • Finals will be for the top 6 teams selected in prelims.<br>
                • A group can have maximum of 2 participants.<br>
                • Questions will be on authors, famous novels from the recent past, classic novels, spellings, word building etc.<br>
                • According to the sub-rounds the time will be allotted to each group<br>

               </p>
             </div>
           </div>
           <div class="section">
            <input type="radio" name="accordion-1" id="section-83" value="toggle"/>
            <label for="section-83"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                Rs.50/- per group or Rs.25/- if registered individually.

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-84" value="toggle"/>
            <label for="section-84"> <span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
               Cash prize will be awarded and amazing goodies.

              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >F.R.I.E.N.D.S</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                <b>Friendship is born at the moment when one person says to another :”What ! you,too?Thought I was the only one”.Here”s an event specifically designed to refresh all those memories and bring back Nostalgia of the good old times!!Take A Simple test to discover how well do your friends know about you and you about them. Surprise your friends and make their day ,a bit more brighter !!</b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • Participants will be seated individually i.e. away from their co-participants.<br>
• A written test will be conducted where each of them has to answer a questionnaire which comprises questions about their friends and themselves.<br>
• Prizes will be awarded to those participants whose answers match the most.<br>
• A team should consist of two participants.<br>


              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
            <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                    Rs.40/- per team.

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"> <span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
                    Bag fabulous prizes and amazing goodies

              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >Heads And Tails (Debate)</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-5" checked="checked"/>
            <label for="section-5"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>“In all debates, let truth be thy aim, not victory, or an unjust interest.” </b>
                <b>― William Penn</b><br>
                How do you mount or share your knowledge, thoughts and ideas? Especially when they are contrary! The right place is in a DEBATE - <b>A WAR</b> between the <b>‘FOR’</b> and the <b>‘AGAINST’. </b>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
            <label for="section-6"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •  There will be 2 rounds.<br>
               • In the Round 1, all the participants are requested to speak on the topic provided by the committee from the topics given below in Round-1.<br>
               • Take the turn either for ‘for’ or ‘against’ (3 minutes for presentation and 2 minutes for rebuttal).<br>
               • Be ready for answering the queries.<br>
               • Good speakers (evaluated based on presentation, facts, examples and rebuttal) as suggested by judges will get to Round-2.<br>
               • Round-2 is a real debate turn.<br>
               • The topic for Round-2 is given below.<br>
               • The participants will be asked for who’s in favor of the topic and who’s not and if the division of the participants for ‘Affirmative’ and ‘Negative’ teams are appropriate then the teams will be divided accordingly otherwise there will be a lucky dip.<br>
               • The top debaters chosen by the judges will be awarded with exciting prizes and goodies.<br>



             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
          <label for="section-7"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
              • The registration fee for this event is Rs.50/-
            </p>

          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
          <label for="section-8"> <span><h3>PRIZES</h3></span></label>
          <div class="content">
            <p>
             Exciting  prizes will be awarded and amazing goodies.!
           </p>

         </div>
       </div>
     </div>
   </div>
  <h3 class="w3-center w3-text-white">Registration Fee : 50/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="50"/>
         <input type="hidden" name="ename" value="Heads And Tails -Debate"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='Heads And Tails -Debate' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >MIGHTIER PENS</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-9" checked="checked"/>
            <label for="section-9"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                Are you good at writing? Do you pen down words giving them a rhythm? Be it a humorous article or a poem that rhymes well with all its verses... Write for creative excellence and earn fame!<br>
                 Submit your write ups to  <b>literary.mm2k17@gmail.com</b> well in advance and bag fabulous prizes.
             </p>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >POTTER MANIA</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-17" checked="checked"/>
            <label for="section-17"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  Bewitched by the wizards laden harry potter series? Can’t forget the fun in quidditich? Are you the ultimate Harry Potter fan? This is our invitation to all the Potterheads,out there. Test your wizarding knowledge and give this quiz a go!

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-18" value="toggle"/>
            <label for="section-18"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • A team consists of 2 participants.<br>
                • Prelims will be held first.<br>
                • Finals will be for the top 6 teams selected in prelims.<br>
                • According to the sub-rounds the time will be allotted to each group.<br>
                • Questions will be from Harry Potter Series.<br>

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-19" value="toggle"/>
            <label for="section-19"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               • Rs.50/- per group or Rs.25/- if registered individually.
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-20" value="toggle"/>
          <label for="section-20"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             •Bag fabulous prizes and amazing goodies .

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >Can You Guess?</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-21" checked="checked"/>
            <label for="section-21"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Fun has no limits... in Mohana Mantra 2k17 its boundless! Yes, new games with new avenues are awaiting your presence. Backed with wit and spontaneity you are deemed to grab fabulous prizes. <br>

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-22" value="toggle"/>
            <label for="section-22"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
               •  A team consists of 2 participants.<br>
               • There will be a closed box in which an object is kept.<br>
               • One of the participants is requested to check out what is inside the box and should not reveal it under any circumstances.<br>
               • The other participant should come up with a series of questions that can be answered with “Yes” or “No” by his/her teammate.<br>
               • For example if a pen is kept inside the box then questions can be like…. Is it a living thing or a non-living thing? Do we use it every day? Is it circular in shape? Etc.<br>
               • The number of questions posed is limited to 20.<br>
               • The team which comes with the solution in less time will be awarded exciting prizes/goodies.<br>





             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-23" value="toggle"/>
          <label for="section-23"> <span><h3>REGISTRATION FEE</h3></span></label>
          <div class="content">
            <p>
             Rs.20/- per team
           </p>

         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-24" value="toggle"/>
        <label for="section-24"> <span><h3>PRIZE</h3></span></label>
        <div class="content">
          <p>
           Bag fabulous prizes and amazing goodies .

         </p>

       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" > STATE YOUR STATUS </h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-27" checked="checked"/>
            <label for="section-27"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Be cheeky, witty, nonsensical or anything. What is the best WhatsApp status that you can come up with?

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-28" value="toggle"/>
            <label for="section-28"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •A theme will be given to the participants.<br>
               • The participant must come up with a creative status in time.<br>
               • The statuses will be judged on the basis of their catchiness, grammar and originality.<br>


             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-29" value="toggle"/>
          <label for="section-29"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             Rs.10/- per participant
           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-30" value="toggle"/>
        <label for="section-30"> <span><h3>PRIZE</h3></span></label>
        <div class="content">
          <p>
           Win exciting prizes and amazing goodies!!
         </p>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >TWEET A TALE</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-31" checked="checked"/>
            <label for="section-31"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Are you good at writing fantabulous tales??? Given a theme, can you prove yourself by writing a tweet sized tale? 140 characters is all you have.

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-32" value="toggle"/>
            <label for="section-32"> <span><h3>RULES</h3></span></label>
            <div class="content">
              <p>
               •  Your story must not exceed 140 characters, including all spaces and punctuation.<br>
               • The tale might be based on any theme of your own and can be fictional or non-fictional.<br>
               • The tales will be judged on the basis of their catchiness, grammar and originality.<br>
               • Submit your work to <b>literary.mm2k17@gmail.com</b> well in advance and win prizes.
             </p>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <h1 class="heading w3-text-white w3-center" style="" >LOGO QUIZ</h1><br>
 <div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-33" checked="checked"/>
            <label for="section-33"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> • A team consists of 2 participants.<br>
                  • There will be a closed box in which Slips consisting of iconic brand logos are kept.<br>
                  • The participant is requested to take out a slip from the box and should guess the brand from the logo.<br>
                  • The number of guesses is limited to 3.<br>
                  • The team which comes with the solution within the specified time will be awarded exciting prizes/goodies.<br>
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-34" checked="checked"/>
            <label for="section-34"><span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                <b> Rs.20/- per team or Rs.10/- if registered individually.

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-35" checked="checked"/>
            <label for="section-35"><span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
                <b> Cash prize will be awarded and amazing goodies.

                </b>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <h1 class="heading w3-text-white w3-center" style="" >CAPTION IT</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-36" checked="checked"/>
            <label for="section-36"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> • A team consists of <b>2 participants</b>.<br>
• A series of pictures will be provided to each team or the participant if registered individually.<br>
• They have to caption those pictures using less than ten words per picture within the specified time .<br>
• The team whose captions are the most creative and related to the pictures will be awarded the prize.<br>
• Captions can be written in English/Telugu . Individual Prizes will be given in accordance with the language used.<br>
• <b>Captions will be judged on the basis of their catchiness, grammar and relativity.</b><br>

                </b>
              </p>
            </div>
          </div>
          <div class="section">
          <input type="radio" name="accordion-1" id="section-37" value="toggle"/>
          <label for="section-37"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             Rs.20/- per team or Rs.10/- if registered individually.
           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-38" value="toggle"/>
        <label for="section-38"> <span><h3>PRIZE</h3></span></label>
        <div class="content">
          <p>
           Cash prize will be awarded and amazing goodies.
         </p>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
  <br><br><br><br><br>
  <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
    <div class="row" style="font-family: 'Comfortaa', cursive;">
      <div class="col-md-12">
        <div class="text-center">
          <br>
          <h2><strong>CONTACT</strong></h2>
          <p class="w3-text-black">In case of any queries or clarifications, please feel
            free to contact us literary.mm2k17@gmail.com<br>
          </p>
        </div>
      </div>
    </div>
    <div class="row" style="font-family: 'Comfortaa', cursive;">
      <div class="col-md-4 col-md-offset-2">
        <div class="text-center">
          <h5>PAVAN</h5>
          <h5>+91 94937 60016</h5>
        </div>
      </div>
      <div class="col-md-4">
        <div class="text-center">
          <h5>SRI VIDYA GAYATHRI</h5>
          <h5>+91 85008 53353</h5>
        </div>
      </div>
    </div><br><br><br>
  </div>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/kalakshetra/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/kalakshetra/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/kalakshetra/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>

</html>
