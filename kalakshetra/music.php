   <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>MUSIC | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>

    html, body{
      height: 100%;
    }
    body {
      background-image: url(img/bg/music.jpg) ;
      background-position: center center;
      background-repeat:  no-repeat;
      background-attachment: fixed;
      background-size:  cover;
      background-color: #999;

    }



    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-white w3-center" style="" >MM IDOL(VOCAL - SOLO)</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
              <label for="section-1"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  <b>The unifying force of the Humanity is Music… raise your voice to match with the tune of heavens. Let the stars be your spectators, moon be your emboldener while you mount the heights of Perfection! </b>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>INSTRUCTIONS</h3></span></label>
              <div class="content">
                <p>
                 •  The performances may be based on your own compositions or a classical or light music.<br>
                 • Time slot for each performance: 5 Min (on an average)<br>
                 • Judges Decision is Final and Binding.<br>
                 • You can sing along while playing your instrument like keys or guitar.<br>
                 • <b>Participants are requested to bring their own Karaoke.(if necessary)</b><br>



               </p>
             </div>
           </div>
           <div class="section">
            <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
            <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                RS.100/- per head

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"> <span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
                Cash prize worth 4000/- and amazing goodies!!

              </p>

            </div>
          </div>
        </div>
      </div>
     <h3 class="w3-center w3-text-white">Registration Fee : 100/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="100"/>
         <input type="hidden" name="ename" value="MM IDOL"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='MM IDOL' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                              echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >CLASH OF VOICES(VOCAL - GROUP)</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-5" checked="checked"/>
            <label for="section-5"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Combine voices, weave magic into words and produce the ultimate performance ever. Enthrall the awaiting audience and Guests alike with the mesmerizing flow of the musical fusion…!!</b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
            <label for="section-6"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •  The performances may be based on your own composition or movie songs medley or light music.<br>
               • Time slot for each performance: 5 Min (on an average)<br>
               • 4 min for performance (max)<br>
               • 10 sec for settling on stage.<br>
               • 10 sec for vacating the stage.<br>
               • Participants are requested to bring their own Karaoke.(if necessary)<br>
               • A group can comprise minimum of 2 and maximum of 6 participants.<br>
               • Judges Decision is Final.<br>


             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
          <label for="section-7"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
              Rs.150/- per group.
            </p>

          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
          <label for="section-8"> <span><h3>PRIZES</h3></span></label>
          <div class="content">
            <p>
             Cash prize worth 2000/-and amazing goodies!!
           </p>

         </div>
       </div>
     </div>
   </div>
  <h3 class="w3-center w3-text-white">Registration Fee : 150/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="150"/>
         <input type="hidden" name="ename" value="CLASH OF VOICES"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='CLASH OF VOICES' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >BATTLE OF BANDS</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-51" checked="checked"/>
            <label for="section-51"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                Synchronizing varied musical instruments requires practice which comes with passion. Given the right place to share pure splendor of bands, to reverberate rock hard memoirs and create new versions of music… instruments emerge victorious. Come and be victorious!
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-61" value="toggle"/>
            <label for="section-61"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •  The team may choose to play either western music or classical.<br>
                • All participants are requested to bring their own musical instruments and equipments.<br>
                •  A troop can have 6-10 participants.<br>
                • The performances may be based on their own composition or movie songs.<br>
                • 6 min for performance<br>
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-71" value="toggle"/>
          <label for="section-71"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
              Rs.500/- per group.
            </p>

          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-81" value="toggle"/>
          <label for="section-81"> <span><h3>PRIZES</h3></span></label>
          <div class="content">
            <p>
             Cash prize worth 5000/-
           </p>

         </div>
       </div>
     </div>
   </div>
  <h3 class="w3-center w3-text-white">Registration Fee : 500/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="500"/>
         <input type="hidden" name="ename" value="BATTLE OF BANDS"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='BATTLE OF BANDS' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>


<h1 class="heading w3-text-white w3-center" style="" >LEADING NOTE (INSTRUMENTAL SOLO)</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-9" checked="checked"/>
            <label for="section-9"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>What is nature without trees? What is sky without clouds? What is education without values? Likewise, what is a song without an instrument backing it up? <br>
                  The question here is, do you have one such instrument bringing life to a song? <Br>
                  If so, Mohana Mantra 2k16 is the apt platform, for all your instruments to see the limelight!
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
            <label for="section-10"> <span><h3>RULES AND INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • The performances may be based on their own composition or movie songs or light music.<br>
                • All participants are requested to bring their own musical instruments and equipments.<br>
                • Time slot for each performance: 6 Min (on an average)<br>
                • Judges Decision is Final.<br>

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-11" value="toggle"/>
            <label for="section-11"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Rs.200/- per head.
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
          <label for="section-12"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Cash prize worth 2000/- and amazing goodies.

           </p>

         </div>
       </div>
     </div>
   </div>
  <h3 class="w3-center w3-text-white">Registration Fee : 200/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="200"/>
         <input type="hidden" name="ename" value="LEADING NOTE"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='LEADING NOTE' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >MUSIC QUIZ</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-13" checked="checked"/>
            <label for="section-13"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>How much do you know about Music? Given a tune can you name the song? The right place to bring forth your musical knowledge is right in here… Hum your way through the contest and win exiting prizes!
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-14" value="toggle"/>
            <label for="section-14"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • Prelims (a written test) will be held at first.<br>
                • Finals will be for the top 6 teams selected in prelims.<br>
                • A group can have maximum of 2 participants.<br>
                • Questions will be on Tollywood and Bollywood movies and music related stuff.<br>
                • According to the sub-rounds the time will be allotted to each group. <br>


              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-15" value="toggle"/>
            <label for="section-15"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Rs.50/- per group or Rs.25/- if registered individually.
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-16" value="toggle"/>
          <label for="section-16"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Surprising gifts will be given at the moment.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >GEET GATA CHAL(ANTAKSHARI)</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-17" checked="checked"/>
            <label for="section-17"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  This competition proves that Music sees brain or age ??
                  Right!!!!! Brain
                  This musical Antakshari competition encourages the participants to not only exhibit their musical abilities,but also a test of their memory skills and on the spot thinking.Get ready to give the traditional family game a new twist with Viva.It's time to sharpen your musical IQ !!!

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-18" value="toggle"/>
            <label for="section-18"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • Participants : 3-4 in a group<br>



              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-19" value="toggle"/>
            <label for="section-19"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               • Entry Fee : 50/- per Team
               25/- per Head<br>
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-20" value="toggle"/>
          <label for="section-20"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             •  Surprising Gifts will be given at the moment.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >INDEPENDENT MUSIC(DESI GANA)</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-21" checked="checked"/>
            <label for="section-21"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  Independent Music (Often shortened as indie music or indie) is music produced independently from commercial record labels or their susubsidiaries,a process that may include an autonomous,do-it-yourself approach to recording and Publishing !!!!!<br>
                  In a country like India,there is no scene of Independent music rather than filmy music.<br>
                  So,As a support to inde music,we are here with an event to encourage independent artists !!!!<br>
                  This is the platform where you can get more Exposure and fame!!!!<br>
                  Hurry up and send your Youtube or Sound cloud links !!!!<br>

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-22" value="toggle"/>
            <label for="section-22"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
               •  The song may be based your own composition or the cover of a famous song.<br>
               • There is no time limit for the song.<br>
               • The song lyrics should not be odd or awkward.<br>
               • The selected songs and music will be played in many places of the college during the days of 'MOHANA MANTRA'<br>
               • The song may be video or audio uploaded in youtube.com or soundcloud.com<br>
               • The participation can visit our college and may get a chance to perform their songs live in front of thousands of people.<br>
               • The songs may be of any language i.e, Hindi,Telugu,Tamil,Malayalam,etc<br>
               • Judges decision is final.<br>




             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-23" value="toggle"/>
          <label for="section-23"> <span><h3>REGISTRATION FEE</h3></span></label>
          <div class="content">
            <p>
             Free
           </p>

         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-24" value="toggle"/>
        <label for="section-24"> <span><h3>PRIZE</h3></span></label>
        <div class="content">
          <p>
           Surprising Gifts will be given at the moment.

         </p>

       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >LYRICIST</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-25" checked="checked"/>
            <label for="section-25"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  A lyricist is a person who writes lyrics-words for songs-as opposed to a composer ,who writes the song’s melody .If you are one of those gifted few, who can weave magic into their words and create a catchy composition, Join us @MM2k17 and work your magic !!

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-26" value="toggle"/>
            <label for="section-26"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
               •  The lyrics  may be of any language i.e, Hindi,Telugu,English,Tamil,Malayalam etc.<br>
               • In case of regional languages, the meaning of the lyrics must be drafted in English.<br>
               • Any instances of plagiarism are not encouraged.<br>
               • Submit your compositions ONLINE well in advance and grab the chance to recite it in front of an overwhelming audience. <br>
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>

<h1 class="heading w3-text-white w3-center" style="" >BEAT BOXING</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-27" checked="checked"/>
            <label for="section-27"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> They say that your heart is just a beat box for the song of your life !! We welcome all the beatboxers of the Generation Y ,to join us at the vibrant gala of MM2k17 ,to create history !! Ready ,Steady,Beatbox !!!

                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-28" value="toggle"/>
            <label for="section-28"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •  The performances may be based on your own compositions.<br>
            • Time slot for each performance: 5 Min (on an average)<br>
            • Judges Decision is Final.<br>

             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-29" value="toggle"/>
            <label for="section-29"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               Free
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-30" value="toggle"/>
            <label for="section-30"> <span><h3>PRIZE</h3></span></label>
            <div class="content">
              <p>
               Win exciting prizes and amazing goodies!!
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us mmmusic.2k17@gmail.com<br>
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>HARSHA VARDHAN</h5>
        <h5>+91 86881 86867</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>SIRI</h5>
        <h5>+91 91218 19369</h5>
      </div>
    </div>
  </div><br><br><br>
</div>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/kalakshetra/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/kalakshetra/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/kalakshetra/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>

</html>
