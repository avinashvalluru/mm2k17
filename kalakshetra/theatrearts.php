   <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Theatre Arts| Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>

    html, body{
      height: 100%;
    }
    body {
      background-image: url(img/bg/theatre_arts.jpg) ;
      background-position: center center;
      background-repeat:  no-repeat;
      background-attachment: fixed;
      background-size:  cover;
      background-color: #999;

    }



    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-white w3-center" style="" >SHORT FILMS</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
              <label for="section-1"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  <b>Is directing your field of talent? Been making short films either as a passion or a hobby? No delaying in getting those films out to the public then! Present your films at the vibrant gala of MM 2k17 and win prizes, fame and of course amazing gifts too..!!</b>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>RULES</h3></span></label>
              <div class="content">
                <p>
                 •  Teams must submit their videos for selection purpose.<br>
                 • Video length should not exceed 15 minutes.<br>
                 <b>• Videos should be in .flv format (vlc player).<br>
                  • Short Films are also  accepted through YouTube link, Google Drive. <br>
                  • Short film can be of the following genres: Thriller, Tragedy, Comedy, Rom-Coms, Drama.<br>
                  • The Judges decision is final and binding.<br>


                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
              <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
              <div class="content">
                <p>
                  Rs.250/-

                </p>

              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
              <label for="section-4"> <span><h3>PRIZES</h3></span></label>
              <div class="content">
                <p>
                  Cash prize worth of Rs.10,000/- and amazing goodies.

                </p>

              </div>
            </div>
          </div>
        </div>
        <h3 class="w3-center w3-text-white">Registration Fee : 250/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="250"/>
         <input type="hidden" name="ename" value="SHORT FILMS"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='SHORT FILMS' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
  <h1 class="heading w3-text-white w3-center" style="" >DIALOGUE MAHARAJ</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-5" checked="checked"/>
              <label for="section-5"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  <b>Bored of shooting videos in Dubsmash? Wanna get the feel of some real acting ??  Then here is the chance to prove your talent ,by delivering the famous dialogues uttered by your favourite actors… Come, Share them with us and treasure those Amazing..zing memories...!!!</b>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
              <label for="section-7"> <span><h3>REGISTRATION</h3></span></label>
              <div class="content">
                <p>
                  Free event
                </p>

              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
              <label for="section-8"> <span><h3>PRIZES</h3></span></label>
              <div class="content">
                <p>
                 An Exciting prize will be awarded for the winner.
               </p>

             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <h1 class="heading w3-text-white w3-center" style="" >LIVE PLAY</h1><br>
 <div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-9" checked="checked"/>
            <label for="section-9"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Feel up for some "real acting"? It’s time to give your expression its life... because here is a stage for you to expose your talent, and earn glory and fame!!</b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
            <label for="section-10"> <span><h3>RULES AND INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • Theme of the skits or drama can be message oriented, humour, sentimental etc.<br>
                • Each team should comprise of participants not more than 6 and not less than 2.<br>
                • Time slot for each performance: 14 Min (on an average)<br>
                • 10 min for performance (max)<br>
                • 2 min for settling on stage<br>
                • 1 min grace<br>
                • 1 min for vacating the stage<br>

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-11" value="toggle"/>
            <label for="section-11"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Group: Rs.250/-<br>
               Individual: Rs.100/-<br>

             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
          <label for="section-12"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Win exciting prizes and amazing goodies!

           </p>

         </div>
       </div>
     </div>
   </div>
     <h3 class="w3-center w3-text-white">Registration Fee : 100/-(SOLO)</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="100"/>
         <input type="hidden" name="ename" value="LIVE PLAY SOLO"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='LIVE PLAY SOLO' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                              echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
          <h3 class="w3-center w3-text-white">Registration Fee : 250/-(GROUP)</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="250"/>
         <input type="hidden" name="ename" value="LIVE PLAY GROUP"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from kala WHERE p_id='$_SESSION[p_id]' and event_name='LIVE PLAY GROUP' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                              echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div> <br><br><br>
<h1 class="heading w3-text-white w3-center" style="" >MOVIE QUIZ</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-13" checked="checked"/>
            <label for="section-13"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Do you remember the first movie you watched? Probably No! But we are sure you remember the movie you watched the most number of times. It could be one of those blockbusters or golden hits or trilogies that captivated you. Do you  Keep up with new releases like Spyder, the Epic Bahubali 2 and so on? Well, all movie buffs here awaits the ultimate combat! Bring forth your MOVIE knowledge and win exiting prizes and loads of fun!! <br>
                  Jai Mahishmathi…..!!!!!
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-14" value="toggle"/>
            <label for="section-14"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • Prelims (a written test) will be held at first.<br>
                • Finals will be for the top 6 teams selected in prelims.<br>
                • A group can have maximum of 3 participants.<br>
                • Questions will be on Tollywood and Bollywood .<br>
                • According to the sub-rounds the time will be allotted to each group.<br>

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-15" value="toggle"/>
            <label for="section-15"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Rs.50/- per group or Rs.30/- if registered individually.
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-16" value="toggle"/>
          <label for="section-16"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Win exciting prizes and amazing goodies.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >PATAS</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-17" checked="checked"/>
            <label for="section-17"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  ENTERTAINMENT! ENTERTAINMENT! ENTERTAINMENT!!!! We are back with a Full-length Entertainer that lets you have a good laugh from the bottom of your heart. Are you a true entertainer with good sense of humor? Then here comes the chance to prove it… Grab the opportunity of getting entertained and awarded at the same time...
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-18" value="toggle"/>
            <label for="section-18"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
                • Session will be held for a duration of 2 ½ hours(max).<br>
• Participants will be made to answer  simple and general questions.<br>
• Participants who give spontaneous responses and make the situation hilarious will be awarded prizes.<br>
• No extra qualification is required.<br>


              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-19" value="toggle"/>
            <label for="section-19"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Rs.50/- per Head
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-20" value="toggle"/>
          <label for="section-20"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Amazing gifts will be awarded for every round

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >VIDEO JOCKEY</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-81" checked="checked"/>
            <label for="section-81"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  Do you enjoy talking to people and have a desire to be popular? Do you have your own style of delivering sarcastic one-liners? Do you have a good command over local dialects /Mimicry/Any other energetic traits that would help to carry on the show? Have you ever wanted to be a VJ? Cuz Here’s chance to showcase your talent as a VJ. Participate in our video jockey contest and get a chance to grab amazing prizes. The best performance will be recorded and uploaded in our social platforms.
                </b>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-22" value="toggle"/>
            <label for="section-22"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
                -  One can participate individually  or as a team consisting of 2 participants .<br>
- The VJ can talk about social issues, humorous topics ,films, current affairs, college life,etc.<br>
- The participants can perform either in regional or in English.<br>
- Time slot for each performance: 14 Min (on an average)<br>
- The Judges decision is final and binding.<br>



              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-23" value="toggle"/>
            <label for="section-23"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               Rs.50/-
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-24" value="toggle"/>
          <label for="section-24"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Win exciting prizes and amazing goodies.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <b>theatrearts.mm2k17@gmail.com</b> <br>
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>NAVEEN</h5>
        <h5>+91 88970 04215</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>KHAJA RASOOL</h5>
        <h5>+91 70321 81292</h5>
      </div>
    </div>
  </div><br><br><br>
</div>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/kalakshetra/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/kalakshetra/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/kalakshetra/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>

</html>
