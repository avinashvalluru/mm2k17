<?php
include("../db_config.php");
    include("../functions.php");
     session_start();

  ?>   
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Kalakshetra | Mohana Mantra 2K17</title>
    <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
    <meta name="author" content="Avinash Valluru"/>
    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/nprogress.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/w3.css">
     <link rel="stylesheet" href="../css/tabs.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland" rel="stylesheet">
    
     <script src="../js/jquery.min.js"></script>
     <script type="text/javascript" src="../js/notify.js"></script>
     <script src="../js/nprogress.js"></script>
    <style>
        body {
            background: #212121 url("img/kala.jpg") no-repeat top center fixed;
            background-size: cover;
            margin: 0;
            padding: 0;
           height: 100%;
            width: 100%; 
        }
         
        figure{
            transform: scale(0.8);
        }
        h1{
                          font-size: 5.5em;
                            font-family: Iceland;
                            
                    }
             @media screen and (max-width: 480px) {   
                h1{
                    transform: scale(0.8);
                }
      }           
    </style>
    
</head>
<body>
<script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<?php include("nav.php");?>
             <h1 class="heading w3-text-yellow w3-center" style="padding-top: 48px;" >Kalakshetra</h1>
<div class="container-fluid" >
        <figure class="snip1091 red"><img src="img/ramp.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Ramp  Walk</span></h2>
          </figcaption><a href="rampwalk"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/photo.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Photography Workshop</span></h2>
          </figcaption><a href="photography"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/cc.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Creative Corner</span></h2>
          </figcaption><a href="creativecorner"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/dance.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Dance</span></h2>
          </figcaption><a href="dance"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/music.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Music</span></h2>
          </figcaption><a href="music"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/ta.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Theatre Arts</span></h2>
          </figcaption><a href="theatrearts"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/car.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Carnival</span></h2>
          </figcaption><a href="xtasy"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/li.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Literary</span></h2>
          </figcaption><a href="literary"></a>
        </figure>
        
        <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p>In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="center">
                                <h5>Siri</h5>
                                <h5>+91 97015 43206</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <h5>Afroz</h5>
                                <h5>+91 90109 86487</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>  

         
</div> 

<script>

  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );
</script>
</body>

</html>