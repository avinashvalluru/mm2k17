<?php
include("../db_config.php");
include("../functions.php");
session_start();

?>   
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>CREATIVE CORNER | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>

    html, body{
      height: 100%;
    }
    body { 
      background-image: url(img/bg/creative_corner.jpg) ;
      background-position: center center;
      background-repeat:  no-repeat;
      background-attachment: fixed;
      background-size:  cover;
      background-color: #999;

    }



    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {   
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }   
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>

</head>
<body>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br> 
  <h1 class="heading w3-text-white w3-center" style="" >ART ATTACK</h1><br>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        <div class="left-menu">
          <div class="accordion">
            <div class="section">
              <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
              <label for="section-1"><span><h3>DESCRIPTION</h3></span></label>
              <div class="content">
                <p>
                  <b>Dream your painting and paint the dream. Come on spread the paint with your hands.</b>     
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
              <label for="section-2"> <span><h3>INSTRUCTIONS</h3></span></label>
              <div class="content">
                <p> 
                 • Painting is done only with hands, usage of paint brush is not allowed.<br>
•  Painting is done on OHP sheet of A4 size.<br>
•  Color paints and OHP sheet will be provided.<br>
•  Time limit is 1 hour.<br>
•  Paintings will be judged based on their artistic finesse,uniqueness and inherent meaning.<br>

               </p>
             </div>
           </div>
           <div class="section">
            <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
            <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                RS.50/- per head.

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"> <span><h3>PRIZES</h3></span></label>
            <div class="content">
              <p>
                Bag fabulous prizes and amazing goodies !!

              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >ART GALLERY</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-5" checked="checked"/>
            <label for="section-5"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Let’s rock in an art gallery. I mean you deserved to be pinned up against a wall. Exhibit your creativity, you are the masterpiece. </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
            <label for="section-6"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p> 
               Contestant can exhibit any kind of drawing or craft.
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
          <label for="section-7"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
              RS.50/- per unit
            </p>

          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
          <label for="section-8"> <span><h3>PRIZES</h3></span></label>
          <div class="content">
            <p>
             Attractive prizes will be awarded.
           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >50-50</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-9" checked="checked"/>
            <label for="section-9"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Have you ever shared your work? This time we will finish half of the work for you. Yes, we give a half and the completion of the rest is your task. 
                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
            <label for="section-10"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                • •  Half the image is given by us. You have to draw the rest.<br>
• •   Stationary will be provided by us.<br>
• •   Time limit is 15 min.<br>

              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-11" value="toggle"/>
            <label for="section-11"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               RS.40/-
             </p>

           </div>
         </div> 
         <div class="section">
          <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
          <label for="section-12"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Bag fabulous prizes and exciting goodies!!

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >BEST MAKER OF WASTE</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-13" checked="checked"/>
            <label for="section-13"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>Reduce, Reuse and Recycle is the most efficient way of protecting our Mother Earth! If you believe that... come up with innovative ideas to create zero waste earth!
                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-14" value="toggle"/>
            <label for="section-14"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                •   Miscellaneous materials will be provided.
•   Using the material provided, you should build something creative.
•  Time limit will be announced at the venue.
•  Team can comprise of two members. 
              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-15" value="toggle"/>
            <label for="section-15"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               RS.50/- per team
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-16" value="toggle"/>
          <label for="section-16"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Attractive prizes will be awarded.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >SOAP CARVING</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-17" checked="checked"/>
            <label for="section-17"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b>  When carving, a sculptor removes everything that is not the statue. In the same way the art of revealing beauty lies in removing what conceals it.   

                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-18" value="toggle"/>
            <label for="section-18"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
                •  Soap and cutter will be provided.<br>
• Time limit is 30 min.<br>


              </p>

            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-19" value="toggle"/>
            <label for="section-19"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               RS.40/-<br>
             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-20" value="toggle"/>
          <label for="section-20"> <span><h3>PRIZE</h3></span></label>
          <div class="content">
            <p>
             Attractive prizes will be awarded.

           </p>

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >THE JIGSAW PUZZLE</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-21" checked="checked"/>
            <label for="section-21"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Life is like a jigsaw puzzle, you have to see the whole picture then put it together piece by piece!<br><b>-Terry McMillan</b>

                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-22" value="toggle"/>
            <label for="section-22"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
               •Team can comprise of two members<br>
•Team which solves more puzzles in the given time will be winner.<br>

             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-23" value="toggle"/>
          <label for="section-23"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             RS.40/-
           </p>

         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-24" value="toggle"/>
        <label for="section-24"> <span><h3>PRIZE</h3></span></label>
        <div class="content">
          <p>
           Attractive prizes will be awarded.

         </p>

       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >JENGA</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-25" checked="checked"/>
            <label for="section-25"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Stack the blocks by creating a taller and increasingly stable structure as the game progresses.
                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-26" value="toggle"/>
            <label for="section-26"> <span><h3>GUIDELINES</h3></span></label>
            <div class="content">
              <p>
               •Take one block on a turn from any level of the tower (except the one below an incomplete top level),place it on the topmost level in order to complete it.<br>
•The game ends when the tower falls or if any block falls from the tower (other than the block a player moves on a turn).<br>
•Challenge your opponent by your highest-level.  <br>
•The one who stands on the top of leader board will be the winner.<br>

             </p>
           </div>
         </div>
   <div class="section">
            <input type="radio" name="accordion-1" id="section-27" value="toggle"/>
            <label for="section-27"> <span><h3>REGISTRATION FEE</h3></span></label>
            <div class="content">
              <p>
               RS 30/-
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-28" value="toggle"/>
            <label for="section-28"> <span><h3>PRIZE</h3></span></label>
            <div class="content">
              <p>
               Attractive prizes will be awarded
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >SAND STORIES</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-29" checked="checked"/>
            <label for="section-29"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> In every grain of sand, there is an untold story of the earth. Come, Say your story, using nothing but some Sand with a dash of colors!!!
                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-30" value="toggle"/>
            <label for="section-30"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               •Colored sand and glue will be provided to all the participants.<br>
•Participants have to design a creative motif based on the given theme.<br>
•Time limit would be 30 minutes.<br>

             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-31" value="toggle"/>
            <label for="section-31"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               50/- per participant
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-32" value="toggle"/>
            <label for="section-32"> <span><h3>PRIZE</h3></span></label>
            <div class="content">
              <p>
               Win exciting prizes and amazing goodies!!!
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >PENCIL SKETCH</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-33" checked="checked"/>
            <label for="section-33"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Do you have a flair for sketching? Can you bring pictures to life with nothing but a pencil in your hand? Then, come join us in the spectacular gala of MM2k17 and bring out the artist in you!!<br>
A pencil and a dream can take you anywhere!!!<br>

                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-34" value="toggle"/>
            <label for="section-34"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               • A4 size sheet and pencils of desired specifications will be provided.<br>
• Participants can sketch on any theme or motifs.<br>
• Participants should finish off sketching within 40 minutes.<br>
• Sketches will be judged based on their artistic finesse, uniqueness and inherent meaning.<br>
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-35" value="toggle"/>
            <label for="section-35"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               50/- per participant
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-36" value="toggle"/>
            <label for="section-36"> <span><h3>PRIZE</h3></span></label>
            <div class="content">
              <p>
               Win exciting prizes and amazing  goodies!!
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>
<h1 class="heading w3-text-white w3-center" style="" >GO FOR GOLD!!!</h1><br>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-37" checked="checked"/>
            <label for="section-37"><span><h3>DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
                <b> Are you an expert in the art of persuasion? Do your friends often come up to you, to convince their parents about letting them go out? Then here’s an event specifically designed to bring out this special ability in you-The art of persuasion!!

                </b>     
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-38" value="toggle"/>
            <label for="section-38"> <span><h3>INSTRUCTIONS</h3></span></label>
            <div class="content">
              <p>
               • Each participant/team will be allotted a product, through lucky-dip. <br>
•Within the specified time, they will have to come up with an advertising and a tagline for the product.<br>
• After the completion of the prescribed time, they’ll have to enact a role play, in the format of an advertisement.<br>
• Participants will be judged on the basis of creativity and persuasive skills.<br>

             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-39" value="toggle"/>
            <label for="section-39"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               20/- person
             </p>
           </div>
         </div>
         <div class="section">
            <input type="radio" name="accordion-1" id="section-40" value="toggle"/>
            <label for="section-40"> <span><h3>PRIZE</h3></span></label>
            <div class="content">
              <p>
               Win exciting prizes and goodies!!
             </p>
           </div>
         </div>
   </div>
 </div>
</div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us creativecorner.mm2k17@gmail.com<br>
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>GURU SRINADH</h5>
        <h5>+91 97006 93769</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>SIRI</h5>
        <h5>+91 91218 19369</h5>
      </div>
    </div>
  </div><br><br><br>
</div> 

</body>

</html>