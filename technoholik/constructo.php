<?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Constructo | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>
    body {
      background: #212121 url("img/bg/constructo.jpg") no-repeat top center fixed;
      background-size: cover;
      margin: 0;
      padding: 0;
      height: 100%;
      width: 100%;
    }


    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-pink w3-center" style="" >MASTER BUILDERS</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                “The best creator next to god is civil engineer” they say. It’s not a easy quest to build things out of scratch. It takes a huge proportion of determination and perception to build things.Get your determination tested in “Master Builders” and prove that you can be the next god. Get registered now and win a exiting prizes." <br>
                <b>REGISTRATION FEE</b>: 300/-
                <br> <b>Dates</b>: Sept 25th - 26th
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
               ●  A Team contains maximum of 3 participants(irrespective of college).<br>
               ● Spot  registrations are available.<br>
               ● online banking also available, to register visit our website mohana mantra 2k17.<br>
               ● The soft copy of your online banking receipt have to mail to constructo2k17@gmail.com or bring it at the event.<br>
               ● details of all team members with full names, MM Id's, contact numbers,course name, college name and city should be send along with the payment receipt.<br>
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>RULES AND REGULATIONS</h3></span></label>
          <div class="content">
            <p>
              ● All Participants must bring their MM Id card on the day of event.<br>
              ● wooden sticks , glues, rubber bands, pins are provided to make bridge, you can cut and bent and shape in any way you want.<br>
              ● You may not use more than 175 sticks and also not more than 2 glues.<br>
              ● The length of the bridge will be given at the time of event.<br>
              ● Use of other materials will lead to disqualification.<br>
              ● Team may have a time limit of 3 hours after that it will be tested.<br>
              ● The structure which bears more loads without any breakage of sticks will be declared as winner.<br>
              ● The organising committee reserves the right to change any or all the above rules as they deem fit.Change in rules if any will be highlighted on the website.<br>
              ● In case of tie, Design of the structure will be taken to consideration.<br>
              ● Decision of judges is final.<br>
            </p>
          </div>
        </div>
      </div>
    </div>
  <h3 class="w3-center w3-text-white">Registration Fee : 300/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="300"/>
         <input type="hidden" name="ename" value="MASTER BUILDERS"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='MASTER BUILDERS' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                              echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
  <h1 class="heading w3-text-pink w3-center" style="" >CAD ARCHITECT</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                CAD is the Application of computer systems to aid in the creation, Modification, Analysis of a design.
                Civil Engineers need to have a plan for construction and it need computer skills for those who are weak in engineering drawing.<br>
                Here, we have a platform to show your computer skills in MOHANA MANTRA 2K17.<br>


                <b>REGISTRATION FEE</b>: 150/-
                <br> <b>Dates</b>: Sept 25th - 26th <br>
                <br>PRIZE MONEY : </b><br>
                I prize : ₹ 750/- <br>
                II Prize : ₹  500/-
                <br>Winners from tirupati can avail free training of 3D MODELLIGNG software worth Rs.8000/- along with cash Prize at CANTER CADD
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-5" value="toggle"/>
            <label for="section-5"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                •  Registration fee per each participate is Rs.150.<br>
                • Interested candidates can register for the event by paying above mentioned amount through online banking.<br>
                • The soft copy of the payment receipt to be mailed to constructo2k17@gmail.com<br>
                • Details of the candidates like full names and MM Id’s along with their contact numbers, course name(group),college name and city need to be sent along with payment receipt.<br>

             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
          <label for="section-6"> <span><h3>RULES AND REGULATIONS</h3></span></label>
          <div class="content">
            <p>
              • The participants will be provided with hard copies of 2D layout related to a plan for construction.<br>
              • The participants need to use the given 2D Layout and design the Layout using commands in AUTOCAD Software.<br>
              • Any kind of malpractice during the event will lead to disqualification of the participant immediately.<br>
              • The organising committee reserves the right to change any or all the above rules as they deem fit. <br>

            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
          <label for="section-7"> <span><h3>JUDGEMENT CRITERIA</h3></span></label>
          <div class="content">
            <p>
              • Judgement is made based on the evaluation of Layout in CAD Software which is completed within the stimulated time.<br>
            • If multiple participants complete the task in time without any errors then time taken by the participant for completion is taken into consideration.’

            </p>
          </div>
        </div>
      </div>
    </div>
<h3 class="w3-center w3-text-white">Registration Fee : 150/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="150"/>
         <input type="hidden" name="ename" value="CAD ARCHITECT"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='CAD ARCHITECT' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                              echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;constructo2k17@gmail.com
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>Pavani Singam</h5>
        <h5>7075170915</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>VAMSI UV</h5>
        <h5>8341731765</h5>
      </div>
    </div>
  </div><br><br><br>
</div>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/technoholik/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/technoholik/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/technoholik/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>
</html>
