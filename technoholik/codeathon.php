   <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Code-a-Thon | Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        body {
          background: #212121 url("img/bg/code.jpg") no-repeat top center fixed;
          background-size: cover;
          margin: 0;
          padding: 0;
          height: 100%;
          width: 100%;
        }


        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {
          h1{
            transform: scale(0.7);

          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br>
      <h1 class="heading w3-text-pink w3-center" style="" >CODING</h1><br>
      <div class="container">
       <div class="row">
        <div class="col-md-10 col-md-offset-1">

          <div class="left-menu">
            <div class="accordion">
              <div class="section">
                <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
                <label for="section-1"><span><h3>ABOUT</h3></span></label>
                <div class="content">
                  <p>
                    For all the code golfers out there! “Coding” in collaboration with CODECHEF is rendering an opportunity where you can show your programming skills. So gear up to do some coding!!! Registrations are now open..<br>
                    <b>REGISTRATION FEE</b>:Rs.200/- per team<br>
                    <b>DATE</b>: September 25th to 26th<br>
                    <b>TIMINGS</b>: 11:30AM-1:00PM <br>
                    <b>PRIZE MONEY</b>:2000/- <br>
                    <b>Certification and 250 laddus(points) from CODECHEF for winners</b><br>
                  </p>
                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
                <label for="section-2"> <span><h3> REGISTRATIONS</h3></span></label>
                <div class="content">
                  <p>
                   ●Team of maximum <b>2</b> are allowed.<br>
                   ●Team formed by students of different institutions are also allowed.<br>
                   ●Registration fee for each team is 200/- <br>
                   ●Interested candidates can register for the event by paying above mentioned amount through online banking.<br>
                   ●All the participants must have CODECHEF ID.If you don't have a CodeChef ID create one<a href="https://www.codechef.com/signup" target="_blank"><span style="color:red"> here(Mandatory)</span></a>..Make sure that you remember username and password on the day of event.<br>
                   ●The soft copy of the payment receipt is to be mailed to codeathon.technoholik2k17@gmail.com<br>
                   ●Details of all the team members like full names and registration id's along with their contact numbers,course name(group),college name and city need to be sent along with payment receipt.<br>
                   ●On  the spot registrations are available( Make sure that you have a CodeChef ID).<br>

                 </p>

               </div>
             </div>
             <div class="section">
              <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
              <label for="section-3"> <span><h3>RULES AND REGULATIONS</h3></span></label>
              <div class="content">
                <p>●  All the participants must report to the respective labs by 10:30AM.<br>
                  ●  Participants may use the programming language they prefer.<br>
                  ● Time limit of the event is 90 minutes.<br>
                  ● coding for a question is considered finished only after the program is complied and executed correctly with no error.<br>
                  ● Output must be in the format specified. And should satisfy all the test cases.<br>
                  ● Any participant found to be indulging in any form of malpractice will be immediately disqualified.<br>
                  ● The organising committee reserves all the rights to change any of the above rules as they deem fit.Any further modification in the guidelines will be conveyed accordingly and highlighted on the website.<br>

                </p>

              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
              <label for="section-4"> <span><h3>JUDGEMENT CRITERIA</h3></span></label>
              <div class="content">
                <p>
                  ●  The team which executes maximum number of correct codes within the given time wins the event.<br>
                  ●  Incase where solo winner could not be determined the team which completes the task efficiently in minimum possible time will be declared as a winner.<br>
                </p>
              </div>
            </div>
            <div class="section">
              <input type="radio" name="accordion-1" id="section-5" value="toggle"/>
              <label for="section-5"> <span><h3>PRICING</h3></span></label>
              <div class="content">
                <p>
                  ● Winners will be awarded cash prizes worth 2000/-<br>
                  ● Certification and 250 laddus(points)  from CODECHEF for winners.<br>
                  ● Laptop stickers for all the participants.<br>
                  ● Certificate of participation from Codechef will be awarded to participants.<br>
                  ● Prizes will be awarded on second day i.e.sept 26th<br>
                </p>
              </div>
            </div>

            <div class="section">
              <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
              <label for="section-6"> <span><h3>FAQ</h3></span></label>
              <div class="content">
                <p>
                 <b>1.What programming language do we use?<br>
                  A.CODECHEF supports over 50 programming languages.Participants can write the code in their preferable language<br>
                  2.What is a CodeChef laddu?<br>
                  A.Laddu is the codechef currency.<br>
                  3.What can I do with codechef laddus?<br>
                  A.Once you get enough number of laddus you can redeem them to get yourself anything from a wide range of codechef goodies.The goodies are listed <a href="https://goodies.codechef.com/goodies/" target="_blank"><span style="color:red"> here</span></a><br>
                  4.Do we have to bring any particulars on the day of event?<br>
                  A.Participants will be provided a file that includes pen and book.
                  Participants who made payment online are advised to bring a hard copy or soft copy of the payment receipt<br>

                  <u>Example problem</u>
                  Print  string of odd length in X format <br>
                  Input:12345 <br>
                  Output:<br>
                  <pre>
 1                     5
     2            4
            3
      2           4
 1                      5


                  </pre>

                 </b>
               </p>
             </div>
           </div>
         </div>
       </div>
    <h3 class="w3-center w3-text-white">Registration Fee : 200/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="200"/>
         <input type="hidden" name="ename" value="CODING"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='CODING' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
 <h1 class="heading w3-text-pink w3-center" style="" >DEBUGGING</h1><br>
 <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">

      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
            <label for="section-7"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                Are you efficient at overhauling errors in a program? Then “Debugging” is a wondrous  platform to showcase all your debugging skills. Eliminate the errors and clear your way to total execution of the code.<br>
                <b>REGISTRATION FEE</b>:Rs.200/- per team<br>
                <b>DATE</b>: September 26th<br>
                <b>TIMINGS</b>: 01:30PM-03:00PM <br>
                <b>PRIZE MONEY</b>:1000/- <br>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
            <label for="section-8"> <span><h3> REGISTRATIONS</h3></span></label>
            <div class="content">
              <p>
               ●Team of maximum <b>2</b> are allowed.<br>
               ●Team formed by students of different institutions are also allowed.<br>
               ●Registration fee for each team is 200/- <br>
               ●Interested candidates can register for the event by paying above mentioned amount through online banking.<br>
               ●The soft copy of the payment receipt is to be mailed to codeathon.technoholik2k17@gmail.com<br>
               ●Details of all the team members like full names and registration id's along with their contact numbers,course name(group),college name and city need to be sent along with payment receipt.<br>
               ●Spot registrations are available.<br>

             </p>

           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-9" value="toggle"/>
          <label for="section-9"> <span><h3>RULES AND REGULATIONS</h3></span></label>
          <div class="content">
            <p>• Event has only one round.<br>
              • The participants are required to debug a given program and answer the multiple choice questions given to them within a stipulated time. <br>
              •Time limit for the event is 90 min.<br>
              •The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website.<br>

            </p>

          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
          <label for="section-10"> <span><h3>PRICING</h3></span></label>
          <div class="content">
            <p>
              ● Winners will be awarded cash prizes worth 1000/- <br>
              ● Certificates will be provided for winners and participants.<br>
            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-11" value="toggle"/>
          <label for="section-11"> <span><h3>FAQ</h3></span></label>
          <div class="content">
          <p>
           <b>&emsp;1.What programming language do we use?<br>
            &emsp;A. Only c<br>
            &emsp;2.Do we have to bring any particulars on the day of event?<br>
            &emsp;A.Participants will be provided a file that includes pen and book.
            &emsp;Participants who made payment online are advised to bring a hard copy or soft copy of the payment receipt<br>
            <br><br>

            &emsp;<u>EXAMPLE PROBLEM</u>:<br>
            &emsp;What will be the output of the program ?<br>
            &emsp;Assume that the size of int is 4<br>
            &emsp;#include<stdio.h><br>
            &emsp;Void  f(char**);<br>
            &emsp;int main()<br>
            &emsp;{<br>
            &emsp;char *c[] = {"ab", "cd", "ef", "gh",”ij”,”kl”};<br>
            &emsp;f(argv);<br>
            &emsp;return 0;<br>
            &emsp;}<br>
            &emsp;Void f(char  **p)<br>
            &emsp;{<br>
            &emsp;Char *t;<br>
            &emsp;t=(p +=sizeof(int))[-1];<br>
            &emsp;printf(“%s\n”,t);<br>
            &emsp;}<br><br>

            &emsp;[A].  ab <br>
            &emsp;[B].  cd <br>
            &emsp;[C].  ef <br>
            &emsp;[D].  gh <br>


          </b>
        </p>
      </div>
    </div>
  </div>
</div>
<h3 class="w3-center w3-text-white">Registration Fee : 200/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="200"/>
         <input type="hidden" name="ename" value="DEBUGGING"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='DEBUGGING' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<h1 class="heading w3-text-pink w3-center" style="" >REVERSE CODING</h1><br>
<div class="container">
 <div class="row">
  <div class="col-md-10 col-md-offset-1">

    <div class="left-menu">
      <div class="accordion">
        <div class="section">
          <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
          <label for="section-12"><span><h3>ABOUT</h3></span></label>
          <div class="content">
            <p>
              Do you feel that your left part of your brain is more active? If yes, then what are you waiting for? You must be great at logical thinking and reasoning. “Revere Coding” collaboration with CODECHEF is rendering an amazing opportunity for you to test your programming skills.<br>
              <b>REGISTRATION FEE</b>:It is a free event.<br>
              <b>DATE</b>: September 25th<br>
              <b>TIMINGS</b>: 2:30PM-4:00PM <br>
              <b>Certification and 250 laddus(points) from CODECHEF for winners</b><br>
            </p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-13" value="toggle"/>
          <label for="section-13"> <span><h3> REGISTRATIONS</h3></span></label>
          <div class="content">
            <p>
             ●   It's a free event there is no registration fee.
             &nbsp;Team size is maximum of 2 members<br>
             ● Interested candidates can register for the event <br>
             ● The soft copy of the receipt is to be mailed to codeathon.technoholik2k17@gmail.com <br>
             ●   All the participants must have CODECHEF ID.If you don't have a CodeChef ID create one<a href="https://www.codechef.com/signup" target="_blank"><span style="color:red"> here(Mandatory)</span></a>.Make sure that you remember username and password on the day of event.<br>
             ● Details of all the team members like full names and MM id’s along with their Contact Numbers, Course Name (group), College Name and City need to be sent along with the receipt.<br>
             ●  On spot registrations available.(Make sure that you have CodeChef ID)<br>

           </p>

         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-14" value="toggle"/>
        <label for="section-14"> <span><h3>RULES AND REGULATIONS</h3></span></label>
        <div class="content">
          <p>●  All the participants must report to the respective labs by 1:30 PM.<br>
           ●    The participants are provided with a problem from which they need to find out a logic.<br>
           ● The participants are required to derive a source code from given logic such that code when executed gives the same logic.<br>
           ● Based on that logic the participants are asked to derive a source code for it.<br>
           ● The participants need to show the logic in the question as the output of the source code.<br>
           ● The time limit for the event is 80 minutes.<br>
           ● Any participant found to be indulging in any form of malpractice will be immediately disqualified.<br>
           ●  The organising committee reserves all the rights to change any of the above rules as they deem fit.Any further modification in the guidelines will be conveyed accordingly and highlighted on the website.<br>


         </p>

       </div>
     </div>
     <div class="section">
      <input type="radio" name="accordion-1" id="section-15" value="toggle"/>
      <label for="section-15"> <span><h3>JUDGEMENT CRITERIA</h3></span></label>
      <div class="content">
        <p>
          ● The participants who solve all the most questions in the stipulated time are declared winners. <br>
          ● In case a sole winner cannot be determines the time taken by the participants to solve the questions is taken into consideration.<br>
          <br>
        </p>
      </div>
    </div>
    <div class="section">
      <input type="radio" name="accordion-1" id="section-16" value="toggle"/>
      <label for="section-16"> <span><h3>PRICING</h3></span></label>
      <div class="content">
        <p>
         ●  Certification and 250 laddus(points)  from CODECHEF for winners.<br>
         ●   Laptop stickers for all the participants.<br>
         ● Participation certificate from Codechef will be awarded to all the participants.<br>

       </p>
     </div>
   </div>

   <div class="section">
    <input type="radio" name="accordion-1" id="section-17" value="toggle"/>
    <label for="section-17"> <span><h3>FAQ</h3></span></label>
    <div class="content">
      <p>
        <b>
         1.What programming language do we use?<br>
         A.Code chef supports over 50 programming languages.Participants can write the code in their preferable language.<br>
         2.What is a CodeChef laddu?<br>
         A.Laddu is the codechef currency.<br>
         3.What can I do with codechef laddus?<br>
         A.Once you get enough number of laddus you can redeem them to get yourself anything from a wide range of codechef goodies.The goodies are listed <a href="https://goodies.codechef.com/goodies/" target="_blank"><span style="color:red"> here</span></a><br>
         4.Do we have to bring any particulars on the day of event?<br>
         A Not necessary.Participants will be provided a pen and paper.<br>

         Example<br>
         Find out the logic and write the source code for the series<br>
         2,6,18,54... and also display the 8 th term<br>
         Solution:n+1 th term= 3*n-1<br>
         Write the source code and display the output<br>
       </b>
     </p>
   </div>
 </div>
</div>
</div>
</div>
</div>
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&nbsp;&nbsp;&nbsp;&nbsp;codeathon.technoholik2k17@gmail.com
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>JYOTHSNA L</h5>
        <h5>7981284337</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>VAMSI UV</h5>
        <h5>8341731765</h5>
      </div>
    </div>
  </div><br><br><br>
</div>
<form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/technoholik/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/technoholik/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/technoholik/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>

</html>
