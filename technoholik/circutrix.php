   <?php
    include("../db_config.php");
    include("../functions.php");
    session_start();

?>
<?php

    if(isset($_POST['reg'])){

       if(isset($_SESSION['p_id']))
       {
       $MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
             $hash_string = '';
          // Merchant Salt as provided by Payu
          $SALT = "H5xwGsQ6"; //Please change this value with live salt for production

          // End point - change to https://secure.payu.in for LIVE mode
          $PAYU_BASE_URL = "https://secure.payu.in";
          $id=$_SESSION['p_id'];
          $sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
          $result = $con->query($sql);
          $row=$result->fetch_assoc();
          $fname=$row['p_id'];
          $email=$row['email'];
          $mbl=$row['mobile'];

          $action = '';
            $amt=$_POST['amount'];
            $amt= $amt+($amt*0.03);
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $posted['txnid']=$txnid;
            $posted['amount']=$amt;
            $posted['firstname']=$fname;
            $posted['email']=$email;
            $posted['phone']=$mbl;
            $posted['productinfo']=$_POST['ename'];;
            $posted['key']=$MERCHANT_KEY;

          $hash = '';
          // Hash Sequence
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                    empty($posted['key'])
                    || empty($posted['txnid'])
                    || empty($posted['amount'])
                    || empty($posted['firstname'])
                    || empty($posted['email'])
                    || empty($posted['phone'])
                    || empty($posted['productinfo'])

            ) {
              $formError = 1;
            } else {

            $hashVarsSeq = explode('|', $hashSequence);

            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                $hash_string .= '|';
              }

              $hash_string .= $SALT;



              $hash = strtolower(hash('sha512', $hash_string));
              $action = $PAYU_BASE_URL . '/_payment';
            }
          } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
          }

        }
        else
        {
          echo '<script>alert("Please login First");</script>';
        }
        $con->close();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Circutrix | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>
    body {
      background: #212121 url("img/bg/circuitrix.jpg") no-repeat top center fixed;
      background-size: cover;
      margin: 0;
      padding: 0;
      height: 100%;
      width: 100%;
    }


    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
 <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
   <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-pink w3-center" style="" >CIRCUIT DESIGN</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                Are you a Circuit Designer???
                Do you have the talent to play with circuits and to bring out the best of its components?
                Do you think you can make life better with your designs?
                If the answers are all “yes!”, then come on! All you have to do is to brush up your concepts and get ready to unleash your designing skills in the ‘Circuit Design’ battlefield at Mohanamantra2k17!<br>
                <b>EVENT ENTRY FEE</b>: RS.250/- Per Team<br>
                <b>DATES</b>: SEP 25TH AND SEP 26TH <br>
                <b>TIME OF THE EVENT</b>: 10:00 AM TO 01:00 PM<br>
                <b>PRIZE MONEY</b> :1000/-<br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3> DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
               •  In this event the abstracts of the circuits designed by the participants must be sent through mail. The mail must also include the components used in the circuit.<br>
               • The selected abstracts will be intimidated via mail.<br>
               • The select teams will be called upon to construct and demonstrate their circuit on the day of event.<br>
               • The duration of the event is 3 hours.<br>

             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             •  The team can register at www.mohanmantra.com<br>
             • The abstracts should be mailed to circuitrixmm2k17@gmail.com or technoholik.mm2k17@gmail.com<br>
             • Maximum of 3 members per team.<br>
             • Teams formed by students of different institutions are also allowed.<br>
             • Registration fee per each team is Rs.250.<br>
             • Interested candidates can register for the event by paying above mentioned amount through Online Banking.<br>
             • The soft copy of the payment receipt is to be mailed to circuitrixmm2k17@gmail.com<br>
             • Details of all the team members like full names and Registration id’s along with their Contact Numbers, Course Name (group), College Name and City need to be sent along with the abstracts.<br>


           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
        <label for="section-4"> <span><h3> JUDGMENT CRITERIA</h3></span></label>
        <div class="content">
          <p>
            • Evaluation will be based on time performance, efficiency of the circuit and demonstration.<br>
            • The decision of the judges is final and irrevocable.<br>

          </p>
        </div>
      </div>
      <div class="section">
        <input type="radio" name="accordion-1" id="section-5" value="toggle"/>
        <label for="section-5"> <span><h3> RULES AND REGULATIONS</h3></span></label>
        <div class="content">
          <p>
            • Any kind of malpractice during the event will lead to disqualification of the team immediately.<br>
            • The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website. <br>


          </p>
        </div>
      </div>
    </div>
  </div>
<h3 class="w3-center w3-text-white">Registration Fee : 250/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="250"/>
         <input type="hidden" name="ename" value="CIRCUIT DESIGN"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='CIRCUIT DESIGN' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
 <h1 class="heading w3-text-pink w3-center" style="" >CIRCUIT HUNT</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" checked="checked"/>
            <label for="section-6"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
               Hunt the circuit to emerge victorious!!!
If you think you are logical, then solve it. It has simple puzzles and logics as clues to test your innovative E-knowledge and capability to identify the specifications of any electronic device. Glue the clues to complete the incomplete circuit and electrify it!!
<br>
                <b>EVENT ENTRY FEE</b>: RS.200/-<br>
                <b>DATES</b>: SEP 25TH AND SEP 26TH <br>
                <b>TIME OF THE EVENT</b>: 02:00 PM TO 04:00 PM<br>
                <b>PRIZE MONEY</b> :700/-<br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
            <label for="section-7"> <span><h3> DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
              • This event involves finding out the missing components of the given circuit and placing them in their respective positions in the circuit within the specified time.<br>
• To find out the missing components, the participants will be given series of clues in the form of technical and logical questions.<br>
• The answers to those questions guide the participant to find out the missing components of the circuit. <br>
• The duration of event is 2 hours.<br>
<br>

             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
          <label for="section-8"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             •  The team can register at www.mohanmantra.com<br>
             • The abstracts should be mailed to circuitrixmm2k17@gmail.com or technoholik.mm2k17@gmail.com<br>
             • Maximum of 2 members per team.<br>
             • Teams formed by students of different institutions are also allowed.<br>
             • Registration fee per each team is Rs.200.<br>
             • Interested candidates can register for the event by paying above mentioned amount through Online Banking.<br>
             • The soft copy of the payment receipt is to be mailed to circuitrixmm2k17@gmail.com<br>
             • Details of all the team members like full names and Registration id’s along with their Contact Numbers, Course Name (group), College Name and City need to be sent along with the abstracts.<br>


           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-9" value="toggle"/>
        <label for="section-9"> <span><h3> JUDGMENT CRITERIA</h3></span></label>
        <div class="content">
          <p>
          • The team which answers the highest number of aptitude questions correctly and complete the circuit without errors in minimum possible time will be declared as the winning team.<br>
• The decision of the judges is final and irrevocable.<br>


          </p>
        </div>
      </div>
      <div class="section">
        <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
        <label for="section-10"> <span><h3> RULES AND REGULATIONS</h3></span></label>
        <div class="content">
          <p>
            • Any kind of malpractice during the event will lead to disqualification of the team immediately. <br>
• No electronic gadgets like phone, calculator are allowed.<br>
• The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website.<br>


          </p>
        </div>
      </div>
    </div>
  </div>
<h3 class="w3-center w3-text-white">Registration Fee : 200/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="200"/>
         <input type="hidden" name="ename" value="CIRCUIT HUNT"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='CIRCUIT HUNT' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<br><br>
<h1 class="heading w3-text-pink w3-center" style="" >ENGINEER VS CIRCUIT</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-11" checked="checked"/>
            <label for="section-11"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
                Are you efficient at finding errors? Then this is a wonderful platform for you to enhance and assess your error finding skills. Eliminate the errors and clear the way to juice up the circuit.<br>
                <b>EVENT ENTRY FEE</b>: RS.100/-<br>
                <b>DATES</b>: SEP 25TH AND SEP 26TH <br>
                <b>TIME OF THE EVENT</b>: 11:00 AM TO 12:00 PM<br>
                <b>PRIZE MONEY</b> :500/-<br>

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
            <label for="section-12"> <span><h3> DESCRIPTION</h3></span></label>
            <div class="content">
              <p>
              The Event involves finding the mistakes in the given circuit within a stipulated time of 1 hour and rectify them.

             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-13" value="toggle"/>
          <label for="section-13"> <span><h3>REGISTRATION</h3></span></label>
          <div class="content">
            <p>
             •  The team can register at www.mohanmantra.com<br>
             • The abstracts should be mailed to circuitrixmm2k17@gmail.com or technoholik.mm2k17@gmail.com<br>
             • Maximum of 1 members per team.<br>
             • Teams formed by students of different institutions are also allowed.<br>
             • Registration fee per each team is Rs.100.<br>
             • Interested candidates can register for the event by paying above mentioned amount through Online Banking.<br>
             • The soft copy of the payment receipt is to be mailed to circuitrixmm2k17@gmail.com<br>
             • Details of all the team members like full names and Registration id’s along with their Contact Numbers, Course Name (group), College Name and City need to be sent along with the abstracts.<br>


           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-14" value="toggle"/>
        <label for="section-14"> <span><h3> JUDGMENT CRITERIA</h3></span></label>
        <div class="content">
          <p>
            • The team which is first to complete without errors, the given circuit will be declared as the winning team.<br>
• The decision of the judges is final and irrevocable.
<br>

          </p>
        </div>
      </div>
      <div class="section">
        <input type="radio" name="accordion-1" id="section-15" value="toggle"/>
        <label for="section-15"> <span><h3> RULES AND REGULATIONS</h3></span></label>
        <div class="content">
          <p>
           •  Any kind of malpractice during the event will lead to disqualification of the team immediately. <br>
• No electronic gadgets like phone, calculator are allowed.<br>
• The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website.<br>
          </p>
        </div>
      </div>

    </div>
  </div>
<h3 class="w3-center w3-text-white">Registration Fee : 100/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
         <input type="hidden" name="amount" value="100"/>
         <input type="hidden" name="ename" value="ENGINEER VS CIRCUIT"/>
         <?php if(!$hash) { ?>
             <?php
                                          include("../db_config.php");
                                          $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='ENGINEER VS CIRCUIT' and status='success'");
                                           $count=$result->num_rows;
                                           if($count==0)
                                           {
                                               echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
                                           }
                                           else
                                           {
                                               echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
                                           }
                                          $con->close();
                                       ?>



          <?php } ?>

      </form>
    </div>
  </div>
</div>
<br><br><br><br><br>
           <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="text-center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p class="w3-text-black">In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="text-center">
                                <h5>Chandra sekhar</h5>
                                <h5>9063664758</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <h5>Kunal Singh</h5>
                                <h5>+91 95812 16803</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>
  <form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input type="hidden" name="surl" value="http://mohanamantra.com/technoholik/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="furl" value="http://mohanamantra.com/technoholik/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
     <input type="hidden" name="curl" value="http://mohanamantra.com/technoholik/response.php" />

        <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
        <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
        <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" /></td>
        <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" /></td>

        </tr>
      </table>
    </form>
</body>
</html>
