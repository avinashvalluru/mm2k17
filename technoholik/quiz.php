<?php
include("../db_config.php");
include("../functions.php");
session_start();
?>   
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Quiz | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>
    body {
      background: #212121 url("img/bg/Quiz.jpg") no-repeat top center fixed;
      background-size: cover;
      margin: 0;
      padding: 0;
      height: 100%;
      width: 100%; 
    }


    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {   
      h1{
        transform: scale(0.7);
        
      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }   
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
</head>
<body>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br> 
  <h1 class="heading w3-text-pink w3-center" style="" >QUIZ’INO</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
               Exuberating , motivating , enlightening and engaging would describe the quiz
                competition in our Mohan mantra . it&#39;s a pleasure for brain and a food for
                thought . it can accomplish anything from being one&#39;s past time to bigger
                things like spreading awareness .<br>
                Let us test your knowledge with quiz event here at MM2k17.<br><br>
                1. QUIZ&#39;INO consists of general questions.<br>
                2.The time limit for this event is 60 minutes.(event continues throughout the
                day).<br>
                3.There will be atmost 15 teams will be allowed to participate at a time.
                4.Each team will be given 50 points intially.<br>

                5.The questions will be projected on the screens with 8-10 options to choose
                from it.<br>
                6.The teams should bid those points on the right answer in the multiples of 5.<br>
                7.There is no limit in bidding the number of points on a answer.<br>
                8.The team which bid the correct answer will get double the points.<br>
                9.The teams which bid on the wrong answer will lose their points.<br>
                10.If a team stands at 0 points,then that team is eliminated.<br><br>
                <b>REGISTRATION FEE</b>:&nbsp;Rs.100/- .<br>
                <b>Dates</b>: &nbsp;Sept 25th - 26th 
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
         • Each team can have at most 3 members.<br> 
• Any no. of teams can come from a college..<br>
• Spot registrations are available.<br>
• Interested candidates can also register for event by paying the above
mentioned amount through online banking.<br>
• The soft copy of the receipt should be mailed to quiztechno2k17@gmail.com<br>
• Details of all team members like full names, Registration id&#39;s, contact numbers,
course name(group), college name, and city should be sent along with the
payment receipt.<br>
 
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
          <label for="section-3"> <span><h3>RULES</h3></span></label>
          <div class="content">
            <p>
    1. All participants must bring their MM id cards on the day of event.<br>
2. Any kind of misbehavior/malpractice by the participants will directly lead to
disqualification of the team.<br>
3. Arguments at any cost are not accepted decision of the quiz team is final.<br>
4. The organizing committee had the rights to change the above rules as they
deem fit. Change in rules if any will be highlighted on the website.

</p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
          <label for="section-4"> <span><h3>Prizes</h3></span></label>
          <div class="content">
            <p>
               The prize money differs based on the winning criteria<br><br>
              1. The first team which achieves more than 240 points in the given time,they
              are awarded with 900\- of cash prize.<br>
              2. The first team which achieves more than 340 points in the given time,they
              are awarded with 1200\- of cash prize.<br>
              3. If None of the team achieved the above two.<br>
              Then the team which gets the highest points among all the teams will be
              awarded with 600\- of cash prize.
         </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<h1 class="heading w3-text-pink w3-center" style="" >Technical Quiz</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-5" value="toggle" />
            <label for="section-5"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>
               Designed by and for those who like to sit and think, the quiz promises participants an adrenaline pumping edge-of-the-seat experience where the only difference between high-fiving teammates and basking in the glory of triumph and, well, not, is going to be their skill to crack that critical piece of ancient wisdom from the dusty recesses of their memory.<br>
                So, if you are a Science/Technology fanatic, Technical Quiz is the place to be. <br><br>
               <b>REGISTRATION FEE</b>:&nbsp;Rs.50/- per head.<br>              
                <b>Dates</b>: &nbsp;Sept 25th - 26th 
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
            <label for="section-6"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
       •  Each team consists of 3 participants.<br>
• A Student should have knowledge in their corresponding technical subjects.<br>
• Quiz will be conducted departments wise like C-branch (CSE, IT, CSSE), E-branch (EEE, ECE, EIE, ECONE) and (civil, mechanical) come under one.<br>
• Each Quiz will be conducted independently involving questions on their respective core subjects.<br>
• Students of different colleges can form a team.<br>
• Any number of teams can come from a college.<br>
• Students from a college can come as team by themselves else we can form a team (here).<br>
• Entry fee is Rs.50 per head.<br>
• Spot registrations are available.<br>
• Interested candidates can also register for the event by paying above mentioned amount through Online Banking.<br>
• The soft copy of the payment receipt is to be mailed to quiztechno2k17@gmail.com. <br>
• The type of quiz that the participant is registering for (C branch, E branch etc) must be mentioned as the subject followed by the names of the team members. <br>
• Details of all the team members like full names and MM id’s along with their Contact Numbers, Course Name (group), College Name and City need to be sent along with the payment receipt.

 
             </p>
           </div>
         </div>
         <div class="section">
          <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
          <label for="section-7"> <span><h3>RULES </h3></span></label>
          <div class="content">
            <p>
• Any kind of malpractice/misbehavior by the participants leads to the immediate disqualification of the team. <br>
• Arguments at any cost are not entertained. The decision of the Quiz team is considered to be final. <br>
• The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website.
</p>
          </div>
        </div>
        <div class="section">
          <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
          <label for="section-8"> <span><h3>Prizes</h3></span></label>
          <div class="content">
            <p>
  • Winner's team will grab prize money and also winner's certificate. <br>
• All the participants  will be awarded with a participation certificate.
</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>     
</div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us <br>&emsp;&emsp;quiztechno2k17@gmail.com   or technoholik.mm2k17@gmail.com 
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5>Mahesh Babu P</h5>
        <h5>+91 79817 67126</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>Vamsi UV</h5>
        <h5>+91 83417 31765</h5>
      </div>
    </div>
  </div><br><br><br>
</div> 
</body>
</html>