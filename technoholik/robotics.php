 <?php
include("../db_config.php");
include("../functions.php");
session_start();

?>
<?php

if(isset($_POST['reg'])){

  if(isset($_SESSION['p_id']))
  {
$MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
$hash_string = '';
// Merchant Salt as provided by Payu
$SALT = "H5xwGsQ6"; //Please change this value with live salt for production

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";
$id=$_SESSION['p_id'];
$sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
$result = $con->query($sql);
$row=$result->fetch_assoc();
$fname=$row['p_id'];
$email=$row['email'];
$mbl=$row['mobile'];

$action = '';
$amt=$_POST['amount'];
$amt= $amt+($amt*0.03);
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
$posted['txnid']=$txnid;
$posted['amount']=$amt;
$posted['firstname']=$fname;
$posted['email']=$email;
$posted['phone']=$mbl;
$posted['productinfo']=$_POST['ename'];;
$posted['key']=$MERCHANT_KEY;

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
    empty($posted['key'])
    || empty($posted['txnid'])
    || empty($posted['amount'])
    || empty($posted['firstname'])
    || empty($posted['email'])
    || empty($posted['phone'])
    || empty($posted['productinfo'])

    ) {
    $formError = 1;
} else {

  $hashVarsSeq = explode('|', $hashSequence);

  foreach($hashVarsSeq as $hash_var) {
    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
    $hash_string .= '|';
  }

  $hash_string .= $SALT;



  $hash = strtolower(hash('sha512', $hash_string));
  $action = $PAYU_BASE_URL . '/_payment';
}
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}

}
else
{
  echo '<script>alert("Please login First");</script>';
}
$con->close();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Robotics | Mohana Mantra 2K17</title>
  <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
  <meta name="author" content="Avinash Valluru"/>
  <meta name="keywords" content="Mohana Mantra,mm,mm2017">
  <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/nprogress.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/w3.css">
  <link rel="stylesheet" href="events.css">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

  <script src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/notify.js"></script>
  <script src="../js/nprogress.js"></script>
  <style>
    body {
      background: #212121 url("img/bg/droids.jpg") no-repeat top center fixed;
      background-size: cover;
      margin: 0;
      padding: 0;
      height: 100%;
      width: 100%;
    }


    h1{
      font-size: 5.5em;
      font-family: Iceland;

    }
    @media screen and (max-width: 480px) {
      h1{
        transform: scale(0.7);

      }
    }
    h3{
      font-family: Play;
      font-size: 1.5em;
      color:yellow;
    }
    p {
      line-height: 1.8;
      margin: 0 0 2rem;
      color: white;
      font-family: 'Quicksand', sans-serif;
      font-size: 1.2em;
    }
  </style>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
  <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
  <script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
      clearInterval(interval);
      NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
      NProgress.start();
    });
  </script>
  <?php include("nav.php");?><br><br><br>
  <h1 class="heading w3-text-pink w3-center" style="" >ROBO WARS</h1><br>
  <div class="container">
   <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="left-menu">
        <div class="accordion">
          <div class="section">
            <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
            <label for="section-1"><span><h3>ABOUT</h3></span></label>
            <div class="content">
              <p>

                ‘War does not determine who is right. Only who is left’
                Passionate about Bots? Or you just like to watch things get crushed from fierce competition?
                The path to glory passes through the alley of destruction, lethal combats, violent bots, and flying blades!
                Merge your technical know-how with the ferocious gladiator in you! Make your bot fight for the ultimate glory or
                land up in the junk yard! So folks, fight smart!! Cause MM2k17's ROBOWARS just got crazier in its new avatar!

              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
            <label for="section-2"> <span><h3> RULES</h3></span></label>
            <div class="content">
              <p>
                  To know  Rules & Regulations<a href="robo_wars.pdf" class="w3-center w3-text-yellow" target="_blank"> Click here</a>
              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
            <label for="section-3"> <span><h3>REGISTRATION</h3></span></label>
            <div class="content">
              <p>
                For registration make sure to send your abstract containing your team details and the bot details to our mail. Last date for abstract submission is 24th of September to robotics.mm2k17@gmail.com


              </p>
            </div>
          </div>
          <div class="section">
            <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
            <label for="section-4"> <span><h3> PRICING</h3></span></label>
            <div class="content">
              <p>
                <b>ENTRY FEE</b>: Rs.800/-<br>
                <b>PRIZE MONEY</b>: Rs.40,000/-<br>
              </p>
            </div>
          </div>
        </div>
      </div>
              <h3 class="w3-center w3-text-white">Registration Fee : 800/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <input type="hidden" name="amount" value="800"/>
        <input type="hidden" name="ename" value="ROBO WARS"/>
        <?php if(!$hash) { ?>
        <?php
        include("../db_config.php");
        $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='ROBO WARS' and status='success'");
        $count=$result->num_rows;
        if($count==0)
        {
          echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
        }
        else
        {
          echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
        }
        $con->close();
        ?>



        <?php } ?>

      </form>
    </div>
  </div>
<br>
<h1 class="heading w3-text-pink w3-center" style="" >LINE FOLLOWER</h1><br>
<div class="container">
 <div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="left-menu">
      <div class="accordion">
        <div class="section">
          <input type="radio" name="accordion-1" id="section-6" value="toggle"/>
          <label for="section-6"><span><h3>ABOUT</h3></span></label>
          <div class="content">
            <p>
             Unleash your designing capabilities in making a robot that can make its way through the complex path and reach its destination. Line follower offers you a platform to showcase your ability in making your robot to be in “line” with your thoughts of reaching the destination!  The Line follower robot is a mobile machine that can detect and follow the line drawn on the floor. Generally, the path is predefined and can be visible like a black line on a white surface with a high contrasted color.  Participants should have to complete the paths as per rules and regulation. Event is divided in three rounds. Participants who completed each round in less time will be declared as a winner.  Design a robot that can traverse the designed grid and reach the destination area.
             <br>
           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-7" value="toggle"/>
        <label for="section-7"> <span><h3> RULES</h3></span></label>
        <div class="content">
          <p>
            To know  Rules & Regulations<a href="line_follower.pdf" class="w3-center w3-text-yellow" target="_blank"> Click here</a>
          </p>
        </div>
      </div>
      <div class="section">
        <input type="radio" name="accordion-1" id="section-8" value="toggle"/>
        <label for="section-8"> <span><h3>REGISTRATION</h3></span></label>
        <div class="content">
          <p>
           For registration make sure to send your abstract containing your team details and the bot details to our mail. Last date for abstract submission is 24th of September to robotics.mm2k17@gmail.com
         </p>
       </div>
     </div>
     <div class="section">
      <input type="radio" name="accordion-1" id="section-9" value="toggle"/>
      <label for="section-9"> <span><h3> PRICING</h3></span></label>
      <div class="content">
        <p>
          <b>ENTRY FEE</b>: Rs.500/-<br>
          <b>PRIZE MONEY</b>: Rs.5,000/-<br>
        </p>
      </div>
    </div>
     </div>
</div>
          <h3 class="w3-center w3-text-white">Registration Fee : 500/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <input type="hidden" name="amount" value="500"/>
        <input type="hidden" name="ename" value="LINE FOLLOWER"/>
        <?php if(!$hash) { ?>
        <?php
        include("../db_config.php");
        $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='LINE FOLLOWER' and status='success'");
        $count=$result->num_rows;
        if($count==0)
        {
          echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
        }
        else
        {
          echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
        }
        $con->close();
        ?>



        <?php } ?>

      </form>
    </div>
  </div>
<br>
<h1 class="heading w3-text-pink w3-center" style="" >ROBO MAZE</h1><br>
<div class="container">
 <div class="row">
  <div class="col-md-10 col-md-offset-1">
    <div class="left-menu">
      <div class="accordion">
        <div class="section">
          <input type="radio" name="accordion-1" id="section-10" value="toggle"/>
          <label for="section-10"><span><h3>ABOUT</h3></span></label>
          <div class="content">
            <p>
            Sometimes choosing a path that no one chooses is the key to achieve greatness. In Robo-Maze, you have the power to control your robot, to decide whether it takes the right path in a mind-boggling maze. Come along then, crack the maze with your intuition and skill to emerge victorious.  Robo-maze is an event where you need to lead your robot to the finishing point of the maze in minimum possible time.
             <br>
           </p>
         </div>
       </div>
       <div class="section">
        <input type="radio" name="accordion-1" id="section-11" value="toggle"/>
        <label for="section-11"> <span><h3> RULES</h3></span></label>
        <div class="content">
          <p>
            To know  Rules & Regulations<a href="robo_maze.pdf" class="w3-center w3-text-yellow" target="_blank"> Click here</a>
          </p>
        </div>
      </div>
      <div class="section">
        <input type="radio" name="accordion-1" id="section-12" value="toggle"/>
        <label for="section-12"> <span><h3>REGISTRATION</h3></span></label>
        <div class="content">
          <p>
           For registration make sure to send your abstract containing your team details and the bot details to our mail. Last date for abstract submission is 24th of September to robotics.mm2k17@gmail.com
         </p>
       </div>
     </div>
     <div class="section">
      <input type="radio" name="accordion-1" id="section-13" value="toggle"/>
      <label for="section-13"> <span><h3> PRICING</h3></span></label>
      <div class="content">
        <p>
          <b>ENTRY FEE</b>: Rs.500/-<br>
          <b>PRIZE MONEY</b>: Rs.5,000/-<br>
        </p>
      </div>
    </div>
     </div>
</div>
          <h3 class="w3-center w3-text-white">Registration Fee : 500/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <input type="hidden" name="amount" value="500"/>
        <input type="hidden" name="ename" value="ROBO MAZE"/>
        <?php if(!$hash) { ?>
        <?php
        include("../db_config.php");
        $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='ROBO MAZE' and status='success'");
        $count=$result->num_rows;
        if($count==0)
        {
        echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
        }
        else
        {
          echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
        }
        $con->close();
        ?>



        <?php } ?>

      </form>
    </div>
  </div>
<br><br><br><br><br>
<div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-12">
      <div class="text-center">
        <br>
        <h2><strong>CONTACT</strong></h2>
        <p class="w3-text-black">In case of any queries or clarifications, please feel
          free to contact us
        </p>
      </div>
    </div>
  </div>
  <div class="row" style="font-family: 'Comfortaa', cursive;">
    <div class="col-md-4 col-md-offset-2">
      <div class="text-center">
        <h5> Pavan</h5>
        <h5>7036606930</h5>
      </div>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <h5>Kunal Singh</h5>
        <h5>+91 95812 16803</h5>
      </div>
    </div>
  </div><br><br><br>
</div>
  <form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

  <input type="hidden" name="surl" value="http://mohanamantra.com/technoholik/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
  <input type="hidden" name="furl" value="http://mohanamantra.com/technoholik/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
  <input type="hidden" name="curl" value="http://mohanamantra.com/technoholik/response.php" />

  <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
  <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
  <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
  <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
  <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" />

</table>
</form>
</body>
</html>
