 <?php
include("../db_config.php");
include("../functions.php");
session_start();

?>
<?php

if(isset($_POST['reg'])){

  if(isset($_SESSION['p_id']))
  {
$MERCHANT_KEY = "m4hSrS"; //Please change this value with live key for production
$hash_string = '';
// Merchant Salt as provided by Payu
$SALT = "H5xwGsQ6"; //Please change this value with live salt for production

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";
$id=$_SESSION['p_id'];
$sql = "SELECT p_id , email,mobile FROM participants WHERE p_id= '$id' ";
$result = $con->query($sql);
$row=$result->fetch_assoc();
$fname=$row['p_id'];
$email=$row['email'];
$mbl=$row['mobile'];

$action = '';
$amt=$_POST['amount'];
$amt= $amt+($amt*0.03);
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
$posted['txnid']=$txnid;
$posted['amount']=$amt;
$posted['firstname']=$fname;
$posted['email']=$email;
$posted['phone']=$mbl;
$posted['productinfo']=$_POST['ename'];;
$posted['key']=$MERCHANT_KEY;

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
    empty($posted['key'])
    || empty($posted['txnid'])
    || empty($posted['amount'])
    || empty($posted['firstname'])
    || empty($posted['email'])
    || empty($posted['phone'])
    || empty($posted['productinfo'])

    ) {
    $formError = 1;
} else {

  $hashVarsSeq = explode('|', $hashSequence);

  foreach($hashVarsSeq as $hash_var) {
    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
    $hash_string .= '|';
  }

  $hash_string .= $SALT;



  $hash = strtolower(hash('sha512', $hash_string));
  $action = $PAYU_BASE_URL . '/_payment';
}
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}

}
else
{
  echo '<script>alert("Please login First");</script>';
}
$con->close();
}
?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Poster Presentation | Mohana Mantra 2K17</title>
      <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
      <meta name="theme-color" content="#000000">
      <meta name="author" content="Avinash Valluru"/>
      <meta name="keywords" content="Mohana Mantra,mm,mm2017">
      <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/nprogress.css">
      <link rel="stylesheet" href="../css/normalize.css">
      <link rel="stylesheet" href="../css/w3.css">
      <link rel="stylesheet" href="events.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland|Roboto" rel="stylesheet">

      <script src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/notify.js"></script>
      <script src="../js/nprogress.js"></script>
      <style>
        body {
          background: #212121 url("img/bg/present.jpg") no-repeat top center fixed;
          background-size: cover;
          margin: 0;
          padding: 0;
          height: 100%;
          width: 100%;
        }


        h1{
          font-size: 5.5em;
          font-family: Iceland;

        }
        @media screen and (max-width: 480px) {
          h1{
            transform: scale(0.7);

          }
        }
        h3{
          font-family: Play;
          font-size: 1.5em;
          color:yellow;
        }
        p {
          line-height: 1.8;
          margin: 0 0 2rem;
          color: white;
          font-family: 'Quicksand', sans-serif;
          font-size: 1.2em;
        }

      </style>

  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
</head>

<body>
  <script>
    $(document).ready(function () {
      submitPayuForm();
    });
  </script>
      <script type="text/javascript">
        NProgress.start();

        // Increase randomly
        var interval = setInterval(function() { NProgress.inc(); }, 1000);

        // Trigger finish when page fully loaded
        jQuery(window).load(function () {
          clearInterval(interval);
          NProgress.done();
        });

        // Trigger bar when exiting the page
        jQuery(window).unload(function () {
          NProgress.start();
        });
      </script>
      <?php include("nav.php");?><br><br><br>
      <h1 class="heading w3-text-pink w3-center" style="" >Poster Presentation</h1><br>
      <div class="container">
       <div class="row">
        <div class="col-md-10 col-md-offset-1">

          <div class="left-menu">
            <div class="accordion">
              <div class="section">
                <input type="radio" name="accordion-1" id="section-1" checked="checked"/>
                <label for="section-1"><span><h3>ABOUT</h3></span></label>
                <div class="content">
                  <p>

The basic trait of an engineer is how well his /her presentation skills are. Anyone can present ideas but it takes a huge amount of proficiency, creativity and imagination to bring down all ideas into one single poster. ”Poster Presentation” provides you a platform to emphasize the ability to deliver a visual presentation. Time’s ticking! Wait no more, registrations are now open. <br>
<b>Registration Fee</b>: RS.300/- <br>
<b>DATES</b>: &nbsp;25 September: EEE, CE and Degree<br>
       &emsp;&emsp;&emsp;&emsp;26 September:  ECE, CSE and ME


                  </p>
                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-2" value="toggle"/>
                <label for="section-2"> <span><h3> REGISTRATIONS</h3></span></label>
                <div class="content">
                  <p>
                     • Spot Registrations are also available.<br>
 • The SUBJECT of the mail sent SHOULD be in the following format: &lt;Branch code&gt; &lt;MM ID&gt; &lt;name of the poster>, the mails which aren’t in this format will be discarded. <br>
Ex:  ECE  mmxxxxxx xyz<br>
• ABSTRACT must contain the Full names and MM id’s of the Participants along with their Contact Numbers, Course Name (group), College Name and City.<br>
• Acceptance of poster for presentation will be intimated through email which will also contain the date, regarding when a particular team must present their poster.<br>
• The selected teams will have to register for the event participation by paying an amount of Rs.300 per team through Online Banking.<br>
• Participants are requested to take care while paying for the event so as not to pay multiple times.<br>
• The soft copy of the payment receipt must be sent to the mail posterpresentation.mm2k17@gmail.com or poster@mohanamantra.com <br>

                  </p>

                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-3" value="toggle"/>
                <label for="section-3"> <span><h3>RULES AND REGULATIONS</h3></span></label>
                <div class="content">
                  <p>All participants must bring their college ID cards at the time of the event. <br>
• The number of authors per poster should not exceed two (2).<br>
• The title must be prominently clear and to the point. The complete TITLE of abstract should be Times New Roman font, size 12 using bold face. <br>
• ABSTRACT should be submitted in MS word format and not more than 250 words.<br>
• The MohanaMantra2k17 logo must be displayed in poster at right top.<br>
• The presenting authors name must be marked by asterisk. <br>
• The team members must bring their self-made posters on the day of competition in time. <br>
• The poster must be printed and presented on a 3x4 feet FLEX SHEET. <br>
• Certificate will be issued to participants who have registered to the event and presented the poster.<br>
• Decision of the judges is final.<br>
• Participants need to pay the Registration fee only when the participant gets the acceptance mail. In case of the participant did payment without acceptance mail, money will not be refunded.<br>
• The organizing committee reserves the rights to change any or all of the above rules as they deem fit. Change in rules, if any will be highlighted on the website.<br>

                  </p>

                </div>
              </div>
              <div class="section">
                <input type="radio" name="accordion-1" id="section-4" value="toggle"/>
                <label for="section-4"> <span><h3>TOPICS</h3></span></label>
                <div class="content">
                  <p>
                    <b>1. Electronics and Communications, EConE,EIE (Code: ECE)</b><br>
                    • VLSI & Embedded systems<br>
                    • Micro processer and micro controllers<br>
                    • Image Processing and Digital Signal processing<br>
                    • Nano Technology<br>
                    • Wireless sensor networks<br>
                    • Smart antennas<br>
                    • MEMS<br>
                    • Miscellaneous and Innovative topics<br>
                    <b>2. ELECRICAL AND ELECTRONICS (Code: EEE)</b><br>
                    • Soft computing techniques in electrical engineering (Genetic, evolutionary, swarm intelligence etc.,)<br>
                    • Artificial intelligence in electrical engineering (Neural, Fuzzy, ANFIS etc)<br>
                    • Power electronics and energy efficient industrial drives<br>
                    • Renewable power conversion technologies and energy storage systems<br>
                    • Power quality issues, monitoring and improvement techniques<br>
                    • Smart grids and expert systems<br>
                    • Miscellaneous and Innovative topics<br>

                    <b>3. CSE, IT, CSSE (Code: CSE)</b><br>
                    • Network security<br>
                    • Software engineering<br>
                    • Cloud Computing<br>
                    • Knowledge and data mining<br>
                    • Mobile Application Development<br>
                    • Internet Of things<br>
                    • Information Security<br>
                    • Miscellaneous and Innovative topics<br>

                    <b>4. Civil (Code: CE)</b><br>
                    • Ground Improvement Techniques<br>
                    • Transportation planning and management<br>
                    • Water Resources Engineering<br>
                    • Structural Engineering<br>
                    • Remote Sensing and GIS<br>
                    • Disaster Mitigation Management<br>
                    • Environmental engineering<br>
                    • Miscellaneous and Innovative topics<br>

                    <b>5. Mechanical (Code: MEC)</b><br>
                    • CAD, CAM & SIM<br>
                    • Robotics<br>
                    • Aerospace Flywheel Development<br>
                    • Heat pipes<br>
                    • Valvetronic Engine<br>
                    • Alternative Fuels<br>
                    • Miscellaneous and Innovative topics<br>
                    <b>6. Degree (Code: Degree)</b><br>
                    • Startups<br>
                    • Role of science and technology in development of India<br>
                    • Solid waste management<br>
                    • Digital India<br>
                    • Scientific background in Indian customs and traditions<br>
                    • Digital payments in the era of demonotisation<br>
                    • GST<br>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <h3 class="w3-center w3-text-white">Registration Fee : 300/-</h3><br>
      <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <input type="hidden" name="amount" value="300"/>
        <input type="hidden" name="ename" value="Poster Presentation"/>
        <?php if(!$hash) { ?>
        <?php
        include("../db_config.php");
        $result= $con->query("SELECT p_id from tech WHERE p_id='$_SESSION[p_id]' and event_name='Poster Presentation' and status='success'");
        $count=$result->num_rows;
        if($count==0)
        {
        echo "<h2 class='w3-center w3-text-cyan'>* Pay At Spot</h2>";
        }
        else
        {
          echo "<h5 class='w3-center' style='color:green'>* You have already registered Successfully<br><br><span style='color:red'> Any issue contact +91 7799773346</span></h5>";
        }
        $con->close();
        ?>



        <?php } ?>

      </form>
    </div>
  </div>
<br><br><br><br><br>
           <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="text-center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p class="w3-text-black">In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="text-center">
                                <h5>Sai Vamsi</h5>
                                <h5>+91 83417 31765</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <h5>Jaideep Kurapati</h5>
                                <h5>+91 9700294153</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>
  <form  action="<?php echo $action; ?>" method="post" name="payuForm" class="w3-hide" >
  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

  <input type="hidden" name="surl" value="http://mohanamantra.com/technoholik/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
  <input type="hidden" name="furl" value="http://mohanamantra.com/technoholik/response.php" /><!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
  <input type="hidden" name="curl" value="http://mohanamantra.com/technoholik/response.php" />

  <input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"/>
  <input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" />
  <input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" /></td>
  <input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />
  <input type="hidden" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" />

</table>
</form>

    </body>

    </html>
