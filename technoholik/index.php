<?php
include("../db_config.php");
    include("../functions.php");
     session_start();

  ?>   
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Technoholik | Mohana Mantra 2K17</title>
    <meta name="description" content="Mohana Mantra is the annual techno-cultural festival hosted by the Sree Vidyanikethan Educational Institutions, during the dates 25th – 27th of September 2017."/>
  <meta name="theme-color" content="#000000">
    <meta name="author" content="Avinash Valluru"/>
    <meta name="keywords" content="Mohana Mantra,mm,mm2017">
    <meta name="keywords" content="mm2k17,mohanamantra,sree vidyanikethan">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/nprogress.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/w3.css">
     <link rel="stylesheet" href="../css/tabs.css">
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fresca|Play|Arima+Madurai|Graduate|Comfortaa:700|Raleway|Quicksand|Iceland" rel="stylesheet">
    
     <script src="../js/jquery.min.js"></script>
     <script type="text/javascript" src="../js/notify.js"></script>
     <script src="../js/nprogress.js"></script>
    <style>
        body {
            background: #212121 url("img/tech.jpg") no-repeat top center fixed;
            background-size: cover;
            margin: 0;
            padding: 0;
           height: 100%;
            width: 100%; 
        }
         
        figure{
            transform: scale(0.8);
        }
        h1{
                          font-size: 5.5em;
                            font-family: Iceland;
                            
                    }
             @media screen and (max-width: 480px) {   
                h1{
                    transform: scale(0.8);
                }
      }           
    </style>
    
</head>
<body>
<script type="text/javascript">
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });
</script>
<?php include("nav.php");?>
             <h1 class="heading w3-text-pink w3-center" style="padding-top: 48px;" >Technoholik</h1>
<div class="container" >
        
        <figure class="snip1091 red"><img src="img/paper.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Paper Presentation</span></h2>
          </figcaption><a href="paper_presentation"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/poster.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Poster Presentation</span></h2>
          </figcaption><a href="poster_presentation"></a>
        </figure>
         <figure class="snip1091 red"><img src="img/robo.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Robotics</span></h2>
          </figcaption><a href="robotics"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/cir.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">circuitrix</span></h2>
          </figcaption><a href="circutrix"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/code.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">Code-a-thon</span></h2>
          </figcaption><a href="codeathon"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/cons.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">constructo</span></h2>
          </figcaption><a href="constructo"></a>
        </figure>
        <figure class="snip1091 navy"><img src="img/mech.jpg" alt="sq-sample10"/>
          <figcaption>
            <h2><span class="w3-text-teal">mechanica</span></h2>
          </figcaption><a href="mechanica"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/expo.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Expo Cross</span></h2>
          </figcaption><a href="expocross"></a>
        </figure>
        <figure class="snip1091 red"><img src="img/Quiz.jpg" alt="sq-sample6"/>
          <figcaption>
            <h2><span class="w3-text-teal">Quiz</span></h2>
          </figcaption><a href="quiz"></a>
        </figure>
       
       <div class="col-md-8 col-md-offset-2 w3-text-black w3-gray">
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-12">
                            <div class="center">
                                <br>
                                <h2><strong>CONTACT</strong></h2>
                                <p>In case of any queries or clarifications, please feel
                                    free to contact us
                                    </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-family: 'Comfortaa', cursive;">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="center">
                                <h5>Vamsi Uv</h5>
                                <h5>+91 83417 31765</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="center">
                                <h5>Kunal Singh</h5>
                                <h5>+91 95812 16803</h5>
                            </div>
                        </div>
                    </div><br><br><br>
        </div>  
         
</div> 

<script>

  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );
</script>
</body>

</html>